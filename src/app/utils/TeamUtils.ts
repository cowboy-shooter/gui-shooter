import {TeamDTO} from "../base/dto/teams/TeamDTO";
import {UserUtils} from "./UserUtils";

export class TeamUtils {

    public static isRequestor(team: TeamDTO): boolean {
        for (let member of team.requestors) {
            if (member.id === UserUtils.getUserProfile().id) {
                return true;
            }
        }
        return false;
    }

    public static isMemberOfTeam(team: TeamDTO): boolean {
        for (let member of team.members) {
            if (member.id === UserUtils.getUserProfile().id) {
                return true;
            }
        }
        return false;
    }

    public static isInvited(team: TeamDTO): boolean {
        for (let member of team.invitees) {
            if (member.id === UserUtils.getUserProfile().id) {
                return true;
            }
        }
        return false;
    }

    public static isLeader(team: TeamDTO): boolean {
        return team.leader && team.leader.id === UserUtils.getUserProfile().id;
    }
}