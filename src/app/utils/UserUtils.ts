import {KeycloakProfile} from "keycloak-js";
import RootComponent from "../RootComponent";

export class UserUtils {

    public static getUserProfile(): KeycloakProfile {
        let profile: KeycloakProfile = RootComponent.keycloak.profile as KeycloakProfile;
        // @ts-ignore
        profile.id = RootComponent.keycloak.userInfo['sub'];
        return RootComponent.keycloak.profile as KeycloakProfile;
    }

    public static isAdmin(): boolean {
        return this.hasRole('admin');
    }

    public static hasRole(role: string): boolean {
        let roles: String[] = this.getResourceRoles();
        let hasRole = false;
        roles.forEach((assignedRole) => {
            if (assignedRole === role) {
                hasRole = true;
                return;
            }
        });
        return hasRole;
    }


    public static getResourceRoles(): String[] {
        // @ts-ignore
        return RootComponent.keycloak.resourceAccess["gui-shooter"].roles;
    }

}