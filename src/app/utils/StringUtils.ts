export class StringUtils {

    public static camelCaseToTitle(camelCaseString: string): string {
        let words: string = '';

        let tempWord = '';

        for (var i = 0; i < camelCaseString.length; i++) {
            let char = camelCaseString.charAt(i);
            if (char === char.toUpperCase()) {
                words += tempWord.charAt(0).toUpperCase() + tempWord.slice(1) + ' ';
                tempWord = char;
            } else {
                tempWord += char;
            }
        }

        return words + tempWord.charAt(0).toUpperCase() + tempWord.slice(1);
    }

}