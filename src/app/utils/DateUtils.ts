import * as moment from 'moment';
import {Properties, Settings} from "../../Settings";

export class DateUtils {

    static formatDate(date: Date): string {
        // @ts-ignore
        return moment(date).format(Settings.getProperty(Properties.GENERAL_DATE_FORMAT) as string);
    }

}