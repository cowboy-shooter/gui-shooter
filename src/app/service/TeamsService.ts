import {Service} from "./Service";
import {TeamDTO, TeamDTOMapper} from "../base/dto/teams/TeamDTO";
import {Page} from "../base/dto/paging/Page";
import {UserDTO} from "../base/dto/teams/UserDTO";
import {GraphQLResponse} from "../base/GraphQLResponse";
import {PageableResponse} from "../base/dto/paging/PageableResponse";

export class TeamsService extends Service<TeamDTOMapper, TeamDTO> {

    public getTeams(page: Page, teamMapper?: TeamDTOMapper): Promise<GraphQLResponse<PageableResponse<TeamDTO>>> {
        return this.buildQuery()
            .setName('teams')
            .setProperties(`paging: {size: ${page.size}, page: ${page.page}, sortBy: "${page.sorting.sortBy}", sortDirection: ${page.sorting.sortDirection}}`)
            .setReturnProperties(teamMapper)
            .getPaged();
    }

    public getAllTeams(teamMapper?: TeamDTOMapper): Promise<GraphQLResponse<TeamDTO[]>> {
        return this.buildQuery()
            .setName('allTeams')
            .setReturnProperties(teamMapper)
            .getList();
    }

    public getTeam(id: string, teamMapper?: TeamDTOMapper): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildQuery()
            .setName('team')
            .setProperties(`id: "${id}"`)
            .setReturnProperties(teamMapper)
            .send();
    }

    public getOwnTeam(teamMapper?: TeamDTOMapper): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildQuery()
            .setName('ownTeam')
            .setReturnProperties(teamMapper)
            .send();
    }

    public kickMemberOut(user: UserDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('kickMemberOut')
            .setProperties(`userId: "${user.id}"`)
            .send();
    }

    public makeMemberLeader(user: UserDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('makeMemberLeader')
            .setProperties(`userId: "${user.id}"`)
            .send();
    }

    public leaveTeam(): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('leaveTeam')
            .send();
    }

    public inviteMember(user: UserDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('inviteMember')
            .setProperties(`userId: "${user.id}"`)
            .send();
    }

    public cancelChallenge(team: TeamDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('cancelChallenge')
            .setProperties(`teamId: "${team.id}"`)
            .send();
    }

    public challengeTeam(team: TeamDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('challengeTeam')
            .setProperties(`teamId: "${team.id}"`)
            .send();
    }

    public getChallengers(teamMapper?: TeamDTOMapper): Promise<GraphQLResponse<TeamDTO[]>> {
        return this.buildQuery()
            .setName('challengers')
            .setReturnProperties(teamMapper)
            .getList();
    }

    public cancelInvite(user: UserDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('cancelInvite')
            .setProperties(`userId: "${user.id}"`)
            .send();
    }

    public requestJoining(team: TeamDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('requestJoining')
            .setProperties(`teamId: "${team.id}"`)
            .send();
    }

    public cancelRequestJoining(team: TeamDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('cancelRequestJoining')
            .setProperties(`teamId: "${team.id}"`)
            .send();
    }

    public acceptRequest(user: UserDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('acceptRequest')
            .setProperties(`userId: "${user.id}"`)
            .send();
    }

    public acceptJoinRequest(team: TeamDTO): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('acceptRequestJoining')
            .setProperties(`teamId: "${team.id}"`)
            .send();
    }

    public createTeam(name: string, url: string, description: string, language: string, flag: string, mapper?: TeamDTOMapper): Promise<GraphQLResponse<TeamDTO>> {
        if (!mapper) {
            mapper = {
                id: true
            }
        }

        return this.buildMutation()
            .setName('createTeam')
            .setProperties(`name: "${name}", url: "${url}", description: "${description}", language: ${language}, flag: "${flag}"`)
            .setReturnProperties(mapper)
            .send();
    }

    public updateTeam(name: string, url: string, description: string, language: string, flag: string, mapper?: TeamDTOMapper): Promise<GraphQLResponse<TeamDTO>> {
        if (!mapper) {
            mapper = {
                id: true
            }
        }

        return this.buildMutation()
            .setName('updateTeam')
            .setProperties(`name: "${name}", url: "${url}" , description: "${description}", language: ${language}, flag: "${flag}"`)
            .setReturnProperties(mapper)
            .send();
    }

    public changeFlag(flag: string): Promise<GraphQLResponse<TeamDTO>> {
        return this.buildMutation()
            .setName('changeFlag')
            .setProperties(`flag: "${flag}"`)
            .send();
    }
}
