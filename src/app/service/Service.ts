import {GraphqlBuilder, GraphQLType} from "./builder/GraphqlBuilder";
import {Mapper} from "../base/dto/Mapper";

export class Service<T extends Mapper, DTO> {

    buildMutation(): GraphqlBuilder<T, DTO> {
        return new GraphqlBuilder(GraphQLType.MUTATION);
    }

    buildQuery(): GraphqlBuilder<T, DTO> {
        return new GraphqlBuilder(GraphQLType.QUERY);
    }
}