import {Service} from "./Service";
import {Page} from "../base/dto/paging/Page";
import {UserTeamWrappedUserDTO, UserTeamWrapperUserDTOMapper} from "../base/dto/teams/UserTeamWrappedUserDTO";
import {GraphQLResponse} from "../base/GraphQLResponse";
import {PageableResponse} from "../base/dto/paging/PageableResponse";

export class TeamUserService extends Service<UserTeamWrapperUserDTOMapper, UserTeamWrappedUserDTO> {

    public getOwnTeamManagement(page: Page, teamUserMapper?: UserTeamWrapperUserDTOMapper): Promise<GraphQLResponse<PageableResponse<UserTeamWrappedUserDTO>>> {
        return this.buildQuery()
            .setName('ownTeamManagement')
            .setProperties(`paging: {size: ${page.size}, page: ${page.page}}`)
            .setReturnProperties(teamUserMapper)
            .getPaged();
    }

}
