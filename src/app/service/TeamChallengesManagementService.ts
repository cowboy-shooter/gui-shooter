import {Service} from "./Service";
import {PageableResponse} from "../base/dto/paging/PageableResponse";
import {Page} from "../base/dto/paging/Page";
import {
    TeamChallengesManagementDTOMapper,
    TeamChallengesManagementWrappedDTO
} from "../base/dto/matches/TeamChallengesManagementWrappedDTO";
import {GraphQLResponse} from "../base/GraphQLResponse";

export class TeamChallengesManagementService extends Service<TeamChallengesManagementDTOMapper, TeamChallengesManagementWrappedDTO> {

    public getOwnChallengesTeamManagement(page: Page, mapper: TeamChallengesManagementDTOMapper): Promise<GraphQLResponse<PageableResponse<TeamChallengesManagementWrappedDTO>>> {
        return this.buildQuery()
            .setName('challengesManagement')
            .setProperties(`paging: {size: ${page.size}, page: ${page.page}}`)
            .setReturnProperties(mapper)
            .getPaged();
    }

}
