import {Service} from "./Service";
import {GraphQLResponse} from "../base/GraphQLResponse";
import {TournamentStatisticsDTO, TournamentStatisticsDTOMapper} from "../base/dto/tournaments/TournamentStatisticsDTO";

export class TournamentsStatisticsService extends Service<TournamentStatisticsDTOMapper, TournamentStatisticsDTO> {

    public getTournamentStatistics(tournamentId: string, mapper: TournamentStatisticsDTOMapper): Promise<GraphQLResponse<TournamentStatisticsDTO>> {
        return this.buildQuery()
            .setName('tournamentStatistics')
            .setProperties(`id: "${tournamentId}"`)
            .setReturnProperties(mapper)
            .send();
    }

}
