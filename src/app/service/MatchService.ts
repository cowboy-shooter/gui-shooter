import {Service} from "./Service";
import {MatchDTO, MatchDTOMapper} from "../base/dto/matches/MatchDTO";
import {TeamDTO} from "../base/dto/teams/TeamDTO";
import {GraphQLResponse} from "../base/GraphQLResponse";
import {PageableResponse} from "../base/dto/paging/PageableResponse";
import {Page} from "../base/dto/paging/Page";

export class MatchService extends Service<MatchDTOMapper, MatchDTO> {

    public acceptChallenge(team: TeamDTO, mapper: MatchDTOMapper): Promise<GraphQLResponse<MatchDTO>> {
        return this.buildMutation()
            .setName('acceptChallenge')
            .setProperties(`teamId: "${team.id}"`)
            .setReturnProperties(mapper)
            .send();
    }

    public getMatches(page: Page, mapper: MatchDTOMapper): Promise<GraphQLResponse<PageableResponse<MatchDTO>>> {
        return this.buildQuery()
            .setName('matches')
            .setProperties(`paging: {size: ${page.size}, page: ${page.page}, sortBy: "${page.sorting.sortBy}", sortDirection: ${page.sorting.sortDirection}}`)
            .setReturnProperties(mapper)
            .getPaged();
    }

    public getParticipatedMatches(teamId: string, mapper: MatchDTOMapper): Promise<GraphQLResponse<MatchDTO[]>> {
        return this.buildQuery()
            .setName('participatedMatches')
            .setProperties(`teamId: "${teamId}"`)
            .setReturnProperties(mapper)
            .getList();
    }

    public getMatch(matchId: string, mapper: MatchDTOMapper): Promise<GraphQLResponse<MatchDTO>> {
        return this.buildQuery()
            .setName('match')
            .setProperties(`matchId: "${matchId}"`)
            .setReturnProperties(mapper)
            .send();
    }

}
