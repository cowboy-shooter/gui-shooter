import {Mapper} from "../../base/dto/Mapper";
import {AxiosInstance} from "axios";
import {GraphQLResponse} from "../../base/GraphQLResponse";
import {PageableResponse, PageableResponseMapper} from "../../base/dto/paging/PageableResponse";
import axios from "axios";
import RootComponent from "../../RootComponent";

export class GraphqlBuilder<T extends Mapper, DTO> {

    private type: GraphQLType;
    private name: string = '';
    private properties: string = '';
    private returnProperties?: T;
    protected axisGraphQL: AxiosInstance;

    constructor(type: GraphQLType) {
        this.type = type;
        this.axisGraphQL =
            axios.create({
                baseURL: '/graphql',
                headers: {
                    Authorization: `bearer ${RootComponent.keycloak.token}`,
                }
            });
    }

    setName(name: string): GraphqlBuilder<T, DTO> {
        this.name = name;
        return this;
    }

    setProperties(properties: string): GraphqlBuilder<T, DTO> {
        this.properties = properties;
        return this;
    }

    setReturnProperties(mapper?: T): GraphqlBuilder<T, DTO> {
        this.returnProperties = mapper;
        return this;
    }

    private getPropertiesString(mapper: {}): string {
        let result = '';
        for (let key in mapper) {
            // @ts-ignore
            let value = mapper[key];
            if (value instanceof Object) {
                result += key + '{' + this.getPropertiesString(value) + '}';
            } else {
                result += key + ', '
            }
        }
        return result;
    }

    send(): Promise<GraphQLResponse<DTO>> {
        return this.sendRequest(this.returnProperties ? this.getPropertiesString(this.returnProperties) : '')
            .then(result => {
                let data = undefined;
                if (result.data.data && result.data.data[this.name]) {
                    data = result.data.data[this.name];
                }

                let response: GraphQLResponse<DTO> = {
                    data: data
                };
                return response;
            });
    }

    getPaged(): Promise<GraphQLResponse<PageableResponse<DTO>>> {
        return this.sendRequest(this.returnProperties ? this.getPropertiesString(this.getPageable(this.returnProperties)) : '')
            .then(result => {
                let data = undefined;
                if (result.data.data && result.data.data[this.name]) {
                    data = result.data.data[this.name];
                }

                let response: GraphQLResponse<PageableResponse<DTO>> = {
                    data: data
                };
                return response;
            });
    }

    getList(): Promise<GraphQLResponse<DTO[]>> {
        return this.sendRequest(this.returnProperties ? this.getPropertiesString(this.returnProperties) : '')
            .then(result => {
                let data = undefined;
                if (result.data.data && result.data.data[this.name]) {
                    data = result.data.data[this.name];
                }

                let response: GraphQLResponse<DTO[]> = {
                    data: data
                };
                return response;
            });
    }

    private sendRequest(returnPropsString: string) {
        let propertiesString = '';
        if (this.properties !== '') {
            propertiesString = `(${this.properties})`;
        }

        if (returnPropsString !== '') {
            returnPropsString = `{${returnPropsString}}`;
        }

        let result: string = `
            ${this.type} {
                ${this.name} ${propertiesString} ${returnPropsString}
            }
        `;

        return this.axisGraphQL.post('', {query: result});
    }

    private getPageable(mapper: T): PageableResponseMapper<T> {
        return {
            content: mapper,
            pageInfo: {
                totalPages: true,
                totalElements: true,
                pageSize: true,
                pageNumber: true,
                numberOfElements: true
            }
        }
    }
}

export enum GraphQLType {
    MUTATION = 'mutation', QUERY = 'query'
}

