import {Service} from "./Service";
import {GraphQLResponse} from "../base/GraphQLResponse";

export class CommonService extends Service<{}, {}> {

    public getProgrammingLanguages(): Promise<GraphQLResponse<{}[]>> {
        return this.buildQuery()
            .setName('programmingLanguages')
            .send()
            .then(result => {
                let languages = result.data;

                let allLanguages: {}[] = [];

                if (languages) {
                    for (let key in languages) {
                        // @ts-ignore
                        let value: string = languages[key];
                        allLanguages.push({ key: key, value: key, text: value });
                    }
                }
                return {
                    data: allLanguages
                };
            });
    }
}