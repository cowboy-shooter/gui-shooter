import {Service} from "./Service";
import {TournamentDTO, TournamentDTOMapper} from "../base/dto/tournaments/TournamentDTO";
import {TeamDTO} from "../base/dto/teams/TeamDTO";
import {GraphQLResponse} from "../base/GraphQLResponse";
import {Page} from "../base/dto/paging/Page";
import {PageableResponse} from "../base/dto/paging/PageableResponse";

export class TournamentsService extends Service<TournamentDTOMapper, TournamentDTO> {

    public createTournament(teams: TeamDTO[], name: string, mapper?: TournamentDTOMapper): Promise<GraphQLResponse<TournamentDTO>> {
        return this.buildMutation()
            .setName('createTournament')
            .setProperties(`teamIds: [${teams.map((team) => `"${team.id}"`)}], name: "${name}"`)
            .setReturnProperties(mapper)
            .send();
    }

    public executeTier(tierId: string, mapper?: TournamentDTOMapper): Promise<GraphQLResponse<TournamentDTO>> {
        return this.buildMutation()
            .setName('executeTier')
            .setProperties(`id: "${tierId}"`)
            .setReturnProperties(mapper)
            .send();
    }

    public fixTier(tierId: string, teams: TeamDTO[], mapper?: TournamentDTOMapper): Promise<GraphQLResponse<TournamentDTO>> {
        return this.buildMutation()
            .setName('fixTier')
            .setProperties(`id: "${tierId}", teamIds: [${teams.map((team) => `"${team.id}"`)}]`)
            .setReturnProperties(mapper)
            .send();
    }

    public getTournaments(page: Page, mapper?: TournamentDTOMapper): Promise<GraphQLResponse<PageableResponse<TournamentDTO>>> {
        return this.buildQuery()
            .setName('tournaments')
            .setProperties(`paging: {size: ${page.size}, page: ${page.page}, sortBy: "${page.sorting.sortBy}", sortDirection: ${page.sorting.sortDirection}}`)
            .setReturnProperties(mapper)
            .getPaged();
    }

    public getTournament(id: string, mapper: TournamentDTOMapper): Promise<GraphQLResponse<TournamentDTO>> {
        return this.buildQuery()
            .setName('tournament')
            .setProperties(`id: "${id}"`)
            .setReturnProperties(mapper)
            .send();
    }

    public getAllTournaments(mapper?: TournamentDTOMapper): Promise<GraphQLResponse<TournamentDTO[]>> {
        return this.buildQuery()
            .setName('tournaments')
            .setReturnProperties(mapper)
            .getList();
    }

}
