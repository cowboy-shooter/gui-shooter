import {Service} from "./Service";
import {UserDTO, UserDTOMapper} from "../base/dto/teams/UserDTO";
import {GraphQLResponse} from "../base/GraphQLResponse";

export class UsersService extends Service<UserDTOMapper, UserDTO> {

    public createUser(user: UserDTO, mapper?: UserDTOMapper): Promise<GraphQLResponse<UserDTO>> {
        return this.buildMutation()
            .setName('createUser')
            .setProperties(`id: "${user.id}", email: "${user.email}", firstName: "${user.firstName}", lastName: "${user.lastName}", username: "${user.username}"`)
            .setReturnProperties(mapper)
            .send();
    }

}