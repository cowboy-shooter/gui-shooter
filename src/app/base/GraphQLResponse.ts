export interface GraphQLResponse<T> {
    data: T
}