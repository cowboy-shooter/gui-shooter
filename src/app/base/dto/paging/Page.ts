import {Sorting} from "./Sorting";

export interface Page {
    size: number,
    page: number,
    sorting: Sorting
}