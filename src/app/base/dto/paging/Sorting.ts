export interface Sorting {
    sortDirection: SortingDirection,
    sortBy: String
}

export enum SortingDirection {
    ASC = "ASC",
    DESC = "DESC"
}