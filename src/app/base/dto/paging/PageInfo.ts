import {Mapper} from "../Mapper";

export interface PageInfo {
    numberOfElements: number,
    pageNumber: number,
    pageSize: number,
    totalElements: number,
    totalPages: number
}

export interface PageInfoMapper extends Mapper {
    numberOfElements: boolean,
    pageNumber: boolean,
    pageSize: boolean,
    totalElements: boolean,
    totalPages: boolean
}