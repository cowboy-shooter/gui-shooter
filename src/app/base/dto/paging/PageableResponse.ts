import {PageInfo, PageInfoMapper} from "./PageInfo";
import {Mapper} from "../Mapper";

export interface PageableResponse<T> {
    content: T[],
    pageInfo: PageInfo
}

export interface PageableResponseMapper<T> extends Mapper {
    content: T,
    pageInfo: PageInfoMapper
}