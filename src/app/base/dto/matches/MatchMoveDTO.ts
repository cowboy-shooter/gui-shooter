import {TeamMatchStateDTO, TeamMatchStateDTOMapper} from "./TeamMatchStateDTO";
import {TeamMoveDTO, TeamMoveDTOMapper} from "./TeamMoveDTO";

export interface MatchMoveDTO {
    id: string,
    team1Move: TeamMoveDTO,
    team2Move: TeamMoveDTO,
    team1State: TeamMatchStateDTO,
    team2State: TeamMatchStateDTO
}

export interface MatchMoveDTOMapper {
    id?: boolean,
    team1Move?: TeamMoveDTOMapper,
    team2Move?: TeamMoveDTOMapper,
    team1State?: TeamMatchStateDTOMapper,
    team2State?: TeamMatchStateDTOMapper,
}