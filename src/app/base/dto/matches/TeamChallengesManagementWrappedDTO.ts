import {TeamDTO, TeamDTOMapper} from "../teams/TeamDTO";
import {Mapper} from "../Mapper";

export interface TeamChallengesManagementWrappedDTO {
    teamChallengesType: TeamChallengesManagementType,
    team: TeamDTO
}

export interface TeamChallengesManagementDTOMapper extends Mapper {
    teamChallengesType: true,
    team: TeamDTOMapper
}

export enum TeamChallengesManagementType {
    NONE = 'NONE', CHALLENGED = 'CHALLENGED'
}