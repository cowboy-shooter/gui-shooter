export interface TeamMatchStateDTO {
    id: string,
    bullets: number,
    blockCounter: number,
    evadeCounter: number,
    isDead: boolean
}

export interface TeamMatchStateDTOMapper {
    id?: boolean,
    bullets?: boolean,
    blockCounter?: boolean,
    evadeCounter?: boolean,
    isDead?: boolean
}