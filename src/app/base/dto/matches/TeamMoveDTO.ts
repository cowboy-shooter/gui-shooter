export interface TeamMoveDTO {
    id: string,
    teamMove: TeamMoveType,
    faulty: boolean,
    message: string
}

export interface TeamMoveDTOMapper {
    id?: boolean,
    teamMove?: boolean,
    faulty?: boolean,
    message?: boolean,
}

export enum TeamMoveType {
    BLOCK = 'BLOCK',
    SHOOT = 'SHOOT',
    EVADE = 'EVADE',
    RELOAD = 'RELOAD',
    NULL = 'NULL'
}