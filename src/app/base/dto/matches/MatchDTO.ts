import {TeamDTO, TeamDTOMapper} from "../teams/TeamDTO";
import {MatchMoveDTO, MatchMoveDTOMapper} from "./MatchMoveDTO";
import {Mapper} from "../Mapper";

export interface MatchDTO {
    id: string,
    team1: TeamDTO,
    team2: TeamDTO,
    moves: MatchMoveDTO[],
    winner: TeamDTO,
    started: Date,
    finished: Date
}

export interface MatchDTOMapper extends Mapper {
    id?: boolean,
    team1?: TeamDTOMapper,
    team2?: TeamDTOMapper,
    moves?: MatchMoveDTOMapper,
    winner?: TeamDTOMapper,
    started?: boolean,
    finished?: boolean
}