import {Mapper} from "../Mapper";
import {TeamDTO, TeamDTOMapper} from "../teams/TeamDTO";
import {TournamentMatchDTO, TournamentMatchDTOMapper} from "./TournamentMatchDTO";

export interface TournamentTierDTO {
    id: string;
    executed: boolean;
    teams: TeamDTO[];
    tournamentMatches: TournamentMatchDTO[];
}

export interface TournamentTierDTOMapper extends Mapper {
    id?: boolean;
    executed?: boolean;
    teams?: TeamDTOMapper;
    tournamentMatches?: TournamentMatchDTOMapper;
}