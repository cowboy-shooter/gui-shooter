import {Mapper} from "../Mapper";
import {TeamDTO, TeamDTOMapper} from "../teams/TeamDTO";
import {TournamentTierDTO, TournamentTierDTOMapper} from "./TournamentTierDTO";

export interface TournamentDTO {
    id: string;
    name: string;
    started: Date;
    winner: TeamDTO;
    secondPlace: TeamDTO;
    firstTier: TournamentTierDTO;
    secondTier: TournamentTierDTO;
    thirdTier: TournamentTierDTO;
    fourthTier: TournamentTierDTO;
}

export interface TournamentDTOMapper extends Mapper {
    id?: boolean,
    name?: boolean,
    started?: boolean,
    winner?: TeamDTOMapper,
    secondPlace?: TeamDTOMapper,
    firstTier?: TournamentTierDTOMapper,
    secondTier?: TournamentTierDTOMapper,
    thirdTier?: TournamentTierDTOMapper,
    fourthTier?: TournamentTierDTOMapper
}