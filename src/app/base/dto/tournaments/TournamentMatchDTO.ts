import {Mapper} from "../Mapper";
import {TeamDTO, TeamDTOMapper} from "../teams/TeamDTO";
import {PlannedMatchDTO, PlannedMatchDTOMapper} from "./PlannedMatchDTO";

export interface TournamentMatchDTO {
    id: string;
    executed: boolean;
    team1Score: number;
    team2Score: number;
    team1: TeamDTO;
    team2: TeamDTO;
    plannedMatches: PlannedMatchDTO[];
}

export interface TournamentMatchDTOMapper extends Mapper {
    id?: boolean;
    executed?: boolean;
    team1Score?: boolean;
    team2Score?: boolean;
    team1?: TeamDTOMapper;
    team2?: TeamDTOMapper;
    plannedMatches?: PlannedMatchDTOMapper;
}