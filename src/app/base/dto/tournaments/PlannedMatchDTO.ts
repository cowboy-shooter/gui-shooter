import {Mapper} from "../Mapper";
import {TeamDTO, TeamDTOMapper} from "../teams/TeamDTO";
import {MatchDTO, MatchDTOMapper} from "../matches/MatchDTO";

export interface PlannedMatchDTO {
    id: string;
    team1: TeamDTO;
    team2: TeamDTO;
    executedMatch: MatchDTO;
}

export interface PlannedMatchDTOMapper extends Mapper {
    id?: boolean;
    team1?: TeamDTOMapper;
    team2?: TeamDTOMapper;
    executedMatch?: MatchDTOMapper;
}