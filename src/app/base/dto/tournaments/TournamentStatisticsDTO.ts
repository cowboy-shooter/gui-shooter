import {Mapper} from "../Mapper";
import {TeamDTO, TeamDTOMapper} from "../teams/TeamDTO";
import {MatchDTO, MatchDTOMapper} from "../matches/MatchDTO";

export interface TournamentStatisticsDTO {
    failResponses: {},
    bulletsShot: {},
    evades: {},
    blocks: {},
    reloads: {},
    unsuccessfulBlocks: {},
    unsuccessfulReloads: {},
    unsuccessfulEvades: {},
    unsuccessfulShots: {},
    wins: {},
    loses: {},
    draws: {},
    blockedShots: {},
    evadedShots: {},
    score: {}
}

export interface TournamentStatisticsDTOMapper extends Mapper {
    failResponses?: boolean,
    bulletsShot?: boolean,
    evades?: boolean,
    blocks?: boolean,
    reloads?: boolean,
    unsuccessfulBlocks?: boolean,
    unsuccessfulReloads?: boolean,
    unsuccessfulEvades?: boolean,
    unsuccessfulShots?: boolean,
    wins?: boolean,
    loses?: boolean,
    draws?: boolean,
    blockedShots?: boolean,
    evadedShots?: boolean,
    score?: boolean
}