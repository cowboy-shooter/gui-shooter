import {UserDTO, UserDTOMapper} from "./UserDTO";
import {Mapper} from "../Mapper";

export interface UserTeamWrappedUserDTO {
    teamUserManagementType: UserTeamWrappedUserType,
    user: UserDTO
}

export interface UserTeamWrapperUserDTOMapper extends Mapper {
    teamUserManagementType: boolean,
    user: UserDTOMapper
}

export enum UserTeamWrappedUserType {
    INVITED = 'INVITED', REQUESTOR = 'REQUESTOR', NONE = 'NONE'
}