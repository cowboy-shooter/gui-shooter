import {Mapper} from "../Mapper";

export interface UserDTO {
    id?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    username?: string;
}

export interface UserDTOMapper extends Mapper {
    id?: boolean;
    firstName?: boolean;
    lastName?: boolean;
    email?: boolean;
    username?: boolean;
}