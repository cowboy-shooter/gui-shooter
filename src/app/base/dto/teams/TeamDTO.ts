import {UserDTO, UserDTOMapper} from "./UserDTO";
import {Mapper} from "../Mapper";

export interface TeamDTO {
    id: string;
    name: string;
    url: string;
    language: string;
    description: string;
    members: UserDTO[];
    requestors: UserDTO[];
    invitees: UserDTO[];
    leader: UserDTO;
    flag: string;
}

export interface TeamDTOMapper extends Mapper {
    id?: boolean,
    name?: boolean,
    url?: boolean,
    language?: boolean,
    description?: boolean,
    members?: UserDTOMapper,
    requestors?: UserDTOMapper,
    invitees?: UserDTOMapper,
    leader?: UserDTOMapper,
    flag?: boolean
}