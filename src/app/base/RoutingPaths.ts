export enum RoutingPaths {
    HOME = '/',
    MATCHES = '/matches',
    TEAMS = '/teams',
    ABOUT = '/about'
}