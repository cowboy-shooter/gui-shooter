import {Component} from 'react'
import {TeamsService} from "../../service/TeamsService";
import {UsersService} from "../../service/UsersService";
import {TeamUserService} from "../../service/TeamUserService";
import {TeamChallengesManagementService} from "../../service/TeamChallengesManagementService";
import {CommonService} from "../../service/CommonService";
import {MatchService} from "../../service/MatchService";
import {TournamentsService} from "../../service/TournamentsService";
import {TournamentsStatisticsService} from "../../service/TournamentsStatisticsService";


export class ServiceComponent<P = {}, S = {}> extends Component<P,S> {

    protected teamsService = new TeamsService();
    protected userService = new UsersService();
    protected teamUserService = new TeamUserService();
    protected teamChallengesManagementService = new TeamChallengesManagementService();
    protected commonService = new CommonService();
    protected matchService = new MatchService();
    protected tournamentsService = new TournamentsService();
    protected tournamentsStatisticsService = new TournamentsStatisticsService();

}

export default ServiceComponent