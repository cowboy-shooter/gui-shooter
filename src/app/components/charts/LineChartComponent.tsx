import * as React from "react";
import {Component} from "react";
import {CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis} from "recharts";


interface LineChartComponentProps {
    data: LineChartData[],
    line1Name: string,
    line2Name: string
}

interface LineChartComponentState {
    fixedData: {}[]
}

export interface LineChartData {
    name: string,
    value1: number,
    value2: number
}

class LineChartComponent extends Component<LineChartComponentProps, LineChartComponentState> {

    constructor(props: LineChartComponentProps) {
        super(props);
        this.state = this.fixState();
    }

    fixState():LineChartComponentState {
        return {
            fixedData: this.props.data.map(
                item => {
                    let result = {name: item.name};
                    // @ts-ignore
                    result[this.props.line1Name] = item.value1;
                    // @ts-ignore
                    result[this.props.line2Name] = item.value2;

                    return result;
                }
            )
        };
    }

    componentDidMount(): void {

    }


    render() {
        return (
            <LineChart
                width={500}
                height={300}
                data={this.state.fixedData}
                style={{margin: "auto"}}
                margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey={this.props.line1Name} stroke="#8884d8" strokeWidth={3} strokeDasharray="5 5" />
                <Line type="monotone" dataKey={this.props.line2Name} stroke="#82ca9d" strokeWidth={3} strokeDasharray="3 4 5 2" />
            </LineChart>
        );
    }
}


export default LineChartComponent
