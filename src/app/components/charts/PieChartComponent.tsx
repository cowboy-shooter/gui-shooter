import * as React from "react";
import {Component} from "react";
import {Cell, Pie, PieChart, Sector} from 'recharts';


interface PieChartComponentProps {
    data?: ChartData[],
    dynamicData?: {}
    title?: string,
    prefab: PieChartComponentPrefabs
}

interface PieChartComponentState {
    data: ChartDataFixed[],
    dataActiveIndex: number
}

export enum PieChartComponentPrefabs {
    SMALL, BIG
}

export interface ChartData {
    name: string,
    value: number,
    color: string
}

interface ChartDataFixed {
    name: string,
    value: number,
    color: string,
    title?: string,
    middleTextStyles: {}
}

const renderActiveShape = (props: any) => {
    const RADIAN = Math.PI / 180;
    const {
        cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
        fill, payload, percent
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={180} y={60}  dy={8} textAnchor="middle" fill={"white"} style={TitleTextStyle}>{payload.title}</text>
            <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill} style={payload.middleTextStyles}>{payload.name.split(' ').join('\n\r')}</text>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={fill}
            />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
            />
            <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
            <text x={ex + (cos >= 0 ? 1 : -1) * 10} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
                {`${(percent * 100).toFixed(2)}%`}
            </text>
        </g>
    );
};

class PieChartComponent extends Component<PieChartComponentProps, PieChartComponentState> {

    constructor(props: PieChartComponentProps) {
        super(props);
        this.state = {
            data: [],
            dataActiveIndex: 0
        };
    }

    componentDidMount(): void {
        if (this.props.data) {
            this.setState({
                data: this.fixData(this.props.data)
            });
        } else if (this.props.dynamicData) {
            this.setState({
                data: this.fixDynamicData(this.props.dynamicData)
            });
        }

    }

    fixData(data: ChartData[]): ChartDataFixed[] {
        let middleTitleTextStyle = {};
        switch(this.props.prefab) {
            case PieChartComponentPrefabs.BIG:
                middleTitleTextStyle = {
                    fontSize: "15px",
                    fontWeight: "bold" as "bold"
                };
                break;
            case PieChartComponentPrefabs.SMALL:
                middleTitleTextStyle = {
                    fontSize: "12px",
                };
                break;
        }

        return data.map(
            item => {
                return {name: item.name, value: item.value, color: item.color, title: this.props.title, middleTextStyles: middleTitleTextStyle}
            }
        )
    }

    fixDynamicData(data: {}): ChartDataFixed[] {
        const {title} = this.props;

        // @ts-ignore
        let chartData: ChartDataFixed[] = Object.keys(data).map(function(index){
            return {
                name: index,
                title: title,
                // @ts-ignore
                value: data[index],
                color: "rgb(" + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + ")"
            }
        });

        return chartData;
    }

    onSectionEnter = (data: any, index: number) => {
        this.setState({
            dataActiveIndex: index,
        });
    };

    render() {
        let width = 0;
        let height = 0;

        let positionX = 0;
        let positionY = 0;

        let innerRadius = 0;
        let outerRadius = 0;

        switch(this.props.prefab) {
            case PieChartComponentPrefabs.BIG:
                width = 400;
                height = 400;
                positionX = 200;
                positionY = 200;
                innerRadius = 60;
                outerRadius = 80;
                break;
            case PieChartComponentPrefabs.SMALL:
                width = 340;
                height = 230;
                positionX = 170;
                positionY = 100;
                innerRadius = 40;
                outerRadius = 60;
                break;
        }

        return (
            <div>
                <PieChart width={width} height={height}>
                    <Pie
                        activeIndex={this.state.dataActiveIndex}
                        activeShape={renderActiveShape}
                        blendStroke={true}
                        data={this.state.data}
                        cx={positionX}
                        cy={positionY}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius}
                        fill="black"
                        stroke={"black"}
                        dataKey="value"
                        onMouseEnter={this.onSectionEnter}>
                        {
                            this.state.data.map((entry, index) => <Cell key={`cell-${index}`} fill={entry.color} stroke={0} strokeDasharray={null} strokeOpacity={0}  />)
                        }
                    </Pie>
                </PieChart>
            </div>
        )
    }
}

const TitleTextStyle = {
    fontSize: "30px",
    fontWeight: "bold" as "bold"
};

export default PieChartComponent
