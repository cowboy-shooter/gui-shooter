import * as React from "react";
import ServiceComponent from "../service/ServiceComponent";
import {Table} from "semantic-ui-react";
import {Sorting, SortingDirection} from "../../base/dto/paging/Sorting";
import {faCaretDown, faCaretUp} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export interface SortableHeaderCellProps {
    value: string,
    sortBy: string,
    sorting: Sorting,
    changeSorting: (sorting: Sorting) => void
}

interface SortableHeaderCellState {
}

class SortableHeaderCell extends ServiceComponent<SortableHeaderCellProps, SortableHeaderCellState> {

    changeSorting = () => {
        let direction = this.props.sorting.sortDirection;

        if (this.props.sorting.sortBy !== this.props.sortBy || direction === SortingDirection.ASC) {
            direction = SortingDirection.DESC;
        } else {
            direction = SortingDirection.ASC;
        }
        this.props.changeSorting({
            sortBy: this.props.sortBy,
            sortDirection: direction
        });
    };

    render() {
        const {value, sortBy, sorting} = this.props;

        let icon = (<div/>);
        let textStyle;

        if (sorting.sortBy === sortBy && sorting.sortDirection === SortingDirection.DESC) {
            icon = <FontAwesomeIcon icon={faCaretDown} style={IconStyle}/>;
            textStyle = {
                color: "rgba(54, 132, 45, 0.88)"
            };
        } else if (sorting.sortBy === sortBy && sorting.sortDirection === SortingDirection.ASC) {
            icon = <FontAwesomeIcon icon={faCaretUp} style={IconStyle}/>;
            textStyle = {
                color: "rgba(54, 132, 45, 0.88)"
            };
        }

        return (<Table.HeaderCell onClick={() => {this.changeSorting()}} style={HeaderCellStyle}><h2 style={textStyle}>{value} {icon}</h2></Table.HeaderCell>)
    }

}

const HeaderCellStyle = {
    cursor: "pointer"
};

const IconStyle = {
    color: "#67ad6773"
};

export default SortableHeaderCell
