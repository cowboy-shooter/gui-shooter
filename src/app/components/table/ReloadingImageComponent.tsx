import * as React from "react";
import ServiceComponent from "../service/ServiceComponent";
import {SemanticSIZES} from "semantic-ui-react/dist/commonjs/generic";
import {Loader} from "semantic-ui-react";
// @ts-ignore
import Image from 'react-image-resizer';

export interface ReloadingImageComponentProps {
    image: string,
    loaderSize: SemanticSIZES,
    imageHeight?: number
    imageWidth?: number
}

interface ReloadingImageComponentState {

}

class ReloadingImageComponent extends ServiceComponent<ReloadingImageComponentProps, ReloadingImageComponentState> {

    render() {
        const {image, imageHeight, imageWidth} = this.props;

        return (
            !image || image === '' ? (<Loader active={true} style={loadingRow} size='medium'></Loader>)
                :(<Image
                    src={image}
                    height={ imageHeight }
                    width={ imageWidth }
                />)
        )
    }

}

const loadingRow = {
    position: "relative",
    marginTop: "20px"
};

export default ReloadingImageComponent
