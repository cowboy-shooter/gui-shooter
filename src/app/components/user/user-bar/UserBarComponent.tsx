import React from 'react'
import UserGravatarButton from "./UserGravatarButton";
import ServiceComponent from "../../service/ServiceComponent";

export class UserBarComponent extends ServiceComponent {

    render() {
        return (
            <UserGravatarButton />
        )
    }
}

export default UserBarComponent