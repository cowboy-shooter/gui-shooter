import React, {Component} from 'react'
import {Dropdown} from "semantic-ui-react";
import Gravatar from 'react-gravatar';
import {KeycloakProfile} from "keycloak-js";
import RootComponent from "../../../RootComponent";
import {UserUtils} from "../../../utils/UserUtils";

export interface GravatarButtonState {
    hover: boolean
}

export class UserGravatarButton extends Component<{}, GravatarButtonState> {

    constructor(props: {}) {
        super(props);
        this.state = {
            hover: false
        }
    }

    onMouseOver = () => {
        this.setState({hover: true});
    };

    onMouseLeave = () => {
        this.setState({hover: false});
    };

    render() {
        let profile: KeycloakProfile = UserUtils.getUserProfile();
        const options = [
            {
                key: 'user',
                text: (
                    <span>Signed in as <strong>{profile.firstName} {profile.lastName}</strong></span>
                ),
                disabled: true,
            },
            { key: 'profile', text: (<a style={{color: "black"}} href={"/account"}>Your Profile</a>) },
            { key: 'sign-out', text: (<div style={{color: "black"}} onClick={() => RootComponent.keycloak.logout()}>Sign Out</div>) },
        ];

        return (
            <Dropdown
                style={DropdownStyle}
                trigger={this.getTrigger(profile)}
                options={options}
                pointing='top right'
                icon={null}
                onMouseOver={this.onMouseOver}
                onMouseLeave={this.onMouseLeave}
            />
        )
    }

    getTrigger(profile: KeycloakProfile) {

        let imageStyle;
        if (this.state.hover) {
            imageStyle = {
                ...ImageStyle,
                borderColor: "#989864"
            };
        } else {
            imageStyle = {
                ...ImageStyle,
                borderColor: "transparent"
            };
        }

        return (
                <Gravatar size={100} email={profile.email} style={imageStyle} onMouseOver={this.onMouseOver} onMouseLeave={this.onMouseLeave}/>
            );
    }
}

const ImageStyle = {
    borderRadius: "50%",
    transition: "all 0.5s ease",
    border: "solid"
};

const DropdownStyle = {
    float: "right"
};

export default UserGravatarButton