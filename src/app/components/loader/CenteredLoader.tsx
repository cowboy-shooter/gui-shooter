import React, {Component} from 'react'
import {Loader} from "semantic-ui-react";


export class CenteredLoader extends Component {

    render() {
        return (<Loader style={LoaderStyle} size='massive'>Loading</Loader>);
    }
}

const LoaderStyle: React.CSSProperties = {
    margin: "auto",
    top: "40vh"
};

export default CenteredLoader