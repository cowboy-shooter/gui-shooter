import {ButtonColors} from "../../enums/ButtonColors";
import {IconDefinition} from "@fortawesome/fontawesome-svg-core";
import {IconButtonTypes} from "./IconButtonPrefabs";

export interface IconButtonProps {
    color: ButtonColors;
    icon: IconDefinition;
    text: string;
    type: IconButtonTypes;
    buttonStyle?: {},
    disabled?: boolean,
    disabledColor?: string
}
