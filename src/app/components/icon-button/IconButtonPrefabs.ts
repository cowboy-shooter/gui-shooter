import {SemanticSIZES} from "semantic-ui-react/dist/commonjs/generic";

export enum IconButtonTypes {
    MASSIVE,
    SMALL
}

export interface IconButtonSpecs {
    imageStyles: any,
    buttonStyles: any,
    buttonSemanticSize: SemanticSIZES,
    blur: number
}

export class IconButtonPrefabs {
    public static getPrefab(type: IconButtonTypes): IconButtonSpecs {
        switch (type) {
            case IconButtonTypes.MASSIVE:
                return {
                    imageStyles: {
                        maxHeight: "70%",
                        maxWidth: "70%",
                        height: "100%",
                        width: "100%",
                        transition: "all 0.5s ease",
                        padding: "20px"
                    }, buttonStyles: {
                        height: "20vh",
                        textAlign: "center",
                        padding: "0px",
                        minHeight: "110px"
                    },
                    buttonSemanticSize: "massive",
                    blur: 8};
            case IconButtonTypes.SMALL:
                return {
                    imageStyles: {
                        maxHeight: "70%",
                        maxWidth: "70%",
                        height: "100%",
                        width: "100%",
                        transition: "all 0.5s ease",
                        padding: "20px"
                    }, buttonStyles: {
                        height: "10vh",
                        textAlign: "center",
                        padding: "0px",
                        minHeight: "110px"
                    },
                    buttonSemanticSize: "small",
                    blur: 5};
        }
    }
}