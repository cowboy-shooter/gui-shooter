import React, {Component} from 'react'
import {Button} from "semantic-ui-react";
import {IconButtonProps} from "./IconButtonProps";
import {IconButtonState} from "./IconButtonState";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {IconButtonPrefabs} from "./IconButtonPrefabs";

export class IconButton<P extends IconButtonProps, S extends IconButtonState> extends Component<P, S> {

    onMouseOver = () => {
        this.setState({hover: true});
    };

    onMouseLeave = () => {
        this.setState({hover: false});
    };

    onClick? = () => {};

    render() {
        const {color, icon, text, type, disabled} = this.props;

        let iconButtonSpecs = IconButtonPrefabs.getPrefab(type);

        let imageStyle;
        if (this.state.hover) {
            imageStyle = {
                ...iconButtonSpecs.imageStyles,
                filter: "blur(0px)"
            };
        } else {
            imageStyle = {
                ...iconButtonSpecs.imageStyles,
                filter: "blur(" + iconButtonSpecs.blur + "px)"
            };
        }

        let style = iconButtonSpecs.buttonStyles;
        if (this.props.buttonStyle) {
            style = this.props.buttonStyle;
        }

        if (disabled) {
            return (
                <Button size={iconButtonSpecs.buttonSemanticSize} fluid basic inverted style={style} disabled={true}>
                    <FontAwesomeIcon icon={icon} style={{
                        ...iconButtonSpecs.imageStyles,
                        color: this.props.disabledColor
                    }}/>
                    <div style={{color: this.props.disabledColor}}>{text}</div>
                </Button>
            )
        }

        if (this.onClick) {
            return (
                <Button size={iconButtonSpecs.buttonSemanticSize} fluid basic inverted color={color} style={style} onMouseOver={this.onMouseOver} onMouseLeave={this.onMouseLeave} onClick={this.onClick}>
                    <FontAwesomeIcon icon={icon} style={imageStyle}/>
                    <div>{text}</div>
                </Button>
            )
        } else {
            return (
                <Button size={iconButtonSpecs.buttonSemanticSize} fluid basic inverted color={color} style={style} onMouseOver={this.onMouseOver} onMouseLeave={this.onMouseLeave}>
                    <FontAwesomeIcon icon={icon} style={imageStyle}/>
                    <div>{text}</div>
                </Button>
            )
        }


    }
}

export default IconButton
