import React, {ChangeEvent, Component, RefObject} from 'react'
import {Button, Image} from "semantic-ui-react";

export interface ImagePickerButtonProps {
    onValueChange: (imageBase64: string) => void,
    file?: string
}

export interface ImagePickerButtonState {
    hover: boolean,
    file?: string
}

export class ImagePickerButton extends Component<ImagePickerButtonProps, ImagePickerButtonState> {

    private fileInputRef:RefObject<any> = React.createRef<any>();

    constructor(props: ImagePickerButtonProps) {
        super(props);
        this.state = {
            hover: false,
            file: props.file
        };
    }

    fileChange = (e: ChangeEvent<HTMLElement>) => {
        // @ts-ignore
        this.loadFile(e.target['files'][0]);
    };

    loadFile(file: File) {
        if (!file.type.includes('image')) {
            console.log('Not an image');
            return;
        }

        let thisInstance: ImagePickerButton = this;
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            thisInstance.setState({
                file: reader.result as string
            });
            thisInstance.props.onValueChange(reader.result as string);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    render() {

        let img = null;
        if (this.state.file) {
            img = (
                <Image
                    src={this.state.file}
                    style={ImageStyle}
                />
            );
        }
        let text = null;
        if (!img) {
            text = (<h3>Pick Image</h3>);
        }

        return (
            <div>
                <Button type={"button"} fluid basic inverted color={"yellow"} onClick={() => this.fileInputRef.current.click()} style={ButtonStyle}>
                    {
                        img
                    }
                    {
                        text
                    }
                </Button>
                <input
                    ref={this.fileInputRef}
                    type="file"
                    hidden
                    onChange={this.fileChange}
                />
            </div>
        )
    }

}

const ImageStyle = {
    // maxHeight: "200px",
    maxWidth: "150px",
    // width: "auto",
    // margin: "auto"
};

const ButtonStyle = {
    // maxWidth: "50%",
    // width: "50%",
    // height: "300px",
    // margin: "auto"
};

export default ImagePickerButton
