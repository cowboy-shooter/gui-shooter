import {RoutingPaths} from "../../base/RoutingPaths";

export interface NavigationComponentProps {
    path: RoutingPaths;
}