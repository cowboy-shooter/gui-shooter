import React, {Component} from 'react'
import {Grid} from "semantic-ui-react";
import {NavigationButtonComponent, NavigationButtonProps} from "./navigation-button/NavigationButtonComponent";
import {ButtonColors} from "../../enums/ButtonColors";
import {faFire, faMap, faUsers, faHouseDamage} from "@fortawesome/free-solid-svg-icons";
import {RoutingPaths} from "../../base/RoutingPaths";
import {IconButtonTypes} from "../icon-button/IconButtonPrefabs";
import {NavigationComponentProps} from "./NavigationComponentProps";

export class NavigationComponent extends Component<NavigationComponentProps> {

    navigationButtons: NavigationButtonProps[] = [
        {
            text: "Home",
            color: ButtonColors.YELLOW,
            icon: faHouseDamage,
            redirectUri: RoutingPaths.HOME,
            type: IconButtonTypes.SMALL,
            disabledColor: "#dcdc2b"
        }, {
            text: "Matches",
            color: ButtonColors.ORANGE,
            icon: faFire,
            redirectUri: RoutingPaths.MATCHES,
            type: IconButtonTypes.SMALL,
            disabledColor: "rgba(228, 155, 107, 0.35)"
        }, {
            text: "Teams",
            color: ButtonColors.GREEN,
            icon: faUsers,
            redirectUri: RoutingPaths.TEAMS,
            type: IconButtonTypes.SMALL,
            disabledColor: "rgba(143, 245, 143, 0.35)"
        }, {
            text: "About",
            color: ButtonColors.BLUE,
            icon: faMap,
            redirectUri: RoutingPaths.ABOUT,
            type: IconButtonTypes.SMALL,
            disabledColor: "rgb(171, 201, 255)"
        }
    ];

    render() {
        const {path} = this.props;

        let buttonElements: JSX.Element[] = [];

        this.navigationButtons.forEach(
            (button) => {
                if (path !== button.redirectUri) {
                    buttonElements.push (
                        <Grid.Column style={ColumnStyle}>
                            <NavigationButtonComponent color={button.color} icon={button.icon} text={button.text} redirectUri={button.redirectUri} type={button.type} />
                        </Grid.Column>
                    );
                } else {

                    buttonElements.push (
                        <Grid.Column style={ColumnStyle}>
                            <NavigationButtonComponent color={button.color} icon={button.icon} text={button.text} disabled={true} disabledColor={button.disabledColor} type={button.type} />
                        </Grid.Column>
                    );
                }
            }
        );

        return (
            <Grid columns={4} style={GridStyle}>
                <Grid.Row>
                    {buttonElements}
                </Grid.Row>
            </Grid>
        )
    }
}

const GridStyle = {
    width: "70%",
    margin: "auto",
    minWidth: "30%"
};

const ColumnStyle = {
    minWidth: "150px"
};

export default NavigationComponent
