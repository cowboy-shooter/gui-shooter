import React from 'react'
import {IconButtonState} from "../../icon-button/IconButtonState";
import IconButton from "../../icon-button/IconButton";
import {Redirect} from "react-router";
import {IconButtonProps} from "../../icon-button/IconButtonProps";


export interface NavigationButtonProps extends IconButtonProps {
    redirectUri?: string;
}

interface HomeRoutingButtonState extends IconButtonState {
    redirect: boolean
}

export class NavigationButtonComponent extends IconButton<NavigationButtonProps, HomeRoutingButtonState> {

    constructor(props: NavigationButtonProps) {
        super(props);
        this.state = {
            hover: false,
            redirect: false
        }
    }

    onClick = () => {
        this.setState({redirect: true});
    };


    render() {
        const {redirectUri, disabled} = this.props;

        if (this.state.redirect === true && redirectUri && !disabled) {
            return <Redirect to={redirectUri} />;
        }

        return super.render();
    }
}

export default NavigationButtonComponent