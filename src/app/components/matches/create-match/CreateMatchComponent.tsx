import * as React from "react";
import {Modal, Pagination, PaginationProps, Table} from "semantic-ui-react";
import {PageInfo} from "../../../base/dto/paging/PageInfo";
import {Page} from "../../../base/dto/paging/Page";
// @ts-ignore
import Image from 'react-image-resizer';
import {
    TeamChallengesManagementDTOMapper,
    TeamChallengesManagementType,
    TeamChallengesManagementWrappedDTO
} from "../../../base/dto/matches/TeamChallengesManagementWrappedDTO";
import {TeamDTO} from "../../../base/dto/teams/TeamDTO";
import {Properties, Settings} from "../../../../Settings";
import ServiceComponent from "../../service/ServiceComponent";
import {SortingDirection} from "../../../base/dto/paging/Sorting";

interface CreateMatchState {
    teamsWrapper: TeamChallengesManagementWrappedDTO[],
    pageInfo?: PageInfo
}

class CreateMatchComponent extends ServiceComponent<{}, CreateMatchState> {

    private page: Page;

    constructor(props: {}) {
        super(props);
        this.state = {
            teamsWrapper: []
        };
        this.page = {
            page: 0,
            size: Settings.getProperty(Properties.CREATE_MATCH_TEAMS_PAGE_SIZE) as number,
            sorting: {
                sortBy: 'started',
                sortDirection: SortingDirection.DESC
            }
        };
    }

    componentDidMount(): void {
        this.reload();
    }

    reload(): void {
        this.reloadTeams();
    }

    reloadTeams() {
        let mapper: TeamChallengesManagementDTOMapper = {
            team: {
                id: true,
                name: true,
                flag: true
            }, teamChallengesType: true
        };

        this.teamChallengesManagementService.getOwnChallengesTeamManagement(this.page, mapper).then(result => {
            this.setState({
                teamsWrapper: result.data.content,
                pageInfo: result.data.pageInfo
            })
        });
    }

    cancelChallenge(team: TeamDTO) {
        this.teamsService.cancelChallenge(team).then(result => {
            this.reloadTeams();
        });
    }

    challengeTeam(team: TeamDTO) {
        this.teamsService.challengeTeam(team).then(result => {
            this.reloadTeams();
        });
    }


    onChange = (event: React.MouseEvent, pageInfo: PaginationProps) => {
        this.page = {
            page: pageInfo.activePage as number - 1,
            size: this.page.size,
            sorting: this.page.sorting
        };
        this.reloadTeams();
    };

    render() {
        return (
            <div style={ButtonStyle}>
                <Modal
                    trigger={
                        <button className="ui inverted orange basic button">Challenge Team</button>
                    }
                    basic style={ModalStyle}>
                    <Table celled inverted basic='very'>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell><h2>Name</h2></Table.HeaderCell>
                                <Table.HeaderCell><h2>Flag</h2></Table.HeaderCell>
                                <Table.HeaderCell><h2>Options</h2></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {
                                this.state.teamsWrapper.map((wrapper) => (
                                    <Table.Row>
                                        <Table.Cell>{wrapper.team.name}</Table.Cell>
                                        <Table.Cell>
                                            <Image
                                                src={wrapper.team.flag}
                                                height={ 50 }
                                                width={ 100 }
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            {
                                                wrapper.teamChallengesType === TeamChallengesManagementType.CHALLENGED ?
                                                    <button onClick={() => this.cancelChallenge(wrapper.team)} className="ui inverted red basic button">Cancel</button>
                                                    : <button onClick={() => this.challengeTeam(wrapper.team)} className="ui inverted orange basic button">Challenge</button>
                                            }
                                        </Table.Cell>
                                    </Table.Row>

                                ))
                            }
                        </Table.Body>
                    </Table>
                    {
                        this.state.pageInfo ? (
                            <Pagination
                                defaultActivePage={1}
                                siblingRange={3}
                                totalPages={this.state.pageInfo.totalPages}
                                onPageChange={this.onChange}
                                pointing
                                secondary
                                inverted
                            />
                        ) : null
                    }
                </Modal>
            </div>
        )
    }

}

const ModalStyle = {
    textAlign: 'center' as 'center',
    top: "20%",
    width: "20%"
};

const ButtonStyle = {
    height: "100%",
    textAlign: "center" as "center",
    float: "right" as "right"
};

export default CreateMatchComponent
