import * as React from "react";
import {MatchDTO, MatchDTOMapper} from "../../../base/dto/matches/MatchDTO";
import ServiceComponent from "../../service/ServiceComponent";
import {Grid, Menu, Tab, TabProps} from "semantic-ui-react";
import DetailsMatchComponent from "./tabs/DetailsMatchComponent";
import ReplayMatchComponent from "./tabs/ReplayMatchComponent";
import TeamCard from "../../teams/card/TeamCard";
import MatchStatisticsComponent from "./tabs/MatchStatisticsComponent";
import CenteredLoader from "../../loader/CenteredLoader";


export interface MatchDetailProps {
    matchId: string
}

interface MatchDetailState {
    match?: MatchDTO,
    tabIndex: number
}

export class MatchDetailComponent extends ServiceComponent<MatchDetailProps, MatchDetailState> {


    constructor(props: MatchDetailProps) {
        super(props);
        this.state = {
            tabIndex: 0
        };
    }

    componentDidMount() {
        this.reloadMatch();
    }

    reloadMatch = () => {
        let matchMapper: MatchDTOMapper = {
            id: true,
            team1: {
                name: true,
                description: true,
                language: true,
                flag: true
            }, team2: {
                name: true,
                description: true,
                language: true,
                flag: true
            }, winner: {
                name: true,
                description: true,
                language: true,
                flag: true
            }, moves: {
                team1Move: {
                    teamMove: true,
                    faulty: true,
                    message: true
                },
                team2Move: {
                    teamMove: true,
                    faulty: true,
                    message: true
                },
                team1State: {
                    bullets: true,
                    blockCounter: true,
                    evadeCounter: true,
                    isDead: true
                },
                team2State: {
                    bullets: true,
                    blockCounter: true,
                    evadeCounter: true,
                    isDead: true
                }
            }
        };
        this.matchService.getMatch(this.props.matchId, matchMapper).then(result => {
            this.setState({
                match: result.data
            })
        });
    };

    onTabChange = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, data: TabProps) => {
        this.setState({
            tabIndex: data['activeIndex'] as number
        });
    };

    render() {
        const {match} = this.state;

        if (!match) {
            return  (
                <CenteredLoader/>
            )
        }

        let tabs = [];
        tabs.push(
            {
                menuItem: (
                    <Menu.Item key='members' style={this.state.tabIndex === tabs.length ? activeTabStyle: defaultTabStyle}>
                        Details
                    </Menu.Item>
                ),
                render: () => (
                    <Tab.Pane style={PanelContentStyle}>
                        <DetailsMatchComponent match={match}/>
                    </Tab.Pane>
                ),
            }
        );

        tabs.push(
            {
                menuItem: (
                    <Menu.Item key='management' style={this.state.tabIndex === tabs.length ? activeTabStyle : defaultTabStyle}>
                        Replay
                    </Menu.Item>
                ),
                render: () => (
                    <Tab.Pane style={PanelContentStyle}>
                        <ReplayMatchComponent match={match}/>
                    </Tab.Pane>
                )
            }
        );

        tabs.push(
            {
                menuItem: (
                    <Menu.Item key='statistics' style={this.state.tabIndex === tabs.length ? activeTabStyle : defaultTabStyle}>
                        Statistics
                    </Menu.Item>
                ),
                render: () => (
                    <Tab.Pane style={PanelContentStyle}>
                        <MatchStatisticsComponent match={match}/>
                    </Tab.Pane>
                )
            }
        );

        return (
            <Grid columns="1" textAlign={"center"}>
                <Grid.Row>
                    <Grid columns="3" textAlign={"center"}>
                            <Grid.Column>
                                <TeamCard team={match.team1} imageSize={300}/>
                            </Grid.Column>
                            <Grid.Column>
                                <TeamCard team={match.team2} imageSize={300}/>
                            </Grid.Column>
                    </Grid>
                </Grid.Row>
                <Grid.Row>
                    <Tab panes={tabs} onTabChange={this.onTabChange} style={semanticTabStyle}/>
                </Grid.Row>
            </Grid>
        );
    }
}

const PanelContentStyle = {
    backgroundColor: "transparent",
    border: "none",
    minWidth: "30%"
};

const defaultTabStyle = {
    color: "white",
    backgroundColor: "transparent",
};

const activeTabStyle = {
    color: "white",
    backgroundColor: "rgba(0, 86, 204, 0.33)",
};


const semanticTabStyle = {
    width: "100%"
};

export default MatchDetailComponent
