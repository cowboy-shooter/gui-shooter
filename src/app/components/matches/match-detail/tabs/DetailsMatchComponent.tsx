import * as React from "react";
import ServiceComponent from "../../../service/ServiceComponent";
import {MatchDTO} from "../../../../base/dto/matches/MatchDTO";
import {Grid, Header} from "semantic-ui-react";
import TeamCard from "../../../teams/card/TeamCard";
import {MatchMoveDTO} from "../../../../base/dto/matches/MatchMoveDTO";
import {TeamMoveDTO, TeamMoveType} from "../../../../base/dto/matches/TeamMoveDTO";

interface MatchDetailsProps {
    match: MatchDTO
}

interface TeamStatistics {
    bulletsLoaded: number,
    bulletsLeft: number,
    shotsTaken: number,
    emptyBlocks: number,
    successfulBlocks: number,
    emptyEvades: number,
    successfulEvades: number,
    faultyActions: number
}

interface GlobalStatistics {
    totalMoves: number
}

interface DetailsMatchComponentState {
    team1Statistics: TeamStatistics,
    team2Statistics: TeamStatistics,
    globalStatistics: GlobalStatistics
}

class DetailsMatchComponent extends ServiceComponent<MatchDetailsProps, DetailsMatchComponentState> {

    constructor(props: MatchDetailsProps) {
        super(props);
        this.state = this.assembleState();
    }

    assembleState(): DetailsMatchComponentState {
        let team1Statistics: TeamStatistics = {
            bulletsLoaded: 0,
            bulletsLeft: 0,
            shotsTaken: 0,
            emptyBlocks: 0,
            successfulBlocks: 0,
            emptyEvades: 0,
            successfulEvades: 0,
            faultyActions: 0
        };
        let team2Statistics: TeamStatistics = {
            bulletsLoaded: 0,
            bulletsLeft: 0,
            shotsTaken: 0,
            emptyBlocks: 0,
            successfulBlocks: 0,
            emptyEvades: 0,
            successfulEvades: 0,
            faultyActions: 0
        };
        let globalStats: GlobalStatistics = {
            totalMoves: 0
        };
        this.props.match.moves.forEach((move) => {
            team1Statistics = this.analyzeMove(move.team1Move, move.team2Move, team1Statistics);
            team2Statistics = this.analyzeMove(move.team2Move, move.team1Move, team2Statistics);
        });
        let lastMove: MatchMoveDTO = this.props.match.moves[this.props.match.moves.length - 1];
        team1Statistics.bulletsLeft = lastMove.team1State.bullets;
        team2Statistics.bulletsLeft = lastMove.team2State.bullets;

        globalStats.totalMoves = this.props.match.moves.length;

        return {
            team1Statistics: team1Statistics,
            team2Statistics: team2Statistics,
            globalStatistics: globalStats
        };
    }

    analyzeMove(thisTeamMove: TeamMoveDTO, otherTeamMove: TeamMoveDTO, stats: TeamStatistics): TeamStatistics {
        if (thisTeamMove.faulty) {
            stats.faultyActions++;
            return stats;
        }
        switch (thisTeamMove.teamMove) {
            case TeamMoveType.SHOOT:
                stats.shotsTaken++;
                break;
            case TeamMoveType.BLOCK:
                if (otherTeamMove.teamMove === TeamMoveType.SHOOT) {
                    stats.successfulBlocks++;
                } else {
                    stats.emptyBlocks++;
                }
                break;
            case TeamMoveType.EVADE:
                if (otherTeamMove.teamMove === TeamMoveType.SHOOT) {
                    stats.successfulEvades++;
                } else {
                    stats.emptyEvades++;
                }
                break;
            case TeamMoveType.RELOAD:
                stats.bulletsLoaded++;
                break;
            case TeamMoveType.NULL:
                stats.faultyActions++;
                break;
        }

        return stats;
    }

    getTeamStatisticsColumn(teamStatistics: TeamStatistics, title: string) {
        return (
            <Grid.Column >
                <Header style={HeaderStyle} inverted={true} size={"medium"}>{title}</Header>
                <p>
                    <b>Bullets Loaded: </b><i>{teamStatistics.bulletsLoaded}</i>
                </p>
                <p>
                    <b>Shots Taken: </b><i>{teamStatistics.shotsTaken}</i>
                </p>
                <p>
                    <b>Bullets Left: </b><i>{teamStatistics.bulletsLeft}</i>
                </p>
                <p>
                    <b>Empty Blocks: </b><i>{teamStatistics.emptyBlocks}</i>
                </p>
                <p>
                    <b>Successful Blocks: </b><i>{teamStatistics.successfulBlocks}</i>
                </p>
                <p>
                    <b>Empty Evades: </b><i>{teamStatistics.emptyEvades}</i>
                </p>
                <p>
                    <b>Successful Evades: </b><i>{teamStatistics.successfulEvades}</i>
                </p>
                <p>
                    <b>Faulty actions: </b><i>{teamStatistics.faultyActions}</i>
                </p>
            </Grid.Column>
        );
    }


    render() {
        const {team1Statistics, team2Statistics, globalStatistics} = this.state;

        return (
            <Grid columns={4} textAlign={"center"}>
                <Grid.Column >
                    <Header style={HeaderStyle} inverted={true} size={"medium"}>Winner</Header>
                    { this.props.match.winner? <TeamCard team={this.props.match.winner} style={CardStyle} imageSize={200}/> : null}
                </Grid.Column>
                <Grid.Column >
                    <Header style={HeaderStyle} inverted={true} size={"medium"}>Global</Header>
                    <p>
                        <b>Moves Total: </b><i>{globalStatistics.totalMoves}</i>
                    </p>
                </Grid.Column>
                {this.getTeamStatisticsColumn(team1Statistics, this.props.match.team1.name)}
                {this.getTeamStatisticsColumn(team2Statistics, this.props.match.team2.name)}
            </Grid>
        );
    }
}

const CardStyle: React.CSSProperties = {
    width: "150px",
    margin: "auto"
};

const HeaderStyle: React.CSSProperties = {
    textAlign: "center"
};

export default DetailsMatchComponent
