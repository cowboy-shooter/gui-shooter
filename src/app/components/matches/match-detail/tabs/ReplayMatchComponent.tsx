import * as React from "react";
import ServiceComponent from "../../../service/ServiceComponent";
import {Button, Divider, Grid, Header, Icon} from "semantic-ui-react";
import {MatchDTO} from "../../../../base/dto/matches/MatchDTO";
import TeamCard from "../../../teams/card/TeamCard";
import {Properties, Settings} from "../../../../../Settings";
import {MatchMoveDTO} from "../../../../base/dto/matches/MatchMoveDTO";
import {TeamMoveType} from "../../../../base/dto/matches/TeamMoveDTO";
import {TeamMatchStateDTO} from "../../../../base/dto/matches/TeamMatchStateDTO";

interface ReplayMatchComponentProps {
    match: MatchDTO
}

interface ReplayMatchComponentState {
    currentMove?: MatchMoveDTO,
    currentMoveIndex: number,
    play: boolean
}

class ReplayMatchComponent extends ServiceComponent<ReplayMatchComponentProps, ReplayMatchComponentState> {

    private interval: any;

    constructor(props: ReplayMatchComponentProps) {
        super(props);
        this.state = {
            currentMoveIndex: -1,
            play: true
        };
    }

    componentDidMount(): void {
        this.play();
    }

    left = () => {
        this.pause();
        let currentIndex = this.state.currentMoveIndex;
        if (currentIndex <= 0) {
            currentIndex = this.props.match.moves.length - 1;
        } else {
            currentIndex--;
        }
        this.playMove(currentIndex);
    };

    right = () => {
        this.pause();
        this.forward();
    };

    forward = () => {
        let currentIndex = this.state.currentMoveIndex;
        if (currentIndex >= this.props.match.moves.length - 1) {
            currentIndex = 0;
        } else {
            currentIndex++;
        }
        this.playMove(currentIndex);
    };

    play = () => {
        this.setState({
            play: true
        });
        this.forward();
        this.interval = setInterval(() => this.forward(), Settings.getProperty(Properties.REPLAY_MATCH_SPEED) as number);
    };

    pause = () => {
        this.setState({
            play: false
        });
        clearInterval(this.interval);
    };

    playMove(index: number) {
        this.setState({
            currentMove: this.props.match.moves[index],
            currentMoveIndex: index
        });
    }

    getTeamStatsCell(state: TeamMatchStateDTO) {
        return (
            <div>
                <p>
                    <i><b>Bullets: </b>{state.bullets}</i>
                </p>
                <p>
                    <i><b>Blocks in Row: </b>{state.blockCounter}</i>
                </p>
                <p>
                    <i><b>Evades: </b>{state.evadeCounter}</i>
                </p>
                <p>
                    <i><b>Alive: </b>{!state.isDead + ""}</i>
                </p>
            </div>
        );
    }

    getMoveCell(move: TeamMoveType, message: string, faulty: boolean) {
        let shootStyle = {};
        let reloadStyle = {};
        let blockStyle = {};
        let evadeStyle = {};
        let noneStyle = {};

        switch (move) {
            case TeamMoveType.SHOOT:
                shootStyle = {
                    color: faulty ? "red" : "green"
                };
                break;
            case TeamMoveType.BLOCK:
                blockStyle = {
                    color: faulty ? "red" : "green"
                };
                break;
            case TeamMoveType.EVADE:
                evadeStyle = {
                    color: faulty ? "red" : "green"
                };
                break;
            case TeamMoveType.RELOAD:
                reloadStyle = {
                    color: faulty ? "red" : "green"
                };
                break;
            case TeamMoveType.NULL:
                noneStyle = {
                    color: "red"
                };
                break;
        }

        return (
            <div style={CellStyle}>
                <Divider horizontal inverted style={shootStyle}>
                    Shoot
                </Divider>
                <Divider horizontal inverted style={blockStyle}>
                    Block
                </Divider>
                <Divider horizontal inverted style={evadeStyle}>
                    Evade
                </Divider>
                <Divider horizontal inverted style={reloadStyle}>
                    Reload
                </Divider>
                <Divider horizontal inverted style={noneStyle}>
                    None
                </Divider>
                {message}
            </div>
        );
    }

    render() {
        return (
            <Grid columns={3} textAlign={"center"}>
                <Grid.Row>
                    <Grid.Column>
                        <TeamCard style={TeamCardStyle} team={this.props.match.team1} imageSize={200}/>
                    </Grid.Column>
                    <Grid.Column>
                    </Grid.Column>
                    <Grid.Column>
                        <TeamCard style={TeamCardStyle} team={this.props.match.team2} imageSize={200}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Grid columns={2}>
                            <Grid.Column>
                                {
                                    this.state.currentMove && this.getTeamStatsCell(this.state.currentMove.team1State)
                                }
                            </Grid.Column>
                            <Grid.Column>
                                {
                                    this.state.currentMove && this.getMoveCell(this.state.currentMove.team1Move.teamMove, this.state.currentMove.team1Move.message, this.state.currentMove.team1Move.faulty)
                                }
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>
                    <Grid.Column>
                        <Header size={"large"} inverted>
                            Round
                        </Header>
                        <Header size={"medium"} inverted>
                            {
                                this.state.currentMoveIndex + 1
                            }
                        </Header>
                    </Grid.Column>
                    <Grid.Column>
                        <Grid columns={2}>
                            <Grid.Column>
                                {
                                    this.state.currentMove && this.getMoveCell(this.state.currentMove.team2Move.teamMove, this.state.currentMove.team2Move.message, this.state.currentMove.team2Move.faulty)
                                }
                            </Grid.Column>
                            <Grid.Column>
                                {
                                    this.state.currentMove && this.getTeamStatsCell(this.state.currentMove.team2State)
                                }
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Button basic inverted color='green'onClick={this.left}>
                            <Icon name='long arrow alternate left' />
                        </Button>
                    </Grid.Column>
                    <Grid.Column>
                        {
                            this.state.play?
                                (<Button basic inverted color='green' onClick={this.pause}>
                                    <Icon name='pause' />
                                </Button>)
                                : (<Button basic inverted color='green'onClick={this.play}>
                                    <Icon name='play' />
                                </Button>)
                        }
                    </Grid.Column>
                    <Grid.Column>
                        <Button basic inverted color='green'onClick={this.right}>
                            <Icon name='long arrow alternate right' />
                        </Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

const TeamCardStyle: React.CSSProperties = {
    width: "50%",
    margin: "auto",
    textAlign: "center"
};

const CellStyle = {
    margin: "auto"

};

export default ReplayMatchComponent
