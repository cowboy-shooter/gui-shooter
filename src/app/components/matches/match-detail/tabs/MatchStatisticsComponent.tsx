import * as React from "react";
import {Component} from "react";
import {MatchDTO} from "../../../../base/dto/matches/MatchDTO";
import {Grid, Header} from "semantic-ui-react";
import PieChartComponent, {ChartData, PieChartComponentPrefabs} from "../../../charts/PieChartComponent";
import {TeamMoveDTO, TeamMoveType} from "../../../../base/dto/matches/TeamMoveDTO";
import LineChartComponent, {LineChartData} from "../../../charts/LineChartComponent";

interface MatchStatisticsComponentProps {
    match: MatchDTO
}


interface MatchStatisticsComponentState {
    team1Stats: TeamStats,
    team2Stats: TeamStats,
    bulletsData: LineChartData[]
}

interface TeamStats {
    actionsData: ChartData[],
    failsData: ChartData[]
}

class MatchStatisticsComponent extends Component<MatchStatisticsComponentProps, MatchStatisticsComponentState> {

    constructor(props: MatchStatisticsComponentProps) {
        super(props);
        this.state = this.assembleState();
    }

    assembleState(): MatchStatisticsComponentState {

        interface TeamActions {
            shooting: number,
            evading: number,
            blocking: number,
            reloading: number,
            failing: number
        }

        interface TeamFails {
            failShooting: number,
            failEvading: number,
            failBlocking: number,
            failReloading: number,
            errorResponses: number,
        }


        let team1Actions: TeamActions = {
            shooting: 0,
            evading: 0,
            blocking: 0,
            reloading: 0,
            failing: 0
        };

        let team2Actions: TeamActions = {
            shooting: 0,
            evading: 0,
            blocking: 0,
            reloading: 0,
            failing: 0
        };

        let team1Fails: TeamFails = {
            failShooting: 0,
            failEvading: 0,
            failBlocking: 0,
            failReloading: 0,
            errorResponses: 0,
        };

        let team2Fails: TeamFails = {
            failShooting: 0,
            failEvading: 0,
            failBlocking: 0,
            failReloading: 0,
            errorResponses: 0,
        };

        let teamsBullets: LineChartData[] = [];

        let countAction = (move: TeamMoveDTO, teamActions: TeamActions, teamFails: TeamFails) => {
            switch (move.teamMove) {
                case TeamMoveType.SHOOT:
                    if (move.faulty) {
                        teamActions.failing++;
                        teamFails.failShooting++;
                    } else {
                        teamActions.shooting++;
                    }
                    break;
                case TeamMoveType.BLOCK:
                    if (move.faulty) {
                        teamActions.failing++;
                        teamFails.failBlocking++;
                    } else {
                        teamActions.blocking++;
                    }
                    break;
                case TeamMoveType.EVADE:
                    if (move.faulty) {
                        teamActions.failing++;
                        teamFails.failEvading++;
                    } else {
                        teamActions.evading++;
                    }
                    break;
                case TeamMoveType.RELOAD:
                    if (move.faulty) {
                        teamActions.failing++;
                        teamFails.failReloading++;
                    } else {
                        teamActions.reloading++;
                    }
                    break;
                case TeamMoveType.NULL:
                    teamFails.errorResponses++;
                    teamActions.failing++;
                    break;
            }
        };

        const {match} = this.props;

        match.moves.forEach(
            (move, index) => {
                countAction(move.team1Move, team1Actions, team1Fails)
                teamsBullets.push({
                    name: "Round " + (index + 1),
                    value1: move.team1State.bullets,
                    value2: move.team2State.bullets
                });
            }
        );

        match.moves.forEach(
            move => {
                countAction(move.team2Move, team2Actions, team2Fails)
            }
        );

        let team1ActionsData: ChartData[] = [
            {name: "Shooting", value: team1Actions.shooting, color: "#fec21b"},
            {name: "Evading", value: team1Actions.evading, color: "#fbfe0d"},
            {name: "Blocking", value: team1Actions.blocking, color: "#f525fe"},
            {name: "Reloading", value: team1Actions.reloading, color: "#0088fe"},
            {name: "Failing", value: team1Actions.failing, color: "#fe676b"}
        ];

        let team2ActionsData: ChartData[] = [
            {name: "Shooting", value: team2Actions.shooting, color: "#fec21b"},
            {name: "Evading", value: team2Actions.evading, color: "#fbfe0d"},
            {name: "Blocking", value: team2Actions.blocking, color: "#f525fe"},
            {name: "Reloading", value: team2Actions.reloading, color: "#0088fe"},
            {name: "Failing", value: team2Actions.failing, color: "#fe676b"}
        ];

        let team1FailData: ChartData[] = [
            {name: "Fail Shooting", value: team1Fails.failShooting, color: "#fe5b0f"},
            {name: "Fail Evading", value: team1Fails.failEvading, color: "#fe1f1e"},
            {name: "Fail Blocking", value: team1Fails.failBlocking, color: "#fe186c"},
            {name: "Fail Reloading", value: team1Fails.failReloading, color: "#fe1067"},
            {name: "Error Responses", value: team1Fails.errorResponses, color: "#fe10a4"}
        ];

        let team2FailData: ChartData[] = [
            {name: "Fail Shooting", value: team2Fails.failShooting, color: "#fe5b0f"},
            {name: "Fail Evading", value: team2Fails.failEvading, color: "#fe1f1e"},
            {name: "Fail Blocking", value: team2Fails.failBlocking, color: "#fe186c"},
            {name: "Fail Reloading", value: team2Fails.failReloading, color: "#fe1067"},
            {name: "Error Responses", value: team2Fails.errorResponses, color: "#fe10a4"}
        ];

        let team1Stats: TeamStats = {
            actionsData: this.fixStatsArray(team1ActionsData),
            failsData: this.fixStatsArray(team1FailData)
        };

        let team2Stats: TeamStats = {
            actionsData: this.fixStatsArray(team2ActionsData),
            failsData: this.fixStatsArray(team2FailData)
        };

        return {
            team1Stats: team1Stats,
            team2Stats: team2Stats,
            bulletsData: teamsBullets
        };
    }

    fixStatsArray = (data: ChartData[]): ChartData[] => {
        let result: ChartData[] = [];

        data.forEach((chartData) => {
            if(chartData.value > 0) {
                result.push(chartData);
            }
        });

        return result;
    };

    render() {
        const {match} = this.props;

        return (
            <Grid columns={1} textAlign={"center"}>
                <Grid.Row>
                    <Grid.Column>
                        <LineChartComponent data={this.state.bulletsData} line1Name={"1. " + match.team1.name} line2Name={"2. " + match.team2.name}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Grid columns={2}>
                            <Grid.Row>
                                <Grid.Column>
                                    <Header inverted={true} size={"medium"}>{match.team1.name}</Header>
                                </Grid.Column>
                                <Grid.Column>
                                    <Header inverted={true} size={"medium"}>{match.team2.name}</Header>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column>
                                    <PieChartComponent data={this.state.team1Stats.actionsData} prefab={PieChartComponentPrefabs.SMALL}/>
                                    <PieChartComponent data={this.state.team1Stats.failsData} prefab={PieChartComponentPrefabs.SMALL}/>
                                </Grid.Column>
                                <Grid.Column>
                                    <PieChartComponent data={this.state.team2Stats.actionsData} prefab={PieChartComponentPrefabs.SMALL}/>
                                    <PieChartComponent data={this.state.team2Stats.failsData} prefab={PieChartComponentPrefabs.SMALL}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default MatchStatisticsComponent
