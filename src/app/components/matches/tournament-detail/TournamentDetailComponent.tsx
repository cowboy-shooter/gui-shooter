import * as React from "react";
import ServiceComponent from "../../service/ServiceComponent";
import {TournamentDTO, TournamentDTOMapper} from "../../../base/dto/tournaments/TournamentDTO";
import {TournamentTierDTOMapper} from "../../../base/dto/tournaments/TournamentTierDTO";
import {Card, Grid, Header, Menu, Tab, TabProps} from "semantic-ui-react";
import {faQuestion} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import TeamCard from "../../teams/card/TeamCard";
import CenteredLoader from "../../loader/CenteredLoader";
import {Properties, Settings} from "../../../../Settings";
import TableTierComponent from "./tabs/TableTierComponent";
import TournamentResultsComponent from "./tabs/TournamentResultsComponent";
import TournamentStatisticsComponent from "./tabs/TournamentStatisticsComponent";


export interface TournamentDetailComponentProps {
    tournamentId: string
}

interface TournamentDetailComponentState {
    tournament?: TournamentDTO,
    tabIndex: number
}

export class TournamentDetailComponent extends ServiceComponent<TournamentDetailComponentProps, TournamentDetailComponentState> {

    private interval: any;

    constructor(props: TournamentDetailComponentProps) {
        super(props);
        this.state = {
            tabIndex: 0
        };

    }

    componentDidMount(): void {
        this.reload();
        this.interval = setInterval(() => this.reload(), Settings.getProperty(Properties.TOURNAMENT_DETAIL_REFRESH_RATE) as number);
    }

    componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    reload = () => {
        this.reloadMatch();

        const {tournament} = this.state;
        if (tournament && tournament.fourthTier && tournament.fourthTier.executed) {
            clearInterval(this.interval);
        }
    };

    reloadMatch = () => {
        let tierMapper: TournamentTierDTOMapper = {
            id: true,
            executed: true,
            teams: {
                id: true,
                name: true
            },
            tournamentMatches: {
                executed: true,
                id: true,
                team1: {
                    id: true,
                    name: true
                }, team2: {
                    id: true,
                    name: true
                },
                team1Score: true,
                team2Score: true,
                plannedMatches: {
                    id: true,
                    executedMatch: {
                        id: true,
                        finished: true,
                        team1: {
                            id: true
                        },
                        team2: {
                            id: true
                        },
                        winner: {
                            id: true
                        }
                    }
                }
            }
        };

        let mapper: TournamentDTOMapper = {
            id: true,
            started: true,
            name: true,
            winner: {
                id: true,
                name: true,
                flag: true,
                description: true,
                language: true
            }, secondPlace: {
                id: true,
                name: true,
                flag: true,
                description: true,
                language: true
            },
            firstTier: tierMapper,
            secondTier: tierMapper,
            thirdTier: tierMapper,
            fourthTier: tierMapper
        };

        this.tournamentsService.getTournament(this.props.tournamentId, mapper)
            .then((result) => {
               this.setState({
                   tournament: result.data
               });
            });
    };

    onTabChange = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, data: TabProps) => {
        this.setState({
            tabIndex: data['activeIndex'] as number
        });
    };

    getTab(name: String, key: string, tabs: {}[], content: JSX.Element) {
        return {
            menuItem: (
                <Menu.Item key={key} style={this.state.tabIndex === tabs.length ? activeTabStyle: defaultTabStyle}>
                    {name}
                </Menu.Item>
            ),
            render: () => (
                <Tab.Pane style={PanelContentStyle}>
                    {content}
                </Tab.Pane>
            ),
        }
    }

    render() {
        const {tournament} = this.state;

        let tabs: {}[] = [];
        tabs.push(this.getTab("1. Tier", "tier1", tabs, (<TableTierComponent tournament={this.state.tournament} tier={this.state.tournament? this.state.tournament.firstTier: undefined} reloadFunction={this.reload}/>)));
        tabs.push(this.getTab("2. Tier", "tier2", tabs, (<TableTierComponent tournament={this.state.tournament} tier={this.state.tournament? this.state.tournament.secondTier: undefined} reloadFunction={this.reload}/>)));
        tabs.push(this.getTab("3. Tier", "tier3", tabs, (<TableTierComponent tournament={this.state.tournament} tier={this.state.tournament? this.state.tournament.thirdTier: undefined} reloadFunction={this.reload}/>)));
        tabs.push(this.getTab("4. Tier", "tier4", tabs, (<TableTierComponent tournament={this.state.tournament} tier={this.state.tournament? this.state.tournament.fourthTier: undefined} reloadFunction={this.reload}/>)));
        tabs.push(this.getTab("Results", "results", tabs, (<TournamentResultsComponent tournament={this.state.tournament} />)));
        if (tournament) {
            tabs.push(this.getTab("Statistics", "statistics", tabs, (<TournamentStatisticsComponent tournament={tournament} />)));
        }

        if (!tournament) {
            return  (
                <CenteredLoader/>
            )
        }

        let winnerCard = null;

        if (tournament.winner) {
            winnerCard = (
                <TeamCard style={WinnerCardStyle} team={tournament.winner} imageSize={300}/>
            );
        } else {
            winnerCard = (
                <Card style={NoWinnerCardStyle}>
                    <Card.Content>
                        <Card.Header style={NoWinnerHeaderStyle}>
                            <FontAwesomeIcon icon={faQuestion} style={IconStyle}/>
                            <h2>Winner</h2>
                        </Card.Header>
                    </Card.Content>
                </Card>
            );
        }

        return (
            <Grid>
                <Grid.Row>
                    {winnerCard}
                </Grid.Row>
                <Grid.Row centered>
                    <Header inverted size={"huge"}>{tournament.name}</Header>
                </Grid.Row>
                <Grid.Row>
                    <Tab panes={tabs} onTabChange={this.onTabChange} style={TabStyle}/>
                </Grid.Row>
            </Grid>
        );
    }
}

const PanelContentStyle: React.CSSProperties = {
    backgroundColor: "transparent",
    border: "none"
};

const defaultTabStyle: React.CSSProperties = {
    color: "white",
    backgroundColor: "transparent",
};

const activeTabStyle: React.CSSProperties = {
    color: "white",
    backgroundColor: "rgba(193, 155, 90, 0.33)",
};

const TabStyle: React.CSSProperties = {
    textAlign: "center",
    margin: "auto",
    width: "100%"
};

const IconStyle: React.CSSProperties = {
    height: "200px",
    width: "200px",
    color: "white"
};

const NoWinnerCardStyle: React.CSSProperties = {
    margin: "auto",
    backgroundColor: "#ffffff1a"
};

const WinnerCardStyle: React.CSSProperties = {
    margin: "auto"
};

const NoWinnerHeaderStyle: React.CSSProperties = {
    color: "white"
};


export default TournamentDetailComponent
