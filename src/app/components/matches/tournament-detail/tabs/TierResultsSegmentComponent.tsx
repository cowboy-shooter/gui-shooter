import * as React from "react";
import {Component} from "react";
import {Header, Segment, Table} from "semantic-ui-react";
import {TournamentTierDTO} from "../../../../base/dto/tournaments/TournamentTierDTO";
import {TeamDTO} from "../../../../base/dto/teams/TeamDTO";


export interface TierResultsSegmentComponentProps {
    tier?: TournamentTierDTO,
    tier2?: TournamentTierDTO,
    title: string,
}

interface TeamScore {
    team: TeamDTO,
    score: number,
    wins: number,
    loses: number,
    draws: number
}

interface TierResultsSegmentComponentState {
    score: TeamScore[]
}

export class TierResultsSegmentComponent extends Component<TierResultsSegmentComponentProps, TierResultsSegmentComponentState> {


    constructor(props: TierResultsSegmentComponentProps) {
        super(props);
        this.state = {
            score: this.countScore()
        };
    }

    countScore = (): TeamScore[] => {
        const {tier, tier2} = this.props;
        if (!tier || !tier.tournamentMatches) {
            return [];
        }
        let teamScoresObject = {};
        tier.teams.forEach((team) => {
            // @ts-ignore
            teamScoresObject[team.id] = {
                team: team,
                score: 0,
                wins: 0,
                loses: 0,
                draws: 0
            }
        });

        this.fillTeamScoreObject(teamScoresObject, tier);

        if (tier2) {
            this.fillTeamScoreObject(teamScoresObject, tier2);
        }

        let scores: TeamScore[] = Object.keys(teamScoresObject).map((index) => {
            // @ts-ignore
            return teamScoresObject[index];
        });
        scores.sort((a, b) => (a.score < b.score) ? 1 : -1)

        return scores;
    };

    fillTeamScoreObject(teamScoresObject: {}, tier: TournamentTierDTO) {
        tier.tournamentMatches.forEach((tournamentMatch) => {
            if (tournamentMatch.executed) {
                // @ts-ignore
                teamScoresObject[tournamentMatch.team1.id].score += tournamentMatch.team1Score;
                // @ts-ignore
                teamScoresObject[tournamentMatch.team2.id].score += tournamentMatch.team2Score;
                tournamentMatch.plannedMatches.forEach((plannedMatch) => {
                    if (plannedMatch.executedMatch) {
                        let match = plannedMatch.executedMatch;

                        if (match.winner && match.winner.id === match.team1.id) {
                            // @ts-ignore
                            teamScoresObject[match.team1.id].wins++;
                            // @ts-ignore
                            teamScoresObject[match.team2.id].loses++;
                        } else if (match.winner && match.winner.id === match.team2.id) {
                            // @ts-ignore
                            teamScoresObject[match.team2.id].wins++;
                            // @ts-ignore
                            teamScoresObject[match.team1.id].loses++;
                        } else {
                            // @ts-ignore
                            teamScoresObject[match.team1.id].draws++;
                            // @ts-ignore
                            teamScoresObject[match.team2.id].draws++;
                        }
                    }
                })
            }
        });
    }


    render() {
        const {title} = this.props;
        const {score} = this.state;

        return (
            <Segment inverted textAlign={"center"}>
                <Header size={"large"}>{title}</Header>
                <Table celled inverted selectable basic='very'>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell><h3>Team</h3></Table.HeaderCell>
                            <Table.HeaderCell><h3>Wins</h3></Table.HeaderCell>
                            <Table.HeaderCell><h3>Loses</h3></Table.HeaderCell>
                            <Table.HeaderCell><h3>Draws</h3></Table.HeaderCell>
                            <Table.HeaderCell><h3>Score</h3></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {
                            score.map((teamScore) => {
                                return (
                                    <Table.Row>
                                        <Table.Cell>{teamScore.team.name}</Table.Cell>
                                        <Table.Cell>{teamScore.wins}</Table.Cell>
                                        <Table.Cell>{teamScore.loses}</Table.Cell>
                                        <Table.Cell>{teamScore.draws}</Table.Cell>
                                        <Table.Cell>{teamScore.score}</Table.Cell>
                                    </Table.Row>
                                )
                            })
                        }
                    </Table.Body>
                </Table>
            </Segment>
        );
    }
}




export default TierResultsSegmentComponent
