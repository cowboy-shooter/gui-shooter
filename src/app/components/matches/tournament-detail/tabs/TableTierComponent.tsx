import * as React from "react";
import {Button, Grid, Modal} from "semantic-ui-react";
import {TournamentTierDTO} from "../../../../base/dto/tournaments/TournamentTierDTO";
import {TournamentDTO} from "../../../../base/dto/tournaments/TournamentDTO";
import ServiceComponent from "../../../service/ServiceComponent";
import MatchDetailComponent from "../../match-detail/MatchDetailComponent";
import {UserUtils} from "../../../../utils/UserUtils";
import TierTeamAssignerComponent from "../../create-tournament/TierTeamAssignerComponent";
import {TeamDTO} from "../../../../base/dto/teams/TeamDTO";


export interface TableTierComponentProps {
    tournament?: TournamentDTO,
    tier?: TournamentTierDTO,
    reloadFunction: () => void,
}

interface TableTierComponentState {
    hoverMatchId?: string
}

export class TableTierComponent extends ServiceComponent<TableTierComponentProps, TableTierComponentState> {


    constructor(props: TableTierComponentProps) {
        super(props);
        this.state = {
        };
    }

    onHoverEnterMatch = (matchId: string) => {
        this.setState({
            hoverMatchId: matchId
        });
    };

    onHoverExitMatch = () => {
        this.setState({
            hoverMatchId: undefined
        });
    };

    executeTier = () => {
        if (!this.props.tier) {
            return;
        }
        this.tournamentsService.executeTier(this.props.tier.id)
            .then(this.props.reloadFunction);
    };

    fixTier = (teams: TeamDTO[]) => {
        if (!this.props.tier) {
            return;
        }
        this.tournamentsService.fixTier(this.props.tier.id, teams)
            .then(this.props.reloadFunction);
    };

    render() {
        const {tier} = this.props;

        return (
            <Grid>
                {
                    UserUtils.isAdmin()? (
                        <Grid.Row centered={true}>
                            <Button basic inverted color='green' onClick={this.executeTier}>
                                Execute Tier
                            </Button>
                            <TierTeamAssignerComponent onAssignFunction={this.fixTier}/>
                        </Grid.Row>
                    ) : null
                }
                <Grid.Row>
                    <Grid columns={11}>
                        {
                            tier? tier.tournamentMatches.map((tournamentMatch) => {
                                let columnCommonStyle:React.CSSProperties = {
                                    border: "solid",
                                    borderColor: "#324eb940",
                                    minWidth: "150px",
                                    transition: "all 0.5s ease"
                                };
                                let columnDefaultStyle: React.CSSProperties = {
                                    ...columnCommonStyle,
                                    background: "#4c5ede26",
                                    cursor: "pointer"
                                };
                                let columnActiveStyle: React.CSSProperties = {
                                    ...columnCommonStyle,
                                    background: "rgba(38, 57, 191, 0.4)",
                                    cursor: "pointer"
                                };
                                let columnDisabledStyle: React.CSSProperties = {
                                    ...columnCommonStyle,
                                    background: "transparent",
                                };
                                let topTeamDefaultStyle: React.CSSProperties = {
                                    height: "82px",
                                    borderBottom: "solid",
                                    borderColor: "white",
                                    paddingTop: "25px"
                                };
                                let bottomTeamDefaultStyle:React.CSSProperties = {
                                    height: "83px",
                                    paddingTop: "25px"
                                };

                                let resultsColumnStyle: React.CSSProperties = {
                                    border: "solid rgba(89, 195, 118, 0.58)",
                                    background: "rgb(14, 17, 36)",
                                    borderTopLeftRadius: "20px",
                                    borderBottomRightRadius: "20px",
                                    marginLeft: "50px",
                                    width: "100px"
                                };
                                let topTeamResultsStyle: React.CSSProperties = {
                                    height: "82px",
                                    borderBottom: "solid",
                                    paddingTop: "25px"
                                };
                                let bottomTeamResultsStyle: React.CSSProperties = {
                                    height: "83px",
                                    paddingTop: "25px"
                                };
                                return (
                                    <Grid.Row style={{height: "200px"}}>
                                        {
                                            tournamentMatch.plannedMatches.map((plannedMatch, index) => {
                                                let columnStyle: React.CSSProperties = {};
                                                if (plannedMatch.id === this.state.hoverMatchId) {
                                                    columnStyle = {...columnActiveStyle}
                                                } else if (plannedMatch.executedMatch === null) {
                                                    columnStyle = {...columnDisabledStyle}
                                                } else {
                                                    columnStyle = {...columnDefaultStyle}
                                                }
                                                let topTeamStyle: React.CSSProperties = {...topTeamDefaultStyle};
                                                let bottomTeamStyle: React.CSSProperties = {...bottomTeamDefaultStyle};

                                                if (index === 0) {
                                                    columnStyle = {
                                                        ...columnStyle,
                                                        borderTopLeftRadius: "20px",
                                                        borderBottomLeftRadius: "20px",
                                                    };
                                                } else if (index === tournamentMatch.plannedMatches.length - 1) {
                                                    columnStyle = {
                                                        ...columnStyle,
                                                        borderTopRightRadius: "20px",
                                                        borderBottomRightRadius: "20px",
                                                    };
                                                }

                                                if (plannedMatch.executedMatch && plannedMatch.executedMatch.winner) {
                                                    if (plannedMatch.executedMatch.winner.id === tournamentMatch.team1.id) {
                                                        topTeamStyle = {
                                                            ...topTeamStyle,
                                                            background: "#33d63352",
                                                            borderTopLeftRadius: "20px",
                                                            borderTopRightRadius: "20px",
                                                        };
                                                    }

                                                    if (plannedMatch.executedMatch.winner.id === tournamentMatch.team2.id) {
                                                        bottomTeamStyle = {
                                                            ...bottomTeamStyle,
                                                            background: "#33d63352",
                                                            borderBottomLeftRadius: "20px",
                                                            borderBottomRightRadius: "20px",
                                                        };
                                                    }
                                                }

                                                let columnElements = (
                                                    <div>
                                                        <div style={topTeamStyle}>
                                                            <h4>{tournamentMatch.team1.name}</h4>
                                                        </div>
                                                        <div style={bottomTeamStyle}>
                                                            <h4 >{tournamentMatch.team2.name}</h4>
                                                        </div>
                                                    </div>
                                                );

                                                if (plannedMatch.executedMatch) {
                                                    return (
                                                        <Modal trigger={
                                                            <Grid.Column style={columnStyle} onMouseEnter={() => this.onHoverEnterMatch(plannedMatch.id)} onMouseLeave={() => this.onHoverExitMatch()}>
                                                                {columnElements}
                                                            </Grid.Column>
                                                        } basic>
                                                            <MatchDetailComponent matchId={plannedMatch.executedMatch.id}/>
                                                        </Modal>
                                                    );
                                                } else {
                                                    return (
                                                        <Grid.Column style={columnStyle}>
                                                            {columnElements}
                                                        </Grid.Column>
                                                    );
                                                }
                                            })
                                        }
                                        <Grid.Column style={resultsColumnStyle}>
                                            <div style={topTeamResultsStyle}>
                                                <h2>{tournamentMatch.team1Score}</h2>
                                            </div>
                                            <div style={bottomTeamResultsStyle}>
                                                <h2 >{tournamentMatch.team2Score}</h2>
                                            </div>
                                        </Grid.Column>
                                    </Grid.Row>
                                )
                            }): null
                        }
                    </Grid>
                </Grid.Row>
            </Grid>

        );
    }
}




export default TableTierComponent
