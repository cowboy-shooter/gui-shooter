import * as React from "react";
import {Component} from "react";
import {Grid, Header} from "semantic-ui-react";
import {TournamentDTO} from "../../../../base/dto/tournaments/TournamentDTO";
import TierResultsSegmentComponent from "./TierResultsSegmentComponent";
import TeamCard from "../../../teams/card/TeamCard";


export interface TournamentResultsComponentProps {
    tournament?: TournamentDTO
}

interface TournamentResultsComponentState {
}

export class TournamentResultsComponent extends Component<TournamentResultsComponentProps, TournamentResultsComponentState> {


    constructor(props: TournamentResultsComponentProps) {
        super(props);
        this.state = {
        };
    }


    render() {
        const {tournament} = this.props;

        if (!tournament) {
            return;
        }

        return (
            <Grid centered>
                {
                    tournament.winner && tournament.secondPlace? (
                        <Grid.Row>
                            <Grid.Column textAlign={"center"} style={ColumnStyle}>
                                <Header inverted>Winner</Header>
                                <TeamCard style={{margin: "auto"}} team={tournament.winner} imageSize={200}/>
                            </Grid.Column>
                            <Grid.Column textAlign={"center"} style={ColumnStyle}>
                                <Header inverted>Second Place</Header>
                                <TeamCard style={{margin: "auto"}} team={tournament.secondPlace} imageSize={200}/>
                            </Grid.Column>
                        </Grid.Row>
                    ): null
                }
                <Grid.Row>
                    <Grid columns={6}>
                        {
                            tournament.firstTier? (
                                <Grid.Column style={ColumnStyle}>
                                    <TierResultsSegmentComponent title={"Tier 1"} tier={tournament.firstTier}></TierResultsSegmentComponent>
                                </Grid.Column>
                            ): null
                        }
                        {
                            tournament.secondTier? (
                                <Grid.Column style={ColumnStyle}>
                                    <TierResultsSegmentComponent title={"Tier 2"} tier={tournament.secondTier}></TierResultsSegmentComponent>
                                </Grid.Column>
                            ): null
                        }
                        {
                            tournament.firstTier && tournament.secondTier? (
                                <Grid.Column style={ColumnStyle}>
                                    <TierResultsSegmentComponent title={"Tier (1 + 2)"} tier={tournament.firstTier} tier2={tournament.secondTier}></TierResultsSegmentComponent>
                                </Grid.Column>
                            ): null
                        }
                        {
                            tournament.thirdTier? (
                                <Grid.Column style={ColumnStyle}>
                                    <TierResultsSegmentComponent title={"Tier 3"} tier={tournament.thirdTier}></TierResultsSegmentComponent>
                                </Grid.Column>
                            ): null
                        }
                        {
                            tournament.fourthTier? (
                                <Grid.Column style={ColumnStyle}>
                                    <TierResultsSegmentComponent title={"Tier 4"} tier={tournament.fourthTier}></TierResultsSegmentComponent>
                                </Grid.Column>
                            ): null
                        }
                    </Grid>
                </Grid.Row>
            </Grid>


        );
    }
}

const ColumnStyle = {
    minWidth: "500px"
};


export default TournamentResultsComponent
