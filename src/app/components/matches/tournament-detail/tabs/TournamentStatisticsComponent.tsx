import * as React from "react";
import {Grid} from "semantic-ui-react";
import {TournamentDTO} from "../../../../base/dto/tournaments/TournamentDTO";
import ServiceComponent from "../../../service/ServiceComponent";
import {
    TournamentStatisticsDTO,
    TournamentStatisticsDTOMapper
} from "../../../../base/dto/tournaments/TournamentStatisticsDTO";
import PieChartComponent, {PieChartComponentPrefabs} from "../../../charts/PieChartComponent";
import {StringUtils} from "../../../../utils/StringUtils";


export interface TournamentStatisticsComponentProps {
    tournament: TournamentDTO
}

interface TournamentStatisticsComponentState {
    tournamentStats?: TournamentStatisticsDTO
}

export class TournamentStatisticsComponent extends ServiceComponent<TournamentStatisticsComponentProps, TournamentStatisticsComponentState> {


    constructor(props: TournamentStatisticsComponentProps) {
        super(props);
        this.state = {
        };
    }

    componentDidMount(): void {
        const {tournament} = this.props;
        let mapper: TournamentStatisticsDTOMapper = {
            failResponses: true,
            blockedShots: true,
            blocks: true,
            bulletsShot: true,
            evadedShots: true,
            evades: true,
            draws: true,
            loses: true,
            wins: true,
            reloads: true,
            score: true,
            unsuccessfulBlocks: true,
            unsuccessfulEvades: true,
            unsuccessfulReloads: true,
            unsuccessfulShots: true,
        };

        this.tournamentsStatisticsService.getTournamentStatistics(tournament.id, mapper)
            .then((result) => {
                this.setState({
                   tournamentStats: result.data
                });
            });
    }


    render() {
        const {tournamentStats} = this.state;

        if (tournamentStats) {
            return (
                <Grid centered columns={2}>
                    {
                        Object.keys(tournamentStats).map((index) => {
                            // @ts-ignore
                            if (Object.keys(tournamentStats[index]).length === 0) {
                                return null;
                            }
                            // @ts-ignore
                            return <PieChartComponent dynamicData={tournamentStats[index]} prefab={PieChartComponentPrefabs.BIG} title={StringUtils.camelCaseToTitle(index)}/>
                        })
                    }
                </Grid>
            );
        } else {
            return null;
        }
    }
}


export default TournamentStatisticsComponent
