import React from 'react'
import {RoutingPaths} from "../../base/RoutingPaths";
import UserBarComponent from "../user/user-bar/UserBarComponent";
import {Divider, Header, Menu, Segment, Tab, Table, TabProps} from "semantic-ui-react";
import {TeamDTO, TeamDTOMapper} from "../../base/dto/teams/TeamDTO";
import {faFire} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// @ts-ignore
import Image from 'react-image-resizer';
import {PageInfo} from "../../base/dto/paging/PageInfo";
import {MatchDTO} from "../../base/dto/matches/MatchDTO";
import {Properties, Settings} from "../../../Settings";
import ServiceComponent from "../service/ServiceComponent";
import NavigationComponent from "../navigation/NavigationComponent";
import MatchesComponentTab from "./MatchesComponentTab";
import TournamentsComponentTab from "./TournamentsComponentTab";

interface MatchesComponentState {
    ownTeam?: TeamDTO,
    challengers: TeamDTO[],
    pageInfo?: PageInfo,
    matches: MatchDTO[],
    tabIndex: number
}

export class MatchesComponent extends ServiceComponent<{}, MatchesComponentState> {

    private interval: any;

    constructor(props: {}) {
        super(props);
        this.state = {
            ownTeam: undefined,
            challengers: [],
            matches: [],
            tabIndex: 0
        };
    }

    componentDidMount() {
        this.reload();
        this.interval = setInterval(() => this.reload(), Settings.getProperty(Properties.CHALLENGES_REFRESH_RATE) as number);
    }

    componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    reload = () => {
        this.reloadChallengers();
    };

    reloadChallengers() {
        let mapper: TeamDTOMapper = {
            id: true,
            name: true,
            flag: true
        };

        this.teamsService.getChallengers(mapper).then(result => {
            this.setState({
                challengers: result.data
            })
        });
    };

    acceptChallenge(team: TeamDTO) {
        this.matchService.acceptChallenge(team, {id: true});
        let challengers = this.state.challengers.filter((challenger) => {
            return challenger !== team;
        });
        this.setState({
            challengers: challengers
        });
    }

    onTabChange = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, data: TabProps) => {
        this.setState({
            tabIndex: data['activeIndex'] as number
        });
    };

    render() {
        let tabs = [];
        tabs.push(
            {
                menuItem: (
                    <Menu.Item key='matches' style={this.state.tabIndex === tabs.length ? activeTabStyle: defaultTabStyle}>
                        Matches
                    </Menu.Item>
                ),
                render: () => (
                    <Tab.Pane style={PanelContentStyle}>
                        <MatchesComponentTab/>
                    </Tab.Pane>
                ),
            }
        );

        tabs.push(
            {
                menuItem: (
                    <Menu.Item key='tournaments' style={this.state.tabIndex === tabs.length ? activeTabStyle : defaultTabStyle}>
                        Tournaments
                    </Menu.Item>
                ),
                render: () => (
                    <Tab.Pane style={PanelContentStyle}>
                        <TournamentsComponentTab/>
                    </Tab.Pane>
                )
            }
        );

        return (
            <div>
                <UserBarComponent/>
                <NavigationComponent path={RoutingPaths.MATCHES}/>
                <FontAwesomeIcon icon={faFire} style={HeaderIconStyle}/>
                <Tab panes={tabs} onTabChange={this.onTabChange} style={TabStyle}/>
                <Segment inverted style={ChallengesStyle}>
                    <Header size={"medium"} textAlign={"center"}>
                        Challenges
                    </Header>
                    <Divider style={ChallengesDivider} />
                    <Table celled inverted basic='very'>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell><h4>Challenger</h4></Table.HeaderCell>
                                <Table.HeaderCell><h4>Flag</h4></Table.HeaderCell>
                                <Table.HeaderCell><h4>Options</h4></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {
                                this.state.challengers && this.state.challengers.map((team) => (
                                    <Table.Row>
                                        <Table.Cell>{team.name}</Table.Cell>
                                        <Table.Cell>
                                            <Image
                                                src={team.flag}
                                                height={ 50 }
                                                width={ 100 }
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <button onClick={() => this.acceptChallenge(team)} className="ui inverted green basic button">Accept</button>
                                        </Table.Cell>
                                    </Table.Row>

                                ))
                            }
                        </Table.Body>
                    </Table>
                </Segment>
            </div>
        )
    }
}

const TabStyle = {
    textAlign: "center",
    margin: "auto",
    width: "60%"
};

const PanelContentStyle = {
    backgroundColor: "transparent",
    border: "none"
};

const defaultTabStyle = {
    color: "white",
    backgroundColor: "transparent",
};

const activeTabStyle = {
    color: "white",
    backgroundColor: "rgba(193, 155, 90, 0.33)",
};

const ChallengesDivider = {
    borderBottom: "1px solid",
    borderColor: "rgb(204, 200, 115)",
};

const ChallengesStyle = {
    borderTop: "double",
    borderBottom: "double",
    borderColor: "rgb(204, 200, 115)",
    borderTopLeftRadius: "20px",
    borderBottomLeftRadius: "20px",
    backgroundColor: "#9c96961c",
    position: "fixed",
    right: "0",
    top: "15vh",
    height: "70vh",
    minWidth: "30vh"
};

const HeaderIconStyle = {
    color: "#e49b6b59",
    margin: "auto",
    width: "100%",
    height: "10vh",
    minHeight: "50px"
};

export default MatchesComponent