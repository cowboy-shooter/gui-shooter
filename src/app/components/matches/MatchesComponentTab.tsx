import React from 'react'
import {Grid, Header, Modal, Pagination, PaginationProps, Table} from "semantic-ui-react";
import {TeamDTO, TeamDTOMapper} from "../../base/dto/teams/TeamDTO";
import CreateMatchComponent from "./create-match/CreateMatchComponent";
// @ts-ignore
import {PageInfo} from "../../base/dto/paging/PageInfo";
import {Page} from "../../base/dto/paging/Page";
import {MatchDTO, MatchDTOMapper} from "../../base/dto/matches/MatchDTO";
import {MatchDetailComponent} from "./match-detail/MatchDetailComponent";
import {Properties, Settings} from "../../../Settings";
import ServiceComponent from "../service/ServiceComponent";
import ReloadingImageComponent from "../table/ReloadingImageComponent";
import {Sorting, SortingDirection} from "../../base/dto/paging/Sorting";
import SortableHeaderCell from "../table/SortableHeaderCell";
import {DateUtils} from "../../utils/DateUtils";

interface MatchesComponentTabState {
    ownTeam?: TeamDTO,
    pageInfo?: PageInfo,
    matches: MatchDTO[],
}

export class MatchesComponentTab extends ServiceComponent<{}, MatchesComponentTabState> {

    private page: Page;
    private interval: any;

    constructor(props: {}) {
        super(props);
        this.state = {
            ownTeam: undefined,
            matches: [],
        };
        this.page = {
            page: 0,
            size: Settings.getProperty(Properties.MATCHES_PAGE_SIZE) as number,
            sorting: {
                sortBy: 'started',
                sortDirection: SortingDirection.DESC
            }
        };
    }

    componentDidMount() {
        this.reload();
        this.interval = setInterval(() => this.reload(), Settings.getProperty(Properties.MATCHES_REFRESH_RATE) as number);
    }

    componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    reload = () => {
        this.reloadOwnTeam();
        this.reloadMatches().then(this.reloadFlags);
    };


    reloadOwnTeam() {
        let mapper: TeamDTOMapper = {
            id: true,

        };

        this.teamsService.getOwnTeam(mapper).then(result => {
            this.setState({
                ownTeam: result.data
            })
        });
    };

    reloadMatches() {
        let mapper: MatchDTOMapper = {
            id: true,
            team1: {
                name: true,
            }, team2: {
                name: true,
            }, winner: {
                name: true
            }, started: true
        };

        return this.matchService.getMatches(this.page, mapper).then(result => {
            let matchesObject = result.data.content.reduce((obj: any, item) => {
                obj[item.id] = item;
                return obj
            }, {});

            if (this.state.matches) {
                this.state.matches.forEach((match) => {
                    if (matchesObject[match.id] && matchesObject[match.id].winner && match.winner) {
                        matchesObject[match.id].winner.flag = match.winner.flag;
                    }
                })
            }

            let matchesArray = Object.keys(matchesObject).map(function(teamIdIndex){
                let match = matchesObject[teamIdIndex];
                return match;
            });

            this.setState({
                matches: matchesArray,
                pageInfo: result.data.pageInfo
            });
        });
    };

    reloadFlags = () => {
        let mapper: MatchDTOMapper = {
            id: true,
            winner: {
                flag: true
            }
        };

        this.matchService.getMatches(this.page, mapper).then(result => {

            let resultObject = result.data.content.reduce((obj: any, item) => {
                obj[item.id] = item;
                return obj
            }, {});

            let matches = this.state.matches.map((match) => {
                if (resultObject[match.id] && match.winner) {
                    match.winner.flag = resultObject[match.id].winner.flag;
                }
                return match;
            });

            this.setState({
                matches: matches,
                pageInfo: result.data.pageInfo
            });
        });
    };

    onChange = (event: React.MouseEvent, pageInfo: PaginationProps) => {
        this.page = {
            page: pageInfo.activePage as number - 1,
            size: this.page.size,
            sorting: this.page.sorting
        };
        this.reloadMatches().then(this.reloadFlags);
    };

    changeSorting = (sorting: Sorting) => {
        this.page = {
            page: this.page.page,
            size: this.page.size,
            sorting: sorting
        };
        this.reloadMatches().then(this.reloadFlags);
    };

    render() {
        let disabledChallengeTeamButton = true;
        if (this.state.ownTeam) {
            disabledChallengeTeamButton = false;
        }

        return (
                <Grid style={GridStyle} textAlign={"center"}>
                    <Grid.Row>
                        {
                            disabledChallengeTeamButton ? <button className="ui inverted orange basic button" disabled={true}>Challenge Team</button> : <CreateMatchComponent/>
                        }
                    </Grid.Row>
                    <Grid.Row>

                    </Grid.Row>
                    <Grid.Row>
                        <Grid columns={1}>
                            <Grid.Column style={ColumnStyle}>
                                <Header size={"huge"} style={TableHeaderStyle}>
                                    Matches
                                </Header>
                                <Table celled inverted selectable basic='very'>
                                    <Table.Header>
                                        <Table.Row>
                                            <SortableHeaderCell value={"Challenger"} sortBy={"team1.name"} sorting={this.page.sorting} changeSorting={this.changeSorting}/>
                                            <SortableHeaderCell value={"Challenged"} sortBy={"team2.name"} sorting={this.page.sorting} changeSorting={this.changeSorting}/>
                                            <Table.HeaderCell colSpan='2'><h2>Winner</h2></Table.HeaderCell>
                                            <SortableHeaderCell value={"Date"} sortBy={"started"} sorting={this.page.sorting} changeSorting={this.changeSorting}/>
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {
                                            this.state.matches && this.state.matches.map((match) => (
                                                <Modal basic trigger={<Table.Row>
                                                    <Table.Cell>{match.team1.name}</Table.Cell>
                                                    <Table.Cell>{match.team2.name}</Table.Cell>
                                                    <Table.Cell>{match.winner? match.winner.name: null}</Table.Cell>
                                                    <Table.Cell>
                                                        {match.winner? <ReloadingImageComponent image={match.winner.flag} loaderSize={'medium'} imageHeight={50} imageWidth={100}/>: null}
                                                    </Table.Cell>
                                                    <Table.Cell>{DateUtils.formatDate(match.started)}</Table.Cell>
                                                </Table.Row>} style={ModalStyle}>
                                                    <MatchDetailComponent matchId={match.id}/>
                                                </Modal>
                                            ))
                                        }
                                    </Table.Body>
                                </Table>
                                {
                                    this.state.pageInfo ? (
                                        <Pagination
                                            defaultActivePage={1}
                                            siblingRange={3}
                                            totalPages={this.state.pageInfo.totalPages}
                                            onPageChange={this.onChange}
                                            pointing
                                            secondary
                                            inverted
                                        />
                                    ) : null
                                }
                            </Grid.Column>
                        </Grid>
                    </Grid.Row>
                </Grid>
        )
    }
}

const GridStyle: React.CSSProperties = {
    textAlign: "center",
    margin: "auto",
    width: "60%",
    minWidth: "850px"
};

const ModalStyle: React.CSSProperties = {
    textAlign: 'center' as 'center',
    top: "50px"
};

const ColumnStyle: React.CSSProperties = {
    border: "double",
    borderColor: "#9c9696",
    borderRadius: "20px",
    height: "50vh",
    backgroundColor: "#9c96961c"
};

const TableHeaderStyle: React.CSSProperties = {
    color: "rgba(216, 117, 51, 0.9)"
};


export default MatchesComponentTab