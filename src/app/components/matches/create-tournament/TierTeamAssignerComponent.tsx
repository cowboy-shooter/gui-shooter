import * as React from "react";
import {Divider, Grid, Header, Modal, Table} from "semantic-ui-react";
// @ts-ignore
import {TeamDTO, TeamDTOMapper} from "../../../base/dto/teams/TeamDTO";
import ServiceComponent from "../../service/ServiceComponent";

interface TierTeamAssignerComponentProps {
    onAssignFunction: (teams: TeamDTO[]) => void
}

interface TierTeamAssignerComponentState {
    teams: TeamDTO[],
    assignedTeams: {},
    unAssignedTeams: {},
    name: string,
    modalOpen: boolean,
    invalidName?: boolean,
}

class TierTeamAssignerComponent extends ServiceComponent<TierTeamAssignerComponentProps, TierTeamAssignerComponentState> {

    constructor(props: TierTeamAssignerComponentProps) {
        super(props);
        this.state = {
            teams: [],
            assignedTeams: {},
            unAssignedTeams: {},
            name: '',
            modalOpen: false
        };
    }

    componentDidMount(): void {
        this.reload();
    }

    reload(): void {
        this.reloadTeams();
    }

    reloadTeams() {
        let mapper: TeamDTOMapper = {
            id: true,
            name: true
        };

        this.teamsService.getAllTeams(mapper).then(result => {
            let resultObject = result.data.reduce((obj: any, item) => {
                obj[item.id] = item;
                return obj
            }, {});

            this.setState({
                teams: result.data,
                assignedTeams: resultObject
            })
        });
    }

    assign = (team: TeamDTO) => {
        let assignedTeams = this.state.assignedTeams;
        let unAssignedTeams = this.state.unAssignedTeams;

        // @ts-ignore
        delete unAssignedTeams[team.id];
        // @ts-ignore
        assignedTeams[team.id] = team;
        this.setState({
            assignedTeams: assignedTeams,
            unAssignedTeams: unAssignedTeams
        });
    };

    unAssign = (team: TeamDTO) => {
        let assignedTeams = this.state.assignedTeams;
        let unAssignedTeams = this.state.unAssignedTeams;

        // @ts-ignore
        delete assignedTeams[team.id];
        // @ts-ignore
        unAssignedTeams[team.id] = team;
        this.setState({
            assignedTeams: assignedTeams,
            unAssignedTeams: unAssignedTeams
        });
    };

    createTournament = () => {
        let assignedTeamObject = this.state.assignedTeams;
        let matchesArray: TeamDTO[] = Object.keys(assignedTeamObject).map(function(index){
            // @ts-ignore
            let team = assignedTeamObject[index] as TeamDTO;
            return team;
        });
        this.props.onAssignFunction(matchesArray);
        this.handleClose();
    };

    handleOpen = () => this.setState({ modalOpen: true });

    handleClose = () => this.setState({ modalOpen: false });

    render() {
        return (
            <div style={ButtonStyle}>
                <Modal
                    open={this.state.modalOpen}
                    onClose={this.handleClose}
                    trigger={
                        <button className="ui inverted yellow basic button" onClick={this.handleOpen}>Fix Tier</button>
                    }
                    basic style={ModalStyle}>
                    <Grid>
                        <Grid.Row>
                            <Grid columns={2}>
                                <Grid.Column  style={UnassignedColumnStyle}>
                                    <Grid.Row>
                                        <Header inverted size={"medium"}>Unassigned</Header>
                                        <Divider style={DividerStyle} />
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Table celled inverted basic='very'>
                                            <Table.Body>
                                                {
                                                    this.state.unAssignedTeams && Object.keys(this.state.unAssignedTeams).map((id) => {
                                                        // @ts-ignore
                                                        let team = this.state.unAssignedTeams[id];
                                                        return (
                                                            <Table.Row>
                                                                <Table.Cell><h4>{team.name}</h4></Table.Cell>
                                                                <Table.Cell>
                                                                    <button onClick={() => this.assign(team)} className="ui inverted green basic button">Add</button>
                                                                </Table.Cell>
                                                            </Table.Row>
                                                        )
                                                    })
                                                }
                                            </Table.Body>
                                        </Table>
                                    </Grid.Row>
                                </Grid.Column>
                                <Grid.Column style={AssignedColumnStyle}>
                                    <Grid.Row>
                                        <Header inverted size={"medium"}>In Tournament</Header>
                                        <Divider style={DividerStyle} />
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Table celled inverted basic='very'>
                                            <Table.Body>
                                                {
                                                    this.state.assignedTeams && Object.keys(this.state.assignedTeams).map((id) => {
                                                        // @ts-ignore
                                                        let team = this.state.assignedTeams[id];
                                                        return (
                                                            <Table.Row>
                                                                <Table.Cell><h4>{team.name}</h4></Table.Cell>
                                                                <Table.Cell>
                                                                    <button onClick={() => this.unAssign(team)} className="ui inverted red basic button">Remove</button>
                                                                </Table.Cell>
                                                            </Table.Row>

                                                        )
                                                    })
                                                }
                                            </Table.Body>
                                        </Table>
                                    </Grid.Row>
                                </Grid.Column>
                            </Grid>
                        </Grid.Row>
                        <Grid.Row>
                            <button onClick={() => this.createTournament()} className="ui inverted green basic button" style={CreateTournamentStyle}>Start Tournament</button>
                        </Grid.Row>
                    </Grid>
                </Modal>
            </div>
        )
    }

}

const CreateTournamentStyle = {
    margin: "auto"
};

const DividerStyle = {
    borderBottom: "double rgba(255, 255, 255, 0.55)"
};

const AssignedColumnStyle = {
    borderTop: "double",
    borderBottom: "double",
    borderBottomRightRadius: "20px",
    borderTopRightRadius: "20px",
    borderTopColor: "green",
    borderBottomColor: "green",
    background: "#0080000d",
    minHeight: "50vh",
    borderLeft: "double",
    borderLeftColor: "rgba(255, 255, 255, 0.55)",
    height: "100%"
};

const UnassignedColumnStyle = {
    borderTop: "double",
    borderBottom: "double",
    borderBottomLeftRadius: "20px",
    borderTopLeftRadius: "20px",
    borderTopColor: "#d24a20",
    borderBottomColor: "#d24a20",
    background: "rgba(181, 23, 23, 0.13)",
    minHeight: "50vh",
    borderRight: "double",
    borderRightColor: "rgba(255, 255, 255, 0.55)",
    height: "100%"
};

const ModalStyle = {
    textAlign: 'center' as 'center',
    top: "20%",
    width: "20%"
};

const ButtonStyle = {
    height: "100%",
    textAlign: "center" as "center",
    float: "right" as "right"
};

export default TierTeamAssignerComponent
