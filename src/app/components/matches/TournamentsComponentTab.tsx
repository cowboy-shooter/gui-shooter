import React from 'react'
// @ts-ignore
import ServiceComponent from "../service/ServiceComponent";
import {UserUtils} from "../../utils/UserUtils";
import CreateTournamentComponent from "./create-tournament/CreateTournamentComponent";
import {Grid, Header, Modal, Pagination, PaginationProps, Table} from "semantic-ui-react";
import {Page} from "../../base/dto/paging/Page";
import {PageInfo} from "../../base/dto/paging/PageInfo";
import {Properties, Settings} from "../../../Settings";
import {Sorting, SortingDirection} from "../../base/dto/paging/Sorting";
import SortableHeaderCell from "../table/SortableHeaderCell";
import {TournamentDTO, TournamentDTOMapper} from "../../base/dto/tournaments/TournamentDTO";
import TournamentDetailComponent from "./tournament-detail/TournamentDetailComponent";
import ReloadingImageComponent from "../table/ReloadingImageComponent";
import {DateUtils} from "../../utils/DateUtils";

interface TournamentsComponentTabState {
    pageInfo?: PageInfo,
    tournaments: TournamentDTO[]
}

export class TournamentsComponentTab extends ServiceComponent<{}, TournamentsComponentTabState> {

    private page: Page;
    private interval: any;

    constructor(props: {}) {
        super(props);
        this.state = {
            tournaments: []
        };
        this.page = {
            page: 0,
            size: Settings.getProperty(Properties.MATCHES_PAGE_SIZE) as number,
            sorting: {
                sortBy: 'started',
                sortDirection: SortingDirection.DESC
            }
        };
    }

    componentDidMount(): void {
        this.reload();
        this.interval = setInterval(() => this.reload(), Settings.getProperty(Properties.TOURNAMENTS_REFRESH_RATE) as number);
    }

    componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    onChange = (event: React.MouseEvent, pageInfo: PaginationProps) => {
        this.page = {
            page: pageInfo.activePage as number - 1,
            size: this.page.size,
            sorting: this.page.sorting
        };
        this.reloadTournaments();
    };

    reload = () => {
        this.reloadTournaments().then(this.reloadFlags);
    };

    reloadTournaments = () => {
        let mapper: TournamentDTOMapper = {
            id: true,
            name: true,
            started: true,
            winner: {
                name: true
            }
        };

        return this.tournamentsService.getTournaments(this.page, mapper).then(result => {
            let tournamentsObject = result.data.content.reduce((obj: any, item) => {
                obj[item.id] = item;
                return obj
            }, {});

            if (this.state.tournaments) {
                this.state.tournaments.forEach((tournament) => {
                    if (tournamentsObject[tournament.id] && tournamentsObject[tournament.id].winner && tournament.winner) {
                        tournamentsObject[tournament.id].winner.flag = tournament.winner.flag;
                    }
                })
            }

            let tournamentsArray = Object.keys(tournamentsObject).map(function(teamIdIndex){
                let tournament = tournamentsObject[teamIdIndex];
                return tournament;
            });

            this.setState({
                tournaments: tournamentsArray,
                pageInfo: result.data.pageInfo
            });
        });
    };

    reloadFlags = () => {
        let mapper: TournamentDTOMapper = {
            id: true,
            winner: {
                flag: true
            }
        };

        this.tournamentsService.getTournaments(this.page, mapper).then(result => {

            let resultObject = result.data.content.reduce((obj: any, item) => {
                obj[item.id] = item;
                return obj
            }, {});

            let tournaments = this.state.tournaments.map((tournament) => {
                if (resultObject[tournament.id] && tournament.winner) {
                    tournament.winner.flag = resultObject[tournament.id].winner.flag;
                }
                return tournament;
            });

            this.setState({
                tournaments: tournaments,
                pageInfo: result.data.pageInfo
            });
        });
    };

    changeSorting = (sorting: Sorting) => {
        this.page = {
            page: this.page.page,
            size: this.page.size,
            sorting: sorting
        };
        this.reloadTournaments();
    };

    render() {
        return (
            <Grid style={GridStyle} textAlign={"center"}>
                <Grid.Row>
                    {UserUtils.isAdmin() ? <CreateTournamentComponent/> : null}
                </Grid.Row>
                <Grid.Row>

                </Grid.Row>
                <Grid.Row>
                    <Grid columns={1}>
                        <Grid.Column style={ColumnStyle}>
                            <Header size={"huge"} style={TableHeaderStyle}>
                                Tournaments
                            </Header>
                            <Table celled inverted selectable basic='very'>
                                <Table.Header>
                                    <Table.Row>
                                        <SortableHeaderCell value={"Name"} sortBy={"name"} sorting={this.page.sorting} changeSorting={this.changeSorting}/>
                                        <Table.HeaderCell colSpan='2'><h2>Winner</h2></Table.HeaderCell>
                                        <SortableHeaderCell value={"Date"} sortBy={"started"} sorting={this.page.sorting} changeSorting={this.changeSorting}/>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {
                                        this.state.tournaments && this.state.tournaments.map((tournament) => (
                                            <Modal basic trigger={<Table.Row>
                                                <Table.Cell>{tournament.name}</Table.Cell>
                                                <Table.Cell>{tournament.winner? tournament.winner.name: null}</Table.Cell>
                                                <Table.Cell>{tournament.winner? <ReloadingImageComponent image={tournament.winner.flag} loaderSize={'medium'} imageHeight={50} imageWidth={100}/>: null}</Table.Cell>
                                                <Table.Cell>{DateUtils.formatDate(tournament.started)}</Table.Cell>
                                            </Table.Row>} style={ModalStyle}>
                                                <TournamentDetailComponent tournamentId={tournament.id}/>
                                            </Modal>
                                        ))
                                    }
                                </Table.Body>
                            </Table>
                            {
                                this.state.pageInfo ? (
                                    <Pagination
                                        defaultActivePage={1}
                                        siblingRange={3}
                                        totalPages={this.state.pageInfo.totalPages}
                                        onPageChange={this.onChange}
                                        pointing
                                        secondary
                                        inverted
                                    />
                                ) : null
                            }
                        </Grid.Column>
                    </Grid>
                </Grid.Row>
            </Grid>
        )
    }
}

const TableHeaderStyle = {
    color: "rgba(216, 117, 51, 0.9)"
};

const ColumnStyle = {
    border: "double",
    borderColor: "#9c9696",
    borderRadius: "20px",
    height: "50vh",
    backgroundColor: "#9c96961c"
};

const ModalStyle = {
    textAlign: 'center' as 'center',
    top: "50px",
    width: "70%"
};

const GridStyle = {
    textAlign: "center",
    margin: "auto",
    width: "60%"
};


export default TournamentsComponentTab