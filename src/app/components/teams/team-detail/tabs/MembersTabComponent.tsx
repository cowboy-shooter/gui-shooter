import * as React from "react";
import {Button, Grid, Header, Popup, SemanticWIDTHS} from "semantic-ui-react";
import {TeamDTO} from "../../../../base/dto/teams/TeamDTO";
import {TeamUtils} from "../../../../utils/TeamUtils";
import {UserDTO} from "../../../../base/dto/teams/UserDTO";
import Gravatar from "react-gravatar";
import ServiceComponent from "../../../service/ServiceComponent";

interface MembersTabProps {
    team: TeamDTO,
    onChange: () => void
}

class MembersTabComponent extends ServiceComponent<MembersTabProps> {

    leaveTeam() {
        this.teamsService.leaveTeam().then(() => this.props.onChange());
    };

    joinTeam(team: TeamDTO) {
        this.teamsService.requestJoining(team).then(() => this.props.onChange());
    };

    cancelJoinTeam(team: TeamDTO) {
        this.teamsService.cancelRequestJoining(team).then(() => this.props.onChange());
    };

    acceptJoinTeam(team: TeamDTO) {
        this.teamsService.acceptJoinRequest(team).then(() => this.props.onChange());
    };

    kickMemberOut(user: UserDTO) {
        this.teamsService.kickMemberOut(user).then(() => this.props.onChange());
    };

    makeMemberLeader(user: UserDTO) {
        this.teamsService.makeMemberLeader(user).then(() => this.props.onChange());
    };

    render() {
        const {team} = this.props;
        let isMember = TeamUtils.isMemberOfTeam(team);
        let isInvited = TeamUtils.isInvited(team);
        let isLeader = TeamUtils.isLeader(team);
        let isRequestor = TeamUtils.isRequestor(team);

        let teamOptions = (<div/>);

        if (isMember) {
            teamOptions = (
                <Button onClick={() => this.leaveTeam()} className="ui inverted red basic button" fluid style={TeamOptionButton}><b>Leave Team</b></Button>
            );
        } else if (team.members.length < 3) {
            if (isInvited) {
                teamOptions = <Button onClick={() => this.acceptJoinTeam(team)} className="ui inverted green basic button" fluid style={TeamOptionButton}><b>Accept joining</b></Button>;
            } else if (isRequestor) {
                teamOptions = <Button onClick={() => this.cancelJoinTeam(team)} className="ui inverted red basic button" fluid style={TeamOptionButton}><b>Cancel Request to join team</b></Button>;
            } else if (!isMember) {
                teamOptions = <Button onClick={() => this.joinTeam(team)} className="ui inverted yellow basic button" fluid style={TeamOptionButton}><b>Join Team</b></Button>;
            }
        }

        return (
            <Grid columns={team.members.length + "" as SemanticWIDTHS} textAlign={"center"}>
                <Grid.Row>
                    {
                        team.members.map((member) => {
                            return (
                                <Grid.Column>
                                    {member.id === team.leader.id ?
                                        (<Header as={"h2"} color={"orange"}>{member.firstName + " " + member.lastName}</Header>)
                                        :
                                        (<Header as={"h2"} color={"grey"}>{member.firstName + " " + member.lastName}</Header>)
                                    }
                                    {<Popup
                                        content={`${member.firstName} ${member.lastName} is a ${member.id === team.leader.id? "leader": "member"} of this team`}
                                        header={member.email}
                                        position='bottom center'
                                        trigger={<Gravatar email={member.email} size={200} style={GravatarStyle}/>}
                                    />}
                                    {
                                        isLeader && member.id !== team.leader.id ? (
                                            <div style={MemberOptions}>
                                                <Button onClick={() => this.makeMemberLeader(member)} className="ui inverted orange basic button"><b>Make Leader</b></Button>
                                                <Button onClick={() => this.kickMemberOut(member)}className="ui inverted red basic button"><b>Kick out</b></Button>
                                            </div>
                                        ): null
                                    }
                                </Grid.Column>
                            )
                        })
                    }
                </Grid.Row>
                <Grid.Row>
                    {
                        teamOptions
                    }
                </Grid.Row>
            </Grid>
        )
    }
}

const TeamOptionButton = {
    height: "50px"
};

const GravatarStyle = {
    borderRadius: "50%",
    height: "auto"
};

const MemberOptions = {
    paddingTop: "15px"
};

export default MembersTabComponent
