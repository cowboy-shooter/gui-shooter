import * as React from "react";
import {
    UserTeamWrappedUserDTO,
    UserTeamWrappedUserType,
    UserTeamWrapperUserDTOMapper
} from "../../../../base/dto/teams/UserTeamWrappedUserDTO";
import {PageInfo} from "../../../../base/dto/paging/PageInfo";
import {Page} from "../../../../base/dto/paging/Page";
import {Button, Pagination, PaginationProps, Table} from "semantic-ui-react";
import Gravatar from "react-gravatar";
import {UserDTO} from "../../../../base/dto/teams/UserDTO";
import {Properties, Settings} from "../../../../../Settings";
import ServiceComponent from "../../../service/ServiceComponent";
import {SortingDirection} from "../../../../base/dto/paging/Sorting";

interface TeamManagementComponentState {
    userWrapper: UserTeamWrappedUserDTO[],
    pageInfo?: PageInfo
}

class ManagementTabComponent extends ServiceComponent<{}, TeamManagementComponentState> {

    private page: Page;

    constructor(props: {}) {
        super(props);
        this.state = {
            userWrapper: []
        };
        this.page = {
            page: 0,
            size: Settings.getProperty(Properties.TEAMS_MEMBER_MANAGEMENT_PAGE_SIZE) as number,
            sorting: {
                sortBy: 'firstName',
                sortDirection: SortingDirection.ASC
            }
        };
    }

    componentDidMount(): void {
        this.reloadUsers();
    }

    reloadUsers() {
        let teamMapper: UserTeamWrapperUserDTOMapper = {
            user: {
                id: true,
                email: true,
                lastName: true,
                firstName: true
            }, teamUserManagementType: true
        };

        this.teamUserService.getOwnTeamManagement(this.page, teamMapper)
            .then((result) => {
                this.setState({
                    userWrapper: result.data.content,
                    pageInfo: result.data.pageInfo
                });
            });
    }

    onPageChange = (event: React.MouseEvent, pageInfo: PaginationProps) => {
        this.page = {
            page: pageInfo.activePage as number - 1,
            size: this.page.size,
            sorting: this.page.sorting
        };
        this.reloadUsers();
    };

    inviteMember(user: UserDTO) {
        this.teamsService.inviteMember(user).then(
            () => this.reloadUsers());
    };

    cancelInvite(user: UserDTO) {
        this.teamsService.cancelInvite(user).then(
            () => this.reloadUsers());
    };

    acceptRequest(user: UserDTO) {
        this.teamsService.acceptRequest(user).then(
            () => this.reloadUsers());
    };

    render() {
        let pagination = null;

        if (this.state.pageInfo) {
            pagination = <Pagination
                defaultActivePage={1}
                siblingRange={3}
                totalPages={this.state.pageInfo.totalPages}
                onPageChange={this.onPageChange}
                pointing
                secondary
                inverted
                style={PaginationStyle}
            />
        }

        const getButton = (teamUserManagementType: UserTeamWrappedUserType, user: UserDTO): React.ReactNode => {
            switch (teamUserManagementType) {
                case UserTeamWrappedUserType.INVITED:
                    return <Button onClick={() => this.cancelInvite(user)} className="ui inverted red basic button" fluid><b>Cancel</b></Button>;
                case UserTeamWrappedUserType.REQUESTOR:
                    return <Button onClick={() => this.acceptRequest(user)} className="ui inverted yellow basic button" fluid><b>Accept</b></Button>;
                case UserTeamWrappedUserType.NONE:
                    return <Button onClick={() => this.inviteMember(user)} className="ui inverted green basic button" fluid><b>Invite</b></Button>;
            }
            console.log(teamUserManagementType);
        };

        return (
            <div>
                <Table inverted basic='very'>
                    <Table.Body>
                        {
                            this.state.userWrapper && this.state.userWrapper.map((wrapper) => (
                                <Table.Row>
                                    <Table.Cell>
                                        <Gravatar size={50} email={wrapper.user.email}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                        {`${wrapper.user.firstName} ${wrapper.user.lastName}`}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {getButton(wrapper.teamUserManagementType, wrapper.user)}
                                    </Table.Cell>
                                </Table.Row>
                            ))
                        }
                    </Table.Body>
                </Table>
                {
                    pagination
                }
            </div>
        )
    }
}

const PaginationStyle = {

};


export default ManagementTabComponent
