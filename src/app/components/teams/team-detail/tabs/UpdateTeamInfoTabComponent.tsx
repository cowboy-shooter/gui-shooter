import * as React from "react";
import {TeamDTO} from "../../../../base/dto/teams/TeamDTO";
import TeamInfoComponent, {TeamInfoProps} from "../../add-team/TeamInfoComponent";
import ServiceComponent from "../../../service/ServiceComponent";

interface UpdateTeamProps {
    team: TeamDTO,
    onChange: () => void
}

class UpdateTeamInfoTabComponent extends ServiceComponent<UpdateTeamProps> {

    updateTeam = (teamInfo: TeamInfoProps) => {
        this.teamsService
            .updateTeam(teamInfo.teamName, teamInfo.url, teamInfo.description, teamInfo.language, teamInfo.teamFlag)
            .then(this.props.onChange);
    };

    render() {
        const {team} = this.props;
        console.log(team);
        return (
            <TeamInfoComponent teamName={team.name} url={team.url} teamFlag={team.flag} language={team.language} description={team.description} onSubmit={this.updateTeam} removeIcon={true}></TeamInfoComponent>
        )
    }
}

export default UpdateTeamInfoTabComponent
