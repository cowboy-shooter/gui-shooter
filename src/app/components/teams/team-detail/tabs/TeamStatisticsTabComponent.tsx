import * as React from "react";
import ServiceComponent from "../../../service/ServiceComponent";
import {TeamDTO} from "../../../../base/dto/teams/TeamDTO";
import {MatchDTO, MatchDTOMapper} from "../../../../base/dto/matches/MatchDTO";
import {TeamMoveDTO, TeamMoveType} from "../../../../base/dto/matches/TeamMoveDTO";
import {Grid} from "semantic-ui-react";
import PieChartComponent, {ChartData, PieChartComponentPrefabs} from "../../../charts/PieChartComponent";


interface TeamStatisticsTabComponentProps {
    team: TeamDTO
}

interface TeamStatisticsTabComponentState {
    matches: MatchDTO[],
    winRateData: ChartData[],
    actionsData: ChartData[],
    failData: ChartData[]
}

class TeamStatisticsTabComponent extends ServiceComponent<TeamStatisticsTabComponentProps, TeamStatisticsTabComponentState> {

    constructor(props: TeamStatisticsTabComponentProps) {
        super(props);
        this.state = {
            matches: [],
            failData: [],
            winRateData: [],
            actionsData: []
        };
    }

    componentDidMount(): void {
        this.reload();
    }

    reload() {
        let mapper: MatchDTOMapper = {
            id: true,
            team1: {
                id: true,
                name: true,
                description: true,
                language: true,
            }, team2: {
                id: true,
                name: true,
                description: true,
                language: true,
            }, winner: {
                id: true,
                name: true,
                description: true,
                language: true,
            }, moves: {
                team1Move: {
                    teamMove: true,
                    faulty: true,
                    message: true
                },
                team2Move: {
                    teamMove: true,
                    faulty: true,
                    message: true
                },
                team1State: {
                    bullets: true,
                    blockCounter: true,
                    evadeCounter: true,
                    isDead: true
                },
                team2State: {
                    bullets: true,
                    blockCounter: true,
                    evadeCounter: true,
                    isDead: true
                }
            }
        };
        this.matchService.getParticipatedMatches(this.props.team.id, mapper)
            .then(content => {
               this.gatherStatistics(content.data);
            })
    }

    gatherStatistics(matches: MatchDTO[]) {
        let wins = 0;
        let loses = 0;
        let draws = 0;

        let shooting = 0;
        let evading = 0;
        let blocking = 0;
        let reloading = 0;
        let failing = 0;

        let failShooting = 0;
        let failEvading = 0;
        let failBlocking = 0;
        let failReloading = 0;
        let errorResponses = 0;

        let countAction = (move: TeamMoveDTO) => {
            switch (move.teamMove) {
                case TeamMoveType.SHOOT:
                    if (move.faulty) {
                        failing++;
                        failShooting++;
                    } else {
                        shooting++;
                    }
                    break;
                case TeamMoveType.BLOCK:
                    if (move.faulty) {
                        failing++;
                        failBlocking++;
                    } else {
                        blocking++;
                    }
                    break;
                case TeamMoveType.EVADE:
                    if (move.faulty) {
                        failing++;
                        failEvading++;
                    } else {
                        evading++;
                    }
                    break;
                case TeamMoveType.RELOAD:
                    if (move.faulty) {
                        failing++;
                        failReloading++;
                    } else {
                        reloading++;
                    }
                    break;
                case TeamMoveType.NULL:
                    errorResponses++;
                    failing++;
                    break;
            }
        };

        matches.forEach(
            match => {
                if (!match.winner) {
                    draws++;
                } else if (this.props.team.id === match.winner.id) {
                    wins++;
                } else {
                    loses++;
                }
                if (match.team1.id === this.props.team.id) {
                    match.moves.forEach(
                        move => {
                            countAction(move.team1Move)
                        }
                    )
                } else {
                    match.moves.forEach(
                        move => {
                            countAction(move.team2Move)
                        }
                    )
                }
            }
        );

        let winRateData: ChartData[] = [
            {name: "Winning", value: wins, color: "#2ffe17"},
            {name: "Loses", value: loses, color: "#fe676b"},
            {name: "Draws", value: draws, color: "#47b4fe"}
        ];

        let actionsData: ChartData[] = [
            {name: "Shooting", value: shooting, color: "#fec21b"},
            {name: "Evading", value: evading, color: "#fbfe0d"},
            {name: "Blocking", value: blocking, color: "#f525fe"},
            {name: "Reloading", value: reloading, color: "#0088fe"},
            {name: "Failing", value: failing, color: "#fe676b"}
        ];

        let failData: ChartData[] = [
            {name: "Fail Shooting", value: failShooting, color: "#fe5b0f"},
            {name: "Fail Evading", value: failEvading, color: "#fe1f1e"},
            {name: "Fail Blocking", value: failBlocking, color: "#fe186c"},
            {name: "Fail Reloading", value: failReloading, color: "#fe1067"},
            {name: "Error Responses", value: errorResponses, color: "#fe10a4"}
        ];

        this.setState({
            matches: matches,
            winRateData: this.fixStatsArray(winRateData),
            actionsData: this.fixStatsArray(actionsData),
            failData: this.fixStatsArray(failData)
        });
    }

    fixStatsArray = (data: ChartData[]): ChartData[] => {
        let result: ChartData[] = [];

        data.forEach((chartData) => {
            if(chartData.value > 0) {
                result.push(chartData);
            }
        });

        return result;
    };

    render() {
        let winRateCount = this.state.winRateData.length;
        let actionRateCount = this.state.actionsData.length;
        let failsRateCount = this.state.failData.length;

        return (
            <Grid columns={3}>
                <Grid.Column>
                    { this.state.winRateData && winRateCount > 0 ? <PieChartComponent data={this.state.winRateData} title={WinRateChartTitle} prefab={PieChartComponentPrefabs.BIG}/> : null}
                </Grid.Column>
                <Grid.Column>
                    { this.state.actionsData && actionRateCount > 0 ? <PieChartComponent data={this.state.actionsData} title={ActionsChartTitle} prefab={PieChartComponentPrefabs.BIG}/> : null}
                </Grid.Column>
                <Grid.Column>
                    { this.state.failData && failsRateCount > 0 ? <PieChartComponent data={this.state.failData} title={FailsChartTitle} prefab={PieChartComponentPrefabs.BIG}/> : null}
                </Grid.Column>
            </Grid>
        )
    }
}

const WinRateChartTitle = "Win Rate";
const ActionsChartTitle = "Actions";
const FailsChartTitle = "Fails";

export default TeamStatisticsTabComponent
