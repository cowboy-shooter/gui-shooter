import * as React from "react";
import {Grid, Menu, Tab, TabProps} from "semantic-ui-react";
import {TeamDTO, TeamDTOMapper} from "../../../base/dto/teams/TeamDTO";
import ManagementTabComponent from "./tabs/ManagementTabComponent";
import {TeamUtils} from "../../../utils/TeamUtils";
import TeamCard from "../card/TeamCard";
import MembersTabComponent from "./tabs/MembersTabComponent";
import UpdateTeamInfoTabComponent from "./tabs/UpdateTeamInfoTabComponent";
import ServiceComponent from "../../service/ServiceComponent";
import TeamStatisticsTabComponent from "./tabs/TeamStatisticsTabComponent";
import CenteredLoader from "../../loader/CenteredLoader";


export interface TeamDetailProps {
    teamId: string
}

interface TeamTableRowState {
    team?: TeamDTO,
    isMember: boolean,
    isLeader: boolean,
    isRequestor: boolean,
    isInvited: boolean,
    tabIndex: number
}

export class TeamDetailComponent extends ServiceComponent<TeamDetailProps, TeamTableRowState> {

    constructor(props: TeamDetailProps) {
        super(props);
        this.state = {
            team: undefined,
            tabIndex: 0,
            isMember: false,
            isLeader: false,
            isRequestor: false,
            isInvited: false
        };
    }

    componentDidMount() {
        this.reloadTeam();
    }

    reloadTeam = () => {
        let teamMapper: TeamDTOMapper = {
            id: true,
            name: true,
            url: true,
            language: true,
            description: true,
            flag: true,
            members: {
                id: true,
                firstName: true,
                lastName: true,
                email: true
            }, requestors: {
                id: true
            }, invitees: {
                id: true
            }, leader: {
                id: true
            }
        };
        this.teamsService.getTeam(this.props.teamId, teamMapper).then(result => {
            let team = result.data;
            if (team === null || team === undefined) {
                this.setState({
                    team: undefined,
                    isMember: false,
                    isLeader: false,
                    isRequestor: false,
                    isInvited: false
                })
            } else {
                this.setState({
                    team: team,
                    isMember: TeamUtils.isMemberOfTeam(team),
                    isLeader: TeamUtils.isLeader(team),
                    isRequestor: TeamUtils.isRequestor(team),
                    isInvited: TeamUtils.isInvited(team)
                })
            }
        });
    };

    onTabChange = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, data: TabProps) => {
        this.setState({
            tabIndex: data['activeIndex'] as number
        });
    };

    render() {
        const {team, isLeader, isMember} = this.state;

        if (!team) {
            return  (
                <CenteredLoader/>
            )
        }

        let tabs = [];
        tabs.push(
            {
                menuItem: (
                    <Menu.Item key='members' style={this.state.tabIndex === tabs.length ? activeTabStyle: defaultTabStyle}>
                        Members
                    </Menu.Item>
                ),
                render: () => (
                    <Tab.Pane style={PanelContentStyle}>
                        <MembersTabComponent team={team} onChange={this.reloadTeam}></MembersTabComponent>
                    </Tab.Pane>
                ),
            }
        );

        if (isMember) {
            tabs.push(
                {
                    menuItem: (
                        <Menu.Item key='management' style={this.state.tabIndex === tabs.length ? activeTabStyle : defaultTabStyle}>
                            Management
                        </Menu.Item>
                    ),
                    render: () => (
                        <Tab.Pane style={PanelContentStyle}>
                            <ManagementTabComponent/>
                        </Tab.Pane>
                    )
                }
            );
        }

        if (isLeader) {
            tabs.push(
                {
                    menuItem: (
                        <Menu.Item key='team' style={this.state.tabIndex === tabs.length ? activeTabStyle : defaultTabStyle}>
                            Team
                        </Menu.Item>
                    ),
                    render: () => (
                        <Tab.Pane style={PanelContentStyle}>
                            <UpdateTeamInfoTabComponent onChange={this.reloadTeam} team={team}></UpdateTeamInfoTabComponent>
                        </Tab.Pane>
                    )
                }
            );
        }
        tabs.push(
            {
                menuItem: (
                    <Menu.Item key='statistics' style={this.state.tabIndex === tabs.length ? activeTabStyle: defaultTabStyle}>
                        Statistics
                    </Menu.Item>
                ),
                render: () => (
                    <Tab.Pane style={PanelContentStyle}>
                        <TeamStatisticsTabComponent team={team}/>
                    </Tab.Pane>
                ),
            }
        );

        return (
            <div>
                <Grid columns="1" textAlign={"center"}>
                    <Grid.Row>
                        <TeamCard team={team} imageSize={300}/>
                    </Grid.Row>
                    <Grid.Row>
                        <Tab panes={tabs} onTabChange={this.onTabChange} style={semanticTabStyle}/>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

const PanelContentStyle = {
    backgroundColor: "transparent",
    border: "none",
    minWidth: "30%"
};

const defaultTabStyle = {
    color: "white",
    backgroundColor: "transparent",
};

const activeTabStyle = {
    color: "white",
    backgroundColor: "rgba(0, 86, 204, 0.33)",
};

const semanticTabStyle = {
    width: "100%"
};

export default TeamDetailComponent
