import React from 'react'
import {Grid, Header, Pagination, PaginationProps, Table} from "semantic-ui-react";
import NavigationComponent from "../navigation/NavigationComponent";
import {RoutingPaths} from "../../base/RoutingPaths";
import UserBarComponent from "../user/user-bar/UserBarComponent";
import TeamTableRow from "./team-table-row/TeamTableRow";
import {TeamDTO, TeamDTOMapper} from "../../base/dto/teams/TeamDTO";
import {Page} from "../../base/dto/paging/Page";
import {PageInfo} from "../../base/dto/paging/PageInfo";
import {TeamUtils} from "../../utils/TeamUtils";
import AddTeamComponent from "./add-team/AddTeamComponent";
import {faUsers} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Properties, Settings} from "../../../Settings";
import ServiceComponent from "../service/ServiceComponent";
import {Sorting, SortingDirection} from "../../base/dto/paging/Sorting";
import SortableHeaderCell from "../table/SortableHeaderCell";

interface TeamsComponentState {
    teams: TeamDTO[],
    pageInfo?: PageInfo,
    ownTeam?: TeamDTO,
    ownTeamLoaded: boolean
}

class TeamsComponent extends ServiceComponent<{}, TeamsComponentState> {

    private page: Page;
    private interval: any;

    constructor(props: {}) {
        super(props);
        this.state = {
            teams: [],
            ownTeamLoaded: false
        };
        this.page = {
            page: 0,
            size: Settings.getProperty(Properties.TEAMS_PAGE_SIZE) as number,
            sorting: {
                sortBy: 'name',
                sortDirection: SortingDirection.ASC
            }
        };
    }

    componentDidMount() {
        this.reload();
        this.interval = setInterval(() => this.reload(), Settings.getProperty(Properties.TEAMS_REFRESH_RATE) as number);
    }

    componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    reload = () => {
        this.reloadTeams();
        this.reloadOwnTeam();
    };

    reloadTeams() {
        let teamMapper: TeamDTOMapper;
        teamMapper = {
            id: true,
            name: true,
            members: {
                id: true,
                email: true
            }, invitees: {
                id: true
            }, requestors: {
                id: true
            }, leader: {
                id: true
            }
        };

        this.teamsService.getTeams(this.page, teamMapper).then(result => {
            if (!result.data) {
                return;
            }

            let teamsObject = result.data.content.reduce((obj: any, item) => {
                obj[item.id] = item;
                return obj
            }, {});

            if (this.state.teams) {
                this.state.teams.forEach((team) => {
                    if (teamsObject[team.id]) {
                        teamsObject[team.id].flag = team.flag;
                    }
                })
            }

            let teamsArray = Object.keys(teamsObject).map(function(teamIdIndex){
                let team = teamsObject[teamIdIndex];
                return team;
            });

            this.setState({
                teams: teamsArray,
                pageInfo: result.data.pageInfo
            })
        });
        this.reloadTeamsFlags();
    }

    reloadTeamsFlags() {
        let teamMapper: TeamDTOMapper;
        teamMapper = {
            id: true,
            flag: true,
        };

        this.teamsService.getTeams(this.page, teamMapper).then(result => {
            if (!result.data) {
                return;
            }

            let resultObject = result.data.content.reduce((obj: any, item) => {
                obj[item.id] = item;
                return obj
            }, {});

            let teams = this.state.teams.map((team) => {
                if (resultObject[team.id]) {
                    team.flag = resultObject[team.id].flag;
                }
                return team;
            });

            this.setState({
                teams: teams
            })
        });
    }

    reloadOwnTeam() {
        let mapper: TeamDTOMapper = {
            id: true
        };

        this.teamsService.getOwnTeam(mapper).then(result => {
            this.onOwnTeamLoaded(result.data);
        });
    };

    onOwnTeamLoaded = (ownTeam: TeamDTO) => {
        this.setState({
            ownTeam: ownTeam,
            ownTeamLoaded: true
        })
    };

    onChange = (event: React.MouseEvent, pageInfo: PaginationProps) => {
        this.page = {
            page: pageInfo.activePage as number - 1,
            size: this.page.size,
            sorting: this.page.sorting
        };
        this.reloadTeams();
    };

    changeSorting = (sorting: Sorting) => {
        this.page = {
            page: this.page.page,
            size: this.page.size,
            sorting: sorting
        };
        this.reloadTeams();
    };

    render() {
        let disabledAddTeamButton = true;
        if (!this.state.ownTeam && this.state.ownTeamLoaded) {
            disabledAddTeamButton = false;
        }

        return (
            <div style={WrapperStyle}>
                <UserBarComponent/>
                <NavigationComponent path={RoutingPaths.TEAMS}/>
                <FontAwesomeIcon icon={faUsers} style={HeaderIconStyle}/>
                <Grid columns={1} style={GridStyle} textAlign={"center"}>
                    <Grid.Row>
                        <AddTeamComponent disabled={disabledAddTeamButton} onTeamLoad={this.onOwnTeamLoaded}/>
                    </Grid.Row>
                    <Grid.Column style={ColumnStyle}>
                        <Header size={"huge"} style={TableHeaderStyle}>
                            Teams
                        </Header>
                        <Table celled inverted selectable basic='very'>
                            <Table.Header>
                                <Table.Row>
                                    <SortableHeaderCell value={"Name"} sortBy={"name"} sorting={this.page.sorting} changeSorting={this.changeSorting}/>
                                    <Table.HeaderCell><h2>Flag</h2></Table.HeaderCell>
                                    <Table.HeaderCell><h2>Members</h2></Table.HeaderCell>
                                    <Table.HeaderCell><h2>State</h2></Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {
                                    this.state.teams.map((team) => <TeamTableRow team={team} isInvited={TeamUtils.isInvited(team)} isLeader={TeamUtils.isLeader(team)} isMember={TeamUtils.isMemberOfTeam(team)} isRequestor={TeamUtils.isRequestor(team)}/>)
                                }
                            </Table.Body>
                        </Table>
                        {
                            this.state.pageInfo ? (
                                <Pagination
                                    defaultActivePage={1}
                                    siblingRange={3}
                                    totalPages={this.state.pageInfo.totalPages}
                                    onPageChange={this.onChange}
                                    pointing
                                    secondary
                                    inverted
                                />
                            ) : null
                        }
                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}

const GridStyle: React.CSSProperties = {
    textAlign: "center",
    margin: "auto",
    width: "60%",
    minWidth: "850px"
};

const ColumnStyle: React.CSSProperties = {
    border: "double",
    borderColor: "#9c9696",
    borderRadius: "20px",
    height: "60vh",
    backgroundColor: "#9c96961c"
};

const WrapperStyle: React.CSSProperties = {
    textAlign: "center" as "center"
};


const HeaderIconStyle: React.CSSProperties = {
    color: "#8ff58f59",
    margin: "auto",
    width: "100%",
    height: "10vh",
    minHeight: "50px"
};

const TableHeaderStyle: React.CSSProperties = {
    color: "rgba(83, 220, 83, 0.68)"
};

export default TeamsComponent
