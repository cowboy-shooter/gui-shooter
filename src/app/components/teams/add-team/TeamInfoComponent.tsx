import * as React from "react";
import {Button, Grid, Input, Label, Select, TextArea} from "semantic-ui-react";
import ImagePickerButton from "../../file-picker/ImagePickerButton";
import {faUserFriends} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ServiceComponent from "../../service/ServiceComponent";
import {Properties, Settings} from "../../../../Settings";

export interface TeamInfoProps {
    teamFlag: string,
    teamName: string,
    url: string,
    description: string,
    language: string,
    removeIcon?: boolean,
    onSubmit: (teamInfoProps: TeamInfoProps) => void
}

interface TeamInfoState extends TeamInfoProps {
    allLanguages: {}[],
    invalidFlag?: boolean,
    invalidTeamName?: boolean,
    invalidContext?: boolean,
    invalidDescription?: boolean,
    invalidLanguage?: boolean,
}

class TeamInfoComponent extends ServiceComponent<TeamInfoProps, TeamInfoState> {

    constructor(props: TeamInfoProps) {
        super(props);
        let url = this.props.url;
        if (!url || url === "") {
            url = (Settings.getProperty(Properties.TEAM_DEFAULT_URL) as string) + "/";
        }
        this.state = {
            teamFlag: this.props.teamFlag,
            teamName: this.props.teamName,
            url: url,
            description: this.props.description,
            language: this.props.language,
            removeIcon: false,
            allLanguages: [],
            onSubmit: () => {}
        };
    }

    componentDidMount(): void {
        this.commonService
            .getProgrammingLanguages()
            .then((result) => {
                this.setState({
                    allLanguages: result.data
                });
            });
        this.validate(this.state);
    }

    onFlagChange = (flagBase64: string) => {
        this.setState({
            teamFlag: flagBase64
        });
        this.validate(this.state);
    };

    submit = () => {
        if (this.validate(this.state)) {
            this.props.onSubmit(this.state);
        }
    };

    encodeContext(context: string): string {
        let result = context;
        result = result.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        result = result.toLowerCase();
        result = result.split(' ').join('-');
        return encodeURI(result);
    }

    validate = (tempState: TeamInfoState) => {
        tempState.invalidFlag = this.state.teamFlag === '';
        tempState.invalidTeamName = this.state.teamName === '' || this.state.teamName.length > 25;
        tempState.invalidContext = this.state.url === '' || this.state.url === '/';
        tempState.invalidDescription = this.state.description === '' || this.state.description.length > 150;
        tempState.invalidLanguage = this.state.language === '';

        this.setState(tempState);
        return !tempState.invalidTeamName
            && !tempState.invalidContext
            && !tempState.invalidFlag
            && !tempState.invalidDescription
            && !tempState.invalidLanguage;
    };

    render() {
        const {invalidDescription, invalidLanguage, invalidTeamName, invalidContext, invalidFlag} = this.state;

        return (
            <Grid>
                {
                    !this.props.removeIcon ? (
                        <Grid.Row>
                            <FontAwesomeIcon icon={faUserFriends} style={HeaderIconStyle}/>
                        </Grid.Row>
                    ) : null
                }
                <Grid.Row>
                    <div style={rowWrapperStyle}>
                        <Label color={invalidFlag ? 'red': 'green'} ribbon style={RibbonStyle}>
                            Flag
                        </Label>
                        <ImagePickerButton file={this.props.teamFlag} onValueChange={this.onFlagChange}/>
                    </div>
                </Grid.Row>
                <Grid.Row>
                    <div style={rowWrapperStyle}>
                        <Label color={invalidTeamName ? 'red': 'green'} ribbon style={RibbonStyle}>
                            Name
                        </Label>
                        <Input
                            onChange={(e) => {
                                let tempState: TeamInfoState = this.state;
                                let encodedOld = (Settings.getProperty(Properties.TEAM_DEFAULT_URL) as string) + "/" + this.encodeContext(tempState.teamName);
                                tempState.teamName = e.target.value;
                                let encodedNew = (Settings.getProperty(Properties.TEAM_DEFAULT_URL) as string) + "/" + this.encodeContext(tempState.teamName);
                                if (tempState.url === encodedOld) {
                                    tempState.url = encodedNew;
                                }
                                this.validate(tempState);
                            }}
                            icon='id card'
                            style={InputStyle}
                            value={this.state.teamName}
                        />
                    </div>
                </Grid.Row>
                <Grid.Row>
                    <div style={rowWrapperStyle}>
                        <Label color={invalidContext ? 'red': 'green'} ribbon style={RibbonStyle}>
                            URL Context
                        </Label>
                        <Input
                            onChange={(e) => {
                                let tempState: TeamInfoState = this.state;
                                let result = this.encodeContext(e.target.value);
                                tempState.url = result;
                                this.validate(tempState);
                            }}
                            icon='paper plane outline'
                            style={InputStyle}
                            value={this.state.url}
                        />
                    </div>
                </Grid.Row>
                <Grid.Row>
                    <div style={rowWrapperStyle}>
                        <Label color={invalidLanguage ? 'red': 'green'} ribbon style={RibbonStyle}>
                            Programming Language
                        </Label>
                        <Select
                            onChange={(e, {value}) => {
                                let tempState: TeamInfoState = this.state;
                                tempState.language = value as string;
                                this.validate(tempState);
                            }}
                            options={this.state.allLanguages}
                            style={InputStyle}
                            value={this.state.language}
                        />
                    </div>
                </Grid.Row>
                <Grid.Row>
                    <div style={rowWrapperStyle}>
                        <Label color={invalidDescription ? 'red': 'green'} ribbon style={RibbonStyle}>
                            Description
                        </Label>
                        <TextArea
                            onChange={(e, {value}) =>{
                                let tempState: TeamInfoState = this.state;
                                tempState.description = value as string;
                                this.validate(tempState);
                            }}
                            style={InputStyle}
                            value={this.state.description}
                        />
                    </div>
                </Grid.Row>
                <Grid.Row>
                    <div style={rowWrapperStyle}>
                        <Button onClick={this.submit} className="ui inverted green basic button" name={"submit"}><b>Submit</b></Button>
                    </div>
                </Grid.Row>
            </Grid>
        )
    }

}

const rowWrapperStyle = {
  margin: "auto"
};

const HeaderIconStyle = {
    width: "100%",
    height: "10vh",
    minHeight: "50px"
};

const RibbonStyle = {
    fontSize: "1em",
    width: "50%",
    marginRight: "100%",
    marginLeft: "15px"
};

const InputStyle = {
    width: "100%"
};

export default TeamInfoComponent
