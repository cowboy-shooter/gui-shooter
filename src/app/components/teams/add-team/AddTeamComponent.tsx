import * as React from "react";
import {Modal} from "semantic-ui-react";
import TeamInfoComponent, {TeamInfoProps} from "./TeamInfoComponent";
import ServiceComponent from "../../service/ServiceComponent";
import {TeamDTO} from "../../../base/dto/teams/TeamDTO";

interface AddTeamComponentState {
    teamFlag: string,
    teamName: string,
    description: string,
    language: string,
    allLanguages: {}[]
}

interface AddTeamButtonProps {
    disabled: boolean,
    onTeamLoad: (team: TeamDTO) => void
}

class AddTeamComponent extends ServiceComponent<AddTeamButtonProps, AddTeamComponentState> {

    constructor(props: AddTeamButtonProps) {
        super(props);
        this.state = {
            teamFlag: '',
            teamName: '',
            description: '',
            language: '',
            allLanguages: []
        };
    }

    componentDidMount(): void {
        this.commonService
            .getProgrammingLanguages()
            .then((result) => {
                this.setState({
                    allLanguages: result.data
                });
            });
    }

    createTeam = (teamInfo: TeamInfoProps) => {
        this.teamsService.createTeam(teamInfo.teamName, teamInfo.url, teamInfo.description, teamInfo.language, teamInfo.teamFlag, {id: true})
            .then((result) => this.props.onTeamLoad(result.data));
    };

    onFlagChange = (flagBase64: string) => {
        this.setState({
            teamFlag: flagBase64
        });
    };

    render() {
        const {disabled} = this.props;
        if (disabled) {
            return <button className="ui inverted green basic button" disabled={true}>Add Team</button>
        }
        return (
            <div style={ButtonStyle}>
                <Modal
                    trigger={
                        <button className="ui inverted green basic button">Add Team</button>
                    }
                    basic style={ModalStyle}>
                    <TeamInfoComponent onSubmit={this.createTeam} teamFlag={''} teamName={''} url={''} description={''} language={''}></TeamInfoComponent>
                </Modal>
            </div>
        )
    }

}

const ModalStyle = {
    textAlign: 'center' as 'center',
    top: "20%",
    width: "20%"
};

const ButtonStyle = {
    height: "100%",
    textAlign: "center" as "center",
    float: "right" as "right"
};

export default AddTeamComponent
