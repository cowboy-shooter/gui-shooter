import * as React from "react";
import {Component} from "react";
import {Card, Image} from "semantic-ui-react";
import {TeamDTO} from "../../../base/dto/teams/TeamDTO";

interface TeamCardProps {
    team: TeamDTO,
    style?: {},
    imageSize: number
}

class TeamCard extends Component<TeamCardProps> {

    render() {
        const {team, style} = this.props;
        return (
            <Card style={style}>
                <Image src={team.flag} height={ this.props.imageSize }/>
                <Card.Content>
                    <Card.Header>{team.name}</Card.Header>
                    <Card.Meta>{team.language}</Card.Meta>
                    <Card.Description>{team.description}</Card.Description>
                </Card.Content>
            </Card>
        )
    }

}

export default TeamCard
