import * as React from "react";
import {Modal, Popup, Table} from "semantic-ui-react";
// @ts-ignore
import TeamDetailComponent from "../team-detail/TeamDetailComponent";
import Gravatar from "react-gravatar";
import {TeamDTO} from "../../../base/dto/teams/TeamDTO";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClipboardCheck, faClipboardList, faCrown, faUsers} from '@fortawesome/free-solid-svg-icons'
import ReloadingImageComponent from "../../table/ReloadingImageComponent";

export interface TeamTableRowProps {
    team: TeamDTO,
    isInvited: boolean,
    isRequestor: boolean,
    isLeader: boolean,
    isMember: boolean
}

export class TeamTableRow extends React.Component<TeamTableRowProps> {

    render() {
        const {team, isInvited, isLeader, isRequestor, isMember} = this.props;

        let popupText = "";
        let stateIcon = null;

        if (isLeader) {
            stateIcon = <FontAwesomeIcon icon={faCrown} style={IconStyle}/>
            popupText = "You are the leader";
        } else if (isMember) {
            stateIcon = <FontAwesomeIcon icon={faUsers} style={IconStyle}/>
            popupText = "You are a member";
        } else if (isInvited) {
            stateIcon = <FontAwesomeIcon icon={faClipboardCheck} style={IconStyle}/>
            popupText = "You have been invited";
        } else if (isRequestor) {
            stateIcon = <FontAwesomeIcon icon={faClipboardList} style={IconStyle}/>
            popupText = "You requested membership";
        }

        return (
            <Modal trigger={
                <Table.Row style={RowStyle}>
                    <Table.Cell>{team.name}</Table.Cell>
                    <Table.Cell>
                        <ReloadingImageComponent image={team.flag} loaderSize={'medium'} imageHeight={50} imageWidth={100}/>
                    </Table.Cell>
                    <Table.Cell>
                        {
                            team.members.map((member) => {
                                return <Gravatar email={member.email} size={50} style={GravatarStyle}/>
                            })
                        }
                    </Table.Cell>
                    <Table.Cell>
                        <Popup
                            content={popupText}
                            position='bottom center'
                            trigger={stateIcon}
                        />
                    </Table.Cell>
                </Table.Row>
            } basic style={ModalStyle}>
                <TeamDetailComponent teamId={team.id}/>
            </Modal>
        );
    }
}

const GravatarStyle = {
    borderRadius: "50%",
    height: "auto"
};


const IconStyle = {
    width: "50px",
    height: "50px"
};

const RowStyle = {
    cursor: "pointer"
};

const ModalStyle = {
    textAlign: 'center' as 'center',
    top: "50px"
};

export default TeamTableRow
