import React, {Component} from 'react'
import {RoutingPaths} from "../../base/RoutingPaths";
import NavigationComponent from "../navigation/NavigationComponent";
import UserBarComponent from "../user/user-bar/UserBarComponent";
import {faMap} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Grid, Header, Icon, Segment} from "semantic-ui-react";
import MenuItem, {MenuItemSetting} from "./menu/MenuItem";
import WhatIsThisItemComponent from "./menu/items/WhatIsThisItemComponent";
import UserProfileDescriptionItemComponent from "./menu/items/UserProfileDescriptionItemComponent";
import HowDoesItWorkDescriptionItemComponent from "./menu/items/HowDoesItWorkDescriptionItemComponent";
import TeamsDescriptionItemComponent from "./menu/items/TeamsDescriptionItemComponent";
import WhatDoIWinDescriptionItemComponent from "./menu/items/WhatDoIWinDescriptionItemComponent";
import GameRulesDescriptionItemComponent from "./menu/items/GameRulesDescriptionItemComponent";
import TournamentsDescriptionItemComponent from "./menu/items/TournamentsDescriptionItemComponent";
import HowDoIStartDescriptionItemComponent from "./menu/items/HowDoIStartDescriptionItemComponent";

interface AboutComponentState {
    activeSetting: MenuItemSetting,
    previousSetting?: MenuItemSetting,
    nextSetting?: MenuItemSetting,
    leftArrowHover: boolean,
    rightArrowHover: boolean
}

const ItemSettings: MenuItemSetting[] = [
    {
        title: 'What is this?',
        content: (<WhatIsThisItemComponent/>)
    }, {
        title: 'User profile',
        content: (<UserProfileDescriptionItemComponent/>)
    }, {
        title: 'Teams',
        content: (<TeamsDescriptionItemComponent/>)
    }, {
        title: 'Tournaments',
        content: (<TournamentsDescriptionItemComponent/>)
    }, {
        title: 'Game Rules',
        content: (<GameRulesDescriptionItemComponent/>)
    }, {
        title: 'How does it work?',
        content: (<HowDoesItWorkDescriptionItemComponent/>)
    }, {
        title: 'How do I start?',
        content: (<HowDoIStartDescriptionItemComponent/>)
    }, {
        title: 'What do I win?',
        content: (<WhatDoIWinDescriptionItemComponent/>)
    }
];

export class AboutComponent extends Component<{}, AboutComponentState> {

    constructor(props: any) {
        super(props);
        this.state = {
            activeSetting: ItemSettings[0],
            nextSetting: ItemSettings[1],
            leftArrowHover: false,
            rightArrowHover: false
        };
    }

    onItemClick = (item: MenuItemSetting): void => {
        let index = ItemSettings.indexOf(item);
        let previousSetting: MenuItemSetting | undefined = undefined;
        let nextSetting: MenuItemSetting | undefined = undefined;

        if (index - 1 >= 0) {
            previousSetting = ItemSettings[index - 1];
        }
        if (index + 1 < ItemSettings.length) {
            nextSetting = ItemSettings[index + 1];
        }

        this.setState({
            activeSetting: item,
            previousSetting: previousSetting,
            nextSetting: nextSetting,
            rightArrowHover: false,
            leftArrowHover: false
        });
    };

    render() {
        const {activeSetting, previousSetting, nextSetting, rightArrowHover, leftArrowHover} = this.state;

        let content: JSX.Element = (<div></div>);

        ItemSettings.forEach((setting) => {
            if (setting === activeSetting) {
                content = setting.content;
            }
        });

        return (
            <div>
                <UserBarComponent/>
                <NavigationComponent path={RoutingPaths.ABOUT}/>
                <FontAwesomeIcon icon={faMap} style={HeaderIconStyle}/>
                <Grid style={GridStyle}>
                    <Grid.Column width={4}>
                        {
                            ItemSettings.map((setting) => {
                                return (
                                    <MenuItem
                                        itemSetting={setting}
                                        active={activeSetting === setting}
                                        onClick={this.onItemClick}
                                    />
                                )
                            })
                        }
                    </Grid.Column>

                    <Grid.Column stretched width={9}>
                        <Segment style={ContentStyle} textAlign={"center"}>
                            {
                                content
                            }
                            {
                                previousSetting? (
                                    <Header as='h3' icon style={leftArrowHover? LeftArrowActive: LeftArrow}
                                            onMouseOver={() => this.setState({leftArrowHover: true})}
                                            onMouseLeave={() => this.setState({leftArrowHover: false})}
                                            onClick={() => this.onItemClick(previousSetting)}>
                                        <Icon size={"huge"} name={"arrow left"} />
                                        {previousSetting.title}
                                    </Header>
                                    ): null
                            }
                            {
                                nextSetting? (
                                    <Header as='h3' icon style={rightArrowHover? RightArrowActive: RightArrow}
                                            onMouseOver={() => this.setState({rightArrowHover: true})}
                                            onMouseLeave={() => this.setState({rightArrowHover: false})}
                                            onClick={() => this.onItemClick(nextSetting)}>
                                        <Icon size={"huge"} name={"arrow right"} />
                                        {nextSetting.title}
                                    </Header>
                                ): null
                            }
                        </Segment>
                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}

const HeaderIconStyle = {
    color: "rgb(171, 201, 255)",
    margin: "auto",
    width: "100%",
    height: "10vh",
    minHeight: "50px"
};

const GridStyle: React.CSSProperties = {
    width: "70%",
    margin: "auto",
    marginTop: "50px",
    minWidth: "100vh"
};

const ContentStyle: React.CSSProperties = {
    background: "rgb(51, 53, 66)"
};

const Arrow: React.CSSProperties = {
    color: "white",
    cursor: "pointer",
    transition: "all 0.3s ease"
};

const LeftArrow: React.CSSProperties = {
    ...Arrow,
    color: "white",
    float: "left"
};

const LeftArrowActive: React.CSSProperties = {
    ...Arrow,
    color: "#9bb7ff",
    float: "left"
};

const RightArrow: React.CSSProperties = {
    ...Arrow,
    color: "white",
    float: "right"
};

const RightArrowActive: React.CSSProperties = {
    ...Arrow,
    color: "#9bb7ff",
    float: "right"
};


export default AboutComponent