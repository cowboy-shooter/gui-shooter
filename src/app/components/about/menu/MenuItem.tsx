import React, {Component} from 'react'
import {Header, Segment} from "semantic-ui-react";

interface MenuItemProps {
    itemSetting: MenuItemSetting,
    active: boolean,
    onClick: (itemSetting: MenuItemSetting) => void
}

export interface MenuItemSetting {
    title: string,
    content: JSX.Element
}

interface MenuItemState {
    hover: boolean
}

export class MenuItem extends Component<MenuItemProps, MenuItemState> {

    constructor(props: MenuItemProps) {
        super(props);
        this.state = {
            hover: false
        }
    }

    onMouseEnter = () => {
        this.setState({hover: true});
    };

    onMouseLeave = () => {
        this.setState({hover: false});
    };

    handleItemClick = () => {
        this.props.onClick(this.props.itemSetting)
    };

    render() {
        const {itemSetting} = this.props;

        let style: React.CSSProperties = DefaultStyle;
        if (this.props.active) {
            style = ActiveStyle;
        } else if (this.state.hover) {
            style = HoverStyle;
        }

        return (
            <Segment style={style} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave} onClick={this.handleItemClick}>
                <Header size={"small"} inverted textAlign={"center"} >{itemSetting.title}</Header>
            </Segment>
        )
    }
}

const CommonStyle: React.CSSProperties = {
    transition: "all 0.5s ease"
};

const HoverStyle: React.CSSProperties = {
    ...CommonStyle,
    backgroundColor: "rgb(77, 82, 115)",
    cursor: "pointer"
};

const DefaultStyle: React.CSSProperties = {
    ...CommonStyle,
    backgroundColor: "#333542"
};

const ActiveStyle: React.CSSProperties = {
    ...CommonStyle,
    backgroundColor: "rgba(82, 117, 210, 0.81)"
};

export default MenuItem