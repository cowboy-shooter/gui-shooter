import React from 'react'
import {Header} from 'semantic-ui-react'
import WhatIsThisItemDescription from "./WhatIsThisItemDescription";
import DescriptionComponent from "./DescriptionComponent";

export class WhatIsThisItemComponent extends DescriptionComponent {

    render() {
        return (
           <div  style={this.WhiteColor}>
               <Header inverted size={"huge"}>What is this?</Header>
               <p>This page serves not only to brag about how awesome I am for writing this, but also as documentation.</p>
               <p>This game is called Cowboy shooter. This was the basic description that I wrote before I started programming this. It served as the task description.</p>
               <WhatIsThisItemDescription/>
           </div>
        )
    }
}

export default WhatIsThisItemComponent