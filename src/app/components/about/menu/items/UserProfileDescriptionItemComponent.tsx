import React from 'react'
import {Header, Image} from 'semantic-ui-react'
import Gravatar from "react-gravatar";
import DescriptionComponent from "./DescriptionComponent";
import userProfile from "../../../../assets/userProfile.png";

export class UserProfileDescriptionItemComponent extends DescriptionComponent {

    render() {
        return (
           <div style={this.WhiteColor}>
               <Header inverted size={"huge"}>User profile</Header>
               <p>In the right top corner you will find your user profile picture. It looks something like this:</p>
               <Gravatar email={"marcel.ouska@gmail.com"} size={50} style={GravatarStyle}/>
               <p>In case you still haven´t set your picture up you´ll see something like this instead:</p>
               <Gravatar email={"a@a"} size={50} style={GravatarStyle}/>
               <p><a href={"https://en.gravatar.com/"}>Here you can set up your own gravatar</a> - it will be bound to your email address. </p>
               <p>Clicking on the picture will give you options to either change your user profile or log out.</p>
               <Image style={ImageStyle}
                      src={userProfile}
                      as='a'
                      size='medium'
                      target='_blank'
               />
           </div>
        )
    }
}

const GravatarStyle = {
    borderRadius: "50%",
    height: "auto"
};

const ImageStyle = {
    height: "auto",
    width: "300px",
    transition: "all 0.5s ease"
};


export default UserProfileDescriptionItemComponent