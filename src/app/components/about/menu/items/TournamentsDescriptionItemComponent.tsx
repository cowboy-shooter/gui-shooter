import React from 'react'
import {Header, Image, Table} from 'semantic-ui-react'
import DescriptionComponent from "./DescriptionComponent";
import tiersModel from "../../../../assets/tiersModel.png";

export class TournamentsDescriptionItemComponent extends DescriptionComponent {

    render() {
        return (
           <div style={this.WhiteColor}>
               <Header inverted size={"huge"}>Tournaments</Header>
               <p>You can find tournaments in the matches menu.</p>
               <p>Each tournament should have 5 and more teams to compete.</p>
               <p>Tournaments are made out of 4 tiers.</p>
               <p>This is an example of a Tier 1. Each team has to compete each team 3 times.</p>
               <p>Win - 3 points</p>
               <p>Draw - 1 point</p>
               <p>Lose - 0 points</p>

               <Header inverted size={"medium"}>Tier 1</Header>
               <Table celled inverted>
                   <Table.Header>
                       <Table.Row>
                           <Table.HeaderCell />
                           <Table.HeaderCell colspan={3}>Team A</Table.HeaderCell>
                           <Table.HeaderCell colspan={3}>Team B</Table.HeaderCell>
                           <Table.HeaderCell colspan={3}>Team C</Table.HeaderCell>
                       </Table.Row>
                   </Table.Header>

                   <Table.Body>
                       <Table.Row>
                           <Table.Cell style={LeftTableCellStyle}>
                               <b>Team A</b>
                           </Table.Cell>
                           <Table.Cell style={EmptyCellStyle}></Table.Cell>
                           <Table.Cell style={EmptyCellStyle}></Table.Cell>
                           <Table.Cell style={EmptyCellStyle}></Table.Cell>

                           <Table.Cell>3</Table.Cell>
                           <Table.Cell>1</Table.Cell>
                           <Table.Cell>3</Table.Cell>

                           <Table.Cell>0</Table.Cell>
                           <Table.Cell>0</Table.Cell>
                           <Table.Cell>0</Table.Cell>
                       </Table.Row>
                       <Table.Row>
                           <Table.Cell style={LeftTableCellStyle}>
                               <b>Team B</b>
                           </Table.Cell>
                           <Table.Cell>0</Table.Cell>
                           <Table.Cell>1</Table.Cell>
                           <Table.Cell>0</Table.Cell>

                           <Table.Cell style={EmptyCellStyle}></Table.Cell>
                           <Table.Cell style={EmptyCellStyle}></Table.Cell>
                           <Table.Cell style={EmptyCellStyle}></Table.Cell>

                           <Table.Cell>1</Table.Cell>
                           <Table.Cell>1</Table.Cell>
                           <Table.Cell>3</Table.Cell>
                       </Table.Row>
                       <Table.Row>
                           <Table.Cell style={LeftTableCellStyle}>
                               <b>Team C</b>
                           </Table.Cell>
                           <Table.Cell>3</Table.Cell>
                           <Table.Cell>3</Table.Cell>
                           <Table.Cell>3</Table.Cell>

                           <Table.Cell>1</Table.Cell>
                           <Table.Cell>1</Table.Cell>
                           <Table.Cell>0</Table.Cell>

                           <Table.Cell style={EmptyCellStyle}></Table.Cell>
                           <Table.Cell style={EmptyCellStyle}></Table.Cell>
                           <Table.Cell style={EmptyCellStyle}></Table.Cell>
                       </Table.Row>
                   </Table.Body>
               </Table>

               <Header inverted size={"medium"}>Tier 1 Score</Header>
               <Table celled inverted>
                   <Table.Header>
                       <Table.Row>
                           <Table.HeaderCell>Team A</Table.HeaderCell>
                           <Table.HeaderCell>Team B</Table.HeaderCell>
                           <Table.HeaderCell>Team C</Table.HeaderCell>
                       </Table.Row>
                   </Table.Header>

                   <Table.Body>
                       <Table.Row>
                           <Table.Cell>7</Table.Cell>
                           <Table.Cell>6</Table.Cell>
                           <Table.Cell>11</Table.Cell>
                       </Table.Row>
                   </Table.Body>
               </Table>

               <p>Tier 2 works the same as Tier 1, but there are tournament matches of 5 and not 3 as in the Tier 1.</p>
               <p>After executing the first 2 tiers, 4 teams with the best score will be chosen to attend tiers 3 and 4.</p>
               <p>Tiers 3 and 4 are classical tournament spider scheme (No idea what it´s called for real).</p>

               <Image style={ImageStyle}
                      src={tiersModel}
                      as='a'
                      size='medium'
                      target='_blank'
               />
           </div>
        )
    }
}

const ImageStyle = {
    height: "auto",
    width: "500px",
    transition: "all 0.5s ease"
};

const LeftTableCellStyle: React.CSSProperties = {
    background: "#2b2b2b"
};

const EmptyCellStyle: React.CSSProperties = {
    background: "#7f8096"
};


export default TournamentsDescriptionItemComponent