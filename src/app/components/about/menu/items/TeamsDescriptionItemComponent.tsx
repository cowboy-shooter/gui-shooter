import React from 'react'
import {Divider, Header, Image} from 'semantic-ui-react'
import DescriptionComponent from "./DescriptionComponent";
import {faClipboardCheck, faClipboardList, faCrown, faUsers} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import createTeam from "../../../../assets/createTeam.png";
import teamDetail from "../../../../assets/teamDetail.png";
import teamManagement from "../../../../assets/teamManagement.png";
import teamStatistics from "../../../../assets/teamStatistics.png";

export class TeamsDescriptionItemComponent extends DescriptionComponent {

    render() {
        return (
           <div  style={this.WhiteColor}>
               <Header inverted size={"huge"}>Teams</Header>
               <p>You can create and view teams here. In case you already have a team, the  <b style={this.YellowColor}>Add Team</b> button will be disabled.</p>

               <br/>
               <br/>
               <Divider horizontal inverted>Create a Team</Divider>

               <Image style={ImageStyle}
                      src={createTeam}
                      as='a'
                      size='medium'
                      target='_blank'
               />
               <p><b style={this.YellowColor}>You can change any of the parameters later on.</b></p>
               <p><b style={this.YellowColor}>If any of the flags is <b style={this.RedColor}>red</b>, you won´t be able to create the team.</b></p>
               <p><b style={this.OrangeColor}>Flag</b> - Flag of the team. It is not really a Flag but more like your team profile picture.</p>
               <p><b style={this.OrangeColor}>Name</b> - Name of the team.</p>
               <p><b style={this.OrangeColor}>URL Context</b> - Where your PLAYER will be listening. This is where Data shooter will be sending its posts.</p>
               <p><b style={this.OrangeColor}>Programming Language</b> - Just informational. Which language is your PLAYER programmed in.</p>
               <p><b style={this.OrangeColor}>Description</b> - Short description of your team.</p>

               <br/>
               <br/>
               <Divider horizontal inverted>Team State</Divider>

               <p>In the table you can see all the teams. On the right side of the table is your state in the team.</p>
               <FontAwesomeIcon icon={faCrown} style={IconStyle}/><p>You are the leader of the team.</p>
               <FontAwesomeIcon icon={faUsers} style={IconStyle}/><p>You are a member of the team.</p>
               <FontAwesomeIcon icon={faClipboardCheck} style={IconStyle}/><p>You have been invited into this team.</p>
               <FontAwesomeIcon icon={faClipboardList} style={IconStyle}/><p>You requested membership in this team.</p>

               <br/>
               <br/>
               <Divider horizontal inverted>Team Detail</Divider>

               <p>Clicking on one of the team rows will show you its details.</p>
               <Image style={ImageStyle}
                      src={teamDetail}
                      as='a'
                      size='medium'
                      target='_blank'
               />

               <br/>
               <br/>
               <Divider horizontal inverted>Team Detail Tabs</Divider>

               <p>There are several different tabs - their visibility depends on your state in the team.</p>
               <p>Members tab will allow you to see all the members in the team. Only 3 members are allowed at the time. You can also find buttons for Leave Team/Accept Joining/Cancel Request here.</p>
               <p>Management tab will show you all the people that can be invited into your team. You can send invites from here and accept requests to be accepted into your team as a member.</p>
               <Image style={ImageStyle}
                      src={teamManagement}
                      as='a'
                      size='medium'
                      target='_blank'
               />
               <p>You can also edit your team in the tab Team.</p>
               <p>Statistics tab will show you how your team is doing so far in all the matches. In case you haven´t played any matches yet you won´t see anything on the page.</p>
               <Image style={ImageStyle}
                      src={teamStatistics}
                      as='a'
                      size='medium'
                      target='_blank'
               />
           </div>
        )
    }
}

const IconStyle: React.CSSProperties = {
    width: "50px",
    height: "50px",
    color: "white"
};
const ImageStyle = {
    height: "auto",
    width: "500px",
    transition: "all 0.5s ease"
};


export default TeamsDescriptionItemComponent