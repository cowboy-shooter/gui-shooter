import React from 'react'
import {Divider, Grid, Header} from 'semantic-ui-react'
import DescriptionComponent from "./DescriptionComponent";

export class GameRulesDescriptionItemComponent extends DescriptionComponent {

    render() {
        return (
           <div style={this.WhiteColor}>
               <Header inverted size={"huge"}>Game rules</Header>

               <p>We will be playing a game called <span style={this.GreenColor}>Shotgun game</span> or also know as <span style={this.GreenColor}>Slap-Slap-Draw</span></p>
               <p>This game has 3 possible moves: <span style={this.YellowColor}>Block</span>, <span style={this.RedColor}>Shoot</span> and <span style={this.BlueColor}>Reload</span></p>
               <p>Two players stand against each other and play actions in a rhythm where in between the actions is slap twice on the table</p>
               <p><b style={this.YellowColor}>Block</b> - protects you from getting shot</p>
               <p><b style={this.BlueColor}>Reload</b> - adds one bullet to be shot during Shoot action</p>
               <p><b style={this.RedColor}>Shoot</b> - shoots one of your bullets if you have any</p>
               <p>If you manage to shoot the player while he is reloading, you won</p>
               <p>Game is shown on this video</p>

               <iframe title={"rulesVideo"} width="420" height="315"
                       src="https://www.youtube.com/embed/PWhcuA7u7Xs">
               </iframe>

               <br/>
               <br/>
               <Divider horizontal inverted>Fixed rules for Cowboy Shooter and limitations</Divider>
               <div style={RulesParagraphsStyle}>
                   <p>Your actions:</p>
                   <p><b style={this.YellowColor}>Block</b> - protects you from getting shot, <span style={this.GreenColor}>maximum 4 blocks in a row.</span></p>
                   <p><b style={this.BlueColor}>Reload</b> - adds one bullet to be shot during Shoot action, <span style={this.GreenColor}>maximum 6 loaded bullets.</span></p>
                   <p><b style={this.RedColor}>Shoot</b> - shoots one of your bullets if you have any.</p>
                   <p><b style={this.OrangeColor}>Evade</b> - protects you from getting shot, <span style={this.GreenColor}>can be used only once per match.</span></p>
                   <p><b style={this.RedColor}>Max rounds per match is 50.</b></p>
               </div>

               <br/>
               <br/>
               <Divider horizontal inverted>Very probable questions</Divider>
               <div style={RulesParagraphsStyle}>
                   <p>What if we shoot at the same time? <div style={this.GreenColor}>You will avoid each other´s bullets, but the bullets will still be taken from you.</div></p>
                   <p>What if my server replies with HTTP error? <div style={this.GreenColor}>It will be taken as action 'NULL' but the match still continues.</div></p>
                   <p>What if I use an action that I cannot use at that time, like Shoot without bullets or block 5th time in a row? <div style={this.GreenColor}>It will be counted as you did nothing but the match continues.</div></p>
                   <p>How long did it take for you to program all this? <div style={this.GreenColor}>.... Long</div></p>
               </div>


               <br/>
               <br/>
               <Divider horizontal inverted>Match Example</Divider>
               <Grid columns={3} style={GridStyle}>
                   <Grid.Row>
                       <Grid.Column><Header inverted size={"medium"}>Team 1</Header></Grid.Column>
                       <Grid.Column><Header inverted size={"medium"}>Team 2</Header></Grid.Column>
                       <Grid.Column><Header inverted size={"medium"}>Notes</Header></Grid.Column>
                   </Grid.Row>
                   <Grid.Row>
                       <Grid.Column>1.RELOAD</Grid.Column>
                       <Grid.Column>1.RELOAD</Grid.Column>
                       <Grid.Column style={this.YellowColor}>Both teams reloaded and have 1 bullet each</Grid.Column>
                   </Grid.Row>
                   <Grid.Row>
                       <Grid.Column>2.BLOCK</Grid.Column>
                       <Grid.Column>2.RELOAD</Grid.Column>
                       <Grid.Column style={this.YellowColor}>Team 1 blocks, Team 2 has now 2 bullets</Grid.Column>
                   </Grid.Row>
                   <Grid.Row>
                       <Grid.Column>3.BLOCK</Grid.Column>
                       <Grid.Column>3.SHOOT</Grid.Column>
                       <Grid.Column style={this.YellowColor}>Team 2 tries to shoot but Team 1 blocks it</Grid.Column>
                   </Grid.Row>
                   <Grid.Row>
                       <Grid.Column>4.EVADE</Grid.Column>
                       <Grid.Column>4.SHOOT</Grid.Column>
                       <Grid.Column style={this.YellowColor}>Team 2 tries to shoot but Team 1 evades it</Grid.Column>
                   </Grid.Row>
                   <Grid.Row>
                       <Grid.Column>5.SHOOT</Grid.Column>
                       <Grid.Column>5.RELOAD</Grid.Column>
                       <Grid.Column style={this.YellowColor}>Team 1 shoots and kills Team 2</Grid.Column>
                   </Grid.Row>
               </Grid>

           </div>
        )
    }
}

const RulesParagraphsStyle: React.CSSProperties = {
    fontSize: "larger",
    width: "50%",
    margin: "auto",
    textAlign: "left"
};

const GridStyle: React.CSSProperties = {
    width: "50%",
    margin: "auto"
};



export default GameRulesDescriptionItemComponent