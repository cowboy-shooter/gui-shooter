import React, {Component} from 'react'

export abstract class DescriptionComponent extends Component {
    OrangeColor: React.CSSProperties = {
        color: "#ff851b"
    };

    RedColor: React.CSSProperties = {
        color: "#ff2036"
    };

    GreenColor: React.CSSProperties = {
        color: "#2ecc40"
    };

    BlueColor: React.CSSProperties = {
        color: "#54c8ff"
    };

    YellowColor: React.CSSProperties = {
        color: "#cccb60"
    };

    WhiteColor: React.CSSProperties = {
        color: "white"
    };
}

export default DescriptionComponent