import React from 'react'
import {Divider, Header, Image} from 'semantic-ui-react'
import DescriptionComponent from "./DescriptionComponent";
import urlContextIpAddress from "../../../../assets/urlContextIpAddress.png";

export class HowDoIStartDescriptionItemComponent extends DescriptionComponent {

    render() {
        return (
           <div style={this.WhiteColor}>
               <Header inverted size={"huge"}>How do I start?</Header>
               <p>Pull one of the projects below</p>
               <p><a href={'https://gitlab.com/cowboy-shooter/players/java-player'}>Java</a></p>
               <p><a href={'https://gitlab.com/cowboy-shooter/players/javascript-player'}>JavaScript</a></p>
               <p>Each project contains a very simple player that returns random values. How to use it is explained in the README.</p>
               <p>For the Java project, the method that you need to implement is in class <b style={this.YellowColor}>com.cowboy.shooter.dummyplayer.service.GameService</b></p>
               <p>For the JavaScript project, the method that you need to implement is in file <b style={this.YellowColor}>player.js</b></p>

               <br/>
               <br/>
               <Divider horizontal inverted>How do I test this?</Divider>
               <p>You player is actually a server (for those who did not notice). That means that you will have to aim Cowboy Shooter to your server on localhost.</p>
               <p>There are several ways of achieving this, but the easiest one is to just enter your IP address in your team profile, while connected to the VPN (or being on the Physter network).</p>
               <p>According to your operating system, acquire the IP address and put it into the Context field of your team. The result should look something like this.</p>
               <Image style={ImageStyle}
                      src={urlContextIpAddress}
                      as='a'
                      size='medium'
                      target='_blank'
               />
               
               <p>You can then start a match against yourself. It´s possible to challenge yourself for testing purposes.</p>

           </div>
        )

    }
}

const ImageStyle = {
    height: "auto",
    width: "700px",
    transition: "all 0.5s ease"
};


export default HowDoIStartDescriptionItemComponent