import React from 'react'
import {Header, Image} from 'semantic-ui-react'
import DescriptionComponent from "./DescriptionComponent";
import diplomatico from "../../../../assets/diplomatico.png";

export class WhatDoIWinDescriptionItemComponent extends DescriptionComponent {

    render() {
        return (
           <div  style={this.WhiteColor}>
               <Header inverted size={"huge"}>What do I win?</Header>
               <p>Probably this!</p>
               <Image style={ImageStyle}
                      src={diplomatico}
                      as='a'
                      size='medium'
                      target='_blank'
               />
               <Header inverted size={"huge"}>What can you lose?</Header>
               <p>Your job, if it shows that you can´t program and make even this work..</p>
           </div>
        )
    }
}

const ImageStyle = {
    height: "auto",
    width: "300px",
    transition: "all 0.5s ease"
};

export default WhatDoIWinDescriptionItemComponent