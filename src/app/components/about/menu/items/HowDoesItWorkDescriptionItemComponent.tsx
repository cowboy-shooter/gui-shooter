import React from 'react'
import {Divider, Header, Image} from 'semantic-ui-react'
import DescriptionComponent from "./DescriptionComponent";
// @ts-ignore
import SyntaxHighlighter from 'react-syntax-highlighter';
// @ts-ignore
import {docco} from 'react-syntax-highlighter/dist/esm/styles/hljs';
import architecture from "../../../../assets/architecture.png";
import serverClientCommunication from "../../../../assets/serverClientCommunication.png";

export class HowDoesItWorkDescriptionItemComponent extends DescriptionComponent {

    render() {
        return (
           <div  style={this.WhiteColor}>
               <Header inverted size={"huge"}>How does it work?</Header>
               <p>Here is a simplified architecture</p>
               <Image style={ImageStyle}
                      src={architecture}
                      as='a'
                      size='medium'
                      target='_blank'
               />

               <p>Basically, Cowboy shooter asks you for your next move. <b style={this.RedColor}>Cowboy shooter is a client and your player is a server!</b></p>
               <Image style={ImageStyle}
                      src={serverClientCommunication}
                      as='a'
                      size='medium'
                      target='_blank'
               />

               <br/>
               <br/>
               <Divider horizontal inverted>What do i need to prepare</Divider>
               <p>You should choose a programming language that you are most comfortable with.</p>
               <p>After choosing it, you need to prepare a project that will be able to respond to HTTP POST with a json that will contain <b style={this.BlueColor}>move</b> attribute, in which you will be sending your move.</p>
               <p>Data Shooter will be sending you HTTP POST messages with the current state of the match.</p>
               <p>Example in Java</p>
               <div style={SyntaxHighlighterStyle}>
                   <SyntaxHighlighter language="java" style={docco}>
                       {'package com.cowboy.shooter.dummyplayer.rest.dto;\n' +
                       '\n' +
                       'import lombok.Data;\n' +
                       '\n' +
                       'import java.io.Serializable;\n' +
                       '\n' +
                       '@Data\n' +
                       'public class MatchMove implements Serializable {\n' +
                       '\n' +
                       '    private Integer sequenceNumber;\n' +
                       '    private TeamMove opponentsLastMove;\n' +
                       '    private String opponentId;\n' +
                       '    private String opponentName;\n' +
                       '\n' +
                       '}\n'}
                   </SyntaxHighlighter>
               </div>

               <div style={SyntaxHighlighterStyle}>
                   <SyntaxHighlighter language="java" style={docco}>
                       {'package com.cowboy.shooter.dummyplayer.rest.dto;\n' +
                       '\n' +
                       'import lombok.Data;\n' +
                       '\n' +
                       '@Data\n' +
                       'public class TeamMoveRestResponse {\n' +
                       '\n' +
                       '    private TeamMove move;\n' +
                       '\n' +
                       '    public TeamMoveRestResponse(TeamMove move) {\n' +
                       '        this.move = move;\n' +
                       '    }\n' +
                       '}\n'}
                   </SyntaxHighlighter>
               </div>

               <div style={SyntaxHighlighterStyle}>
                   <SyntaxHighlighter language="java" style={docco}>
                       {'package com.cowboy.shooter.dummyplayer.rest;\n' +
                       '\n' +
                       'import com.cowboy.shooter.dummyplayer.rest.dto.MatchMove;\n' +
                       'import com.cowboy.shooter.dummyplayer.rest.dto.TeamMove;\n' +
                       'import com.cowboy.shooter.dummyplayer.rest.dto.TeamMoveRestResponse;\n' +
                       'import org.springframework.web.bind.annotation.PostMapping;\n' +
                       'import org.springframework.web.bind.annotation.RequestBody;\n' +
                       'import org.springframework.web.bind.annotation.RestController;\n' +
                       '\n' +
                       'import java.util.Random;\n' +
                       '\n' +
                       '@RestController\n' +
                       'public class GameRestController {\n' +
                       '\n' +
                       '    @PostMapping(path= "/move", consumes = "application/json", produces = "application/json")\n' +
                       '    public TeamMoveRestResponse makeMove(@RequestBody MatchMove lastMove) {\n' +
                       '        // Your logic can just go here\n' +
                       '        TeamMove[] possibleValues = TeamMove.values();\n' +
                       '        return new TeamMoveRestResponse(possibleValues[new Random().nextInt(possibleValues.length)]);\n' +
                       '    }\n' +
                       '\n' +
                       '}\n'}
                   </SyntaxHighlighter>
               </div>

               <p style={this.YellowColor}>These examples were written without compiling or trying so don´t expect it to be perfect</p>

           </div>
        )
    }
}

const ImageStyle = {
    height: "auto",
    width: "700px",
    transition: "all 0.5s ease"
};

const SyntaxHighlighterStyle: React.CSSProperties = {
    width: "80%",
    margin: "auto",
    textAlign: "left",
    borderRadius: "20px"
};


export default HowDoesItWorkDescriptionItemComponent