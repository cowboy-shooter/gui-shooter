import React from 'react'
import {Image} from 'semantic-ui-react'
import fallout from "../../../../assets/fallout.png";
import DescriptionComponent from "./DescriptionComponent";

export class WhatIsThisItemDescription extends DescriptionComponent {

    render() {
        return (
           <div style={this.WhiteColor}>
               <p>
                   Cowboy shooter is a turn-based strategy coding game.
               </p>
               <p>
                   Users can compete in the tab <b style={this.OrangeColor}>Matches</b>.
                   Teams can <b style={this.YellowColor}>challenge each other</b> in private match sessions; both teams need to agree with the ongoing match.
                   Power users (like me) can also set a <b style={this.YellowColor}>tournament</b> up where all the teams will be <b style={this.YellowColor}>fighting against each other</b>.
                   Each challenged match always needs a <b style={this.YellowColor}>consent of both teams</b> to be executed. This, however, does not apply in the tournament.
                   <b style={this.YellowColor}> Each match will be recorded and can be replayed. </b>
                   Teams can then see older match results and see what their mistakes were.
               </p>
               <p>
                   Users can create teams with other users in the tab <b style={this.GreenColor}>Teams</b>.
                   Users are able to see other teams as well.
                   Each team can choose their <b style={this.YellowColor}>Flag and Name</b>.
                   Each user can be in <b style={this.YellowColor}>one team only</b>.
                   Each team can have <b style={this.YellowColor}>1-3 people</b>.
               </p>
               <p>
                   All the necessary rules and the manual how to play the game can be found in the tab <b style={this.BlueColor}>About</b>
               </p>
               <p>
                   Ready to play?
               </p>
               <Image style={ImageStyle}
                      src={fallout}
                      as='a'
                      size='medium'
                      target='_blank'
               />
           </div>
        )
    }
}

const ImageStyle = {
    height: "auto",
    width: "300px",
    transition: "all 0.5s ease"
};



export default WhatIsThisItemDescription