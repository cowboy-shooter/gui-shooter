import React, {Component} from 'react'
import {Container, Header, Image, Segment} from 'semantic-ui-react'
import fallout from "../../../assets/fallout.png"

export class BasicDescriptionComponent extends Component {

    render() {
        return (
            <Container text>
                <Segment inverted>
                    <Header as='h2'>Cowboy Shooter</Header>
                    <p>
                        Cowboy shooter is turn-based strategy coding game of my own evil making .... <b>Muhehehe</b>
                    </p>
                    <p>
                        Users can compete in the tab <b style={OrangeColor}>Matches</b>.
                        Teams can <b style={YellowColor}>challenge each other</b> in private match sessions, both teams need to agree with the ongoing match.
                        In these sessions teams can setup <b style={YellowColor}>how many times</b> do they need to defeat the other team <b style={YellowColor}>for the total win</b>.
                        Power users (like me) can also setup a <b style={YellowColor}>tournament</b> where all the teams will be <b style={YellowColor}>fighting against each other</b>.
                        Each challenged match always needs a <b style={YellowColor}>consent of both teams</b> (or more in tournament) to be executed.
                        <b style={YellowColor}> Each match will be recorded and can be replayed</b>
                        Teams then can see older matches results and see what their mistakes were.
                    </p>
                    <p>
                        Users can create teams with other users in the tab <b style={GreenColor}>Teams</b>.
                        Users are able to see other teams as well.
                        Each team can choose their <b style={YellowColor}>Flag, Name and Avatar</b>.
                        Avatar will be used in the game as a character.
                        Each user can be in <b style={YellowColor}>one team only</b>.
                        Each team can have <b style={YellowColor}>1-3 people</b>.
                    </p>
                    <p>
                        All the necessary rules and the manual how to play the game can be found in the tab <b style={BlueColor}>About</b>
                    </p>
                    <p>
                        Ready to play?
                    </p>
                    <Image style={ImageStyle}
                           src={fallout}
                           as='a'
                           size='medium'
                           target='_blank'
                    />
                </Segment>
            </Container>
        )
    }
}

const ImageStyle = {
    height: "auto",
    width: "300px",
    transition: "all 0.5s ease"
};

const OrangeColor = {
    color: "#ff851b"
};

const GreenColor = {
    color: "#2ecc40"
};

const BlueColor = {
    color: "#54c8ff"
};

const YellowColor = {
    color: "#cccb60"
};

export default BasicDescriptionComponent