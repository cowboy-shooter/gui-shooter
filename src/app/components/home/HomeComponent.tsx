import React, {Component} from 'react'
import {Container, Grid, Header, Segment} from 'semantic-ui-react'
import {ButtonColors} from "../../enums/ButtonColors";
import {RoutingPaths} from "../../base/RoutingPaths";
import {faFire, faMap, faUsers} from '@fortawesome/free-solid-svg-icons'
import {IconButtonTypes} from "../icon-button/IconButtonPrefabs";
import UserBarComponent from "../user/user-bar/UserBarComponent";
import NavigationButtonComponent from "../navigation/navigation-button/NavigationButtonComponent";
import WhatIsThisItemDescription from "../about/menu/items/WhatIsThisItemDescription";


export class HomeComponent extends Component {

    render() {
        return (
            <div>
                <UserBarComponent/>
                <Grid columns={3} style={GridStyle}>
                    <Grid.Row>
                        <Grid.Column style={ColumnStyle}>
                            <NavigationButtonComponent color={ButtonColors.ORANGE} icon={faFire} text={"Matches"} redirectUri={RoutingPaths.MATCHES} type={IconButtonTypes.MASSIVE}/>
                        </Grid.Column>
                        <Grid.Column style={ColumnStyle}>
                            <NavigationButtonComponent color={ButtonColors.GREEN} icon={faUsers} text={"Teams"} redirectUri={RoutingPaths.TEAMS} type={IconButtonTypes.MASSIVE}/>
                        </Grid.Column>
                        <Grid.Column style={ColumnStyle}>
                            <NavigationButtonComponent color={ButtonColors.BLUE} icon={faMap} text={"About"} redirectUri={RoutingPaths.ABOUT} type={IconButtonTypes.MASSIVE} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Container text>
                            <Segment inverted>
                                <Header as='h2'>Cowboy Shooter</Header>
                                <WhatIsThisItemDescription/>
                            </Segment>
                        </Container>
                    </Grid.Row>
                </Grid>
            </div>
        )
    }
}

const GridStyle = {
    width: "50%",
    margin: "auto",
    minWidth: "500px",
    paddingTop: "5%"
};

const ColumnStyle = {
    minWidth: "150px"
};

export default HomeComponent
