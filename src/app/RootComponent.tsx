import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Keycloak from 'keycloak-js';
import {RoutingPaths} from "./base/RoutingPaths";
import MatchesComponent from "./components/matches/MatchesComponent";
import AboutComponent from "./components/about/AboutComponent";
import TeamsComponent from "./components/teams/TeamsComponent";
import HomeComponent from "./components/home/HomeComponent";
import {Properties, Settings} from '../Settings';
import CenteredLoader from "./components/loader/CenteredLoader";


export class RootComponent extends Component {

    public static keycloak = Keycloak(Settings.getProperty(Properties.KEYCLOAK_FILE));

    constructor(props: any) {
        super(props);
        this.state = {
            keycloak: {
                userAuthenticated: false,
                userProfile: {},
                token: '',
                keycloak: undefined
            }
        };
    }

    componentDidMount() {
        this.setupKeycloak();
    }

    setupKeycloak() {
        RootComponent.keycloak.init({onLoad: 'login-required'})
            .success(() => {
                this.onKeycloakResponse();
                setInterval(() => this.refreshToken(), Settings.getProperty(Properties.ACCESS_TOKEN_REFRESH_RATE) as number);
            })
            .error(() => alert('failed to authenticate'));
    }

    refreshToken = () => {
        RootComponent.keycloak.updateToken(Settings.getProperty(Properties.TOKEN_MIN_VALIDITY) as number)
            .success(this.onKeycloakResponse)
            .error(() => alert('failed to refresh'));
    };

    onKeycloakResponse = () => {
        RootComponent.keycloak.loadUserProfile().success( () => {
            RootComponent.keycloak.loadUserInfo().success(() => this.forceUpdate());
        });
    };

    render() {
        return (
                <div>
                    {
                        RootComponent.keycloak.authenticated ?
                            (<Router>
                                <Switch>
                                    <Route path={RoutingPaths.MATCHES}>
                                        <MatchesComponent />
                                    </Route>
                                    <Route path={RoutingPaths.ABOUT}>
                                        <AboutComponent />
                                    </Route>
                                    <Route path={RoutingPaths.TEAMS}>
                                        <TeamsComponent />
                                    </Route>
                                    <Route path={RoutingPaths.HOME}>
                                        <HomeComponent />
                                    </Route>
                                </Switch>
                            </Router>)
                            : ( <CenteredLoader/>)
                        }
                </div>
        )
    }
}

export default RootComponent