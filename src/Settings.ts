import * as data from './settings.json';

export class Settings {

    public static getProperty(kind: Properties): string | number {
        let activeProfile: string = data.activeProfile;
        // @ts-ignore
        let profileContent: any = data.profiles[activeProfile];
        if (profileContent[kind]) {
            return profileContent[kind];
        } else {
            // @ts-ignore
            return data.global[kind];
        }
    }
}

export enum Properties {
    KEYCLOAK_FILE = 'keycloakFile',
    PROXIES_FILE = 'proxiesFile',
    TOURNAMENT_DETAIL_REFRESH_RATE = 'tournamentsDetailRefreshRate',
    TOURNAMENTS_REFRESH_RATE = 'tournamentsRefreshRate',
    TEAMS_REFRESH_RATE = 'teamsRefreshRate',
    MATCHES_REFRESH_RATE = 'matchesRefreshRate',
    CHALLENGES_REFRESH_RATE = 'challengesRefreshRate',
    ACCESS_TOKEN_REFRESH_RATE = 'accessTokenRefreshRate',
    TOKEN_MIN_VALIDITY = 'tokenMinValidity',
    TEAMS_PAGE_SIZE = 'teamsPageSize',
    TEAMS_MEMBER_MANAGEMENT_PAGE_SIZE = 'teamsMemberManagementPageSize',
    MATCHES_PAGE_SIZE = 'matchesPageSize',
    TOURNAMENTS_PAGE_SIZE = 'tournamentsPageSize',
    CREATE_MATCH_TEAMS_PAGE_SIZE = 'createMatchTeamsPageSize',
    REPLAY_MATCH_SPEED = 'replayMatchSpeed',
    REPLAY_MATCH_MOVES_SIZE = 'replayMatchMovesSize',
    TEAM_DEFAULT_URL = 'teamDefaultUrl',
    GENERAL_DATE_FORMAT = 'generalDateFormat'
}