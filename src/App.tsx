import React from 'react';
import './App.css';
import RootComponent from "./app/RootComponent";

const App: React.FC = () => {
  return (<RootComponent/>);
};

export default App;
