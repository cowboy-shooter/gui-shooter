const proxy = require("http-proxy-middleware");
const settings = require("./settings.json");

module.exports = app => {
    let activeProfile = settings.activeProfile;
    let profileContent = settings.profiles[activeProfile];
    let filePath = profileContent['proxiesFile'];
    const proxies = require(filePath).proxies;
    proxies.forEach(proxyItem => {
        app.use(
            proxyItem.context,
            proxy({
                target: proxyItem.destination,
                changeOrigin: true
            })
        );
    });

    // let settings = new Settings();
    // let proxies = settings.getProfile().getProxies();
    //
    // proxies.forEach(proxyItem => {
    //     app.use(
    //         proxyItem.context,
    //         proxy({
    //             target: proxyItem.destination,
    //             changeOrigin: true
    //         })
    //     );
    // });
};